<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="webthemez">
    <title>Hotel Veranum</title>
    
    <link href="${pageContext.request.contextPath}/css/estiloIndex.css" rel="stylesheet">
	<!-- core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/animate.min.css" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/css/prettyPhoto.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
	
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slider-style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slider-custom.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/modernizr.custom.79639.js"></script>
		
	           
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/ico/favicon.ico"> 
    
    <!-- Javascripts -->
    <script src="${pageContext.request.contextPath}/js/accounting.min.js"></script>
    
    <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script> 
    <script src="${pageContext.request.contextPath}/js/mousescroll.js"></script>
    <script src="${pageContext.request.contextPath}/js/smoothscroll.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.prettyPhoto.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.isotope.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.inview.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/wow.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.ba-cond.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.slitslider.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/slitslider-custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/custom-scripts.js"></script>
    
    <!-- DatePicker -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
    
	<!--     Validacion -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
	
    <link href="${pageContext.request.contextPath}/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/jquery-ui-timepicker-addon.js"></script>
    
</head> 

<body id="home">
    
    
    <div class="content">
    	<div class="row">
		    <c:choose>
			    <c:when test="${page=='index'}">
			       <tiles:insertAttribute name="header-index" /> 
			    </c:when>    
			    <c:otherwise>
			        <tiles:insertAttribute name="header" />
			    </c:otherwise>
			</c:choose>
		</div>
		
		<div class="row">
			<tiles:insertAttribute name="body" />
		</div>
		
		<div class="row">
    		<tiles:insertAttribute name="footer" />
    	</div>
	</div>
    
</body>
</html>