<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">
<header id="header">
    <nav id="main-nav" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/index"><img src="${pageContext.request.contextPath}/images/logo-veranum.png" alt="logo"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="scroll"><a href="${pageContext.request.contextPath}/index">Inicio</a></li>  
                    <li class="scroll"><a href="${pageContext.request.contextPath}/index#services">Nuestros Servicios</a></li>
                    <li class="scroll"><a href="${pageContext.request.contextPath}/index#about">Acerca de nosotros</a></li> 
                    <li class="scroll"><a href="${pageContext.request.contextPath}/index#our-team">Nuestro equipo</a></li>
                    <li class="scroll"><a href="${pageContext.request.contextPath}/index#reserve">Reserva</a></li>
                    <li class="dropdown">
			          
						<c:choose>
							<c:when test="${NomUser.getNom_user().equals('Invitado')}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									Invitado
								<span class="caret"></span></a>		
								<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
									<li><a href="${pageContext.request.contextPath}/ingreso">Ingresar</a></li>
									<li class="dropdown-submenu">
										<a tabindex="-1" href="#">Registro</a>
										<ul class="dropdown-menu">
											<li><a tabindex="-1" href="${pageContext.request.contextPath}/empresa/registro">Empresa</a></li>
											<li><a tabindex="-1" href="${pageContext.request.contextPath}/persona/registro">Persona</a></li>
										</ul>
									</li>
								</ul>		          
							</c:when>
							<c:otherwise>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									${NomUser.getNom_user()}
								<span class="caret"></span></a>		
								<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
								<c:choose>
										<c:when test="${NomUser.getId_rol() == 2}">
											<li><a href="${pageContext.request.contextPath}/persona/perfil">Perfil</a></li>
										</c:when>
										
										<c:otherwise>
											<li><a href="${pageContext.request.contextPath}/empresa/perfil">Perfil</a></li>
										</c:otherwise>
								</c:choose>
									<li><a href="${pageContext.request.contextPath}/mis-reservas">Mis Reservas</a></li>
									<li><a href="${pageContext.request.contextPath}/dar-de-baja">Desactivar Usuario</a></li>
									<li><a href="${pageContext.request.contextPath}/CambiarClave">Cambiar clave</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="${pageContext.request.contextPath}/salir">Salir</a></li>
								</ul>
							</c:otherwise>
						</c:choose>
			          
						
			        </li>                      
                </ul>
            </div>
            
        </div><!--/.container-->
    </nav><!--/nav-->
</header><!--/header-->

