<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<br/>
<br/>
<div id="footer">
      <div class="navbar navbar-bottom">
        <div class="navbar-inner">
          <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 Veranum Hotels and Resorts Worldwide, Inc., Todos los derechos reservados.
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>