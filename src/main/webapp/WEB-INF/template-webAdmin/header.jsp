<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Dashboard</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
                    <ul id="g-account-menu" class="dropdown-menu" role="menu">
                        <li><a href="#">My Profile</a></li>
                    </ul>
                </li>
                <li><a href="${pageContext.request.contextPath}/salir"><i class="glyphicon glyphicon-lock"></i> Salir</a></li>
            </ul>
        </div>
	</div>
</div>

 <div class="col-sm-3">
     <!-- Left column -->
     <strong><i class="glyphicon glyphicon-wrench"></i> Tools</strong>

     <hr>

     <ul class="nav nav-stacked scrollable-menu" role="menu">
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu1"> Cargo <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu1">
                 <li><a href="${pageContext.request.contextPath}/admin/cargo/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/cargo/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             </ul>
         </li>
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu2"> Cargo Adicional <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu2">
				 <li><a href="${pageContext.request.contextPath}/admin/cargoAdicional/agregar"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/cargoAdicional/listar"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>

             </ul>
         </li>
         
         
       
          <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu25"> Consumo <i class="glyphicon glyphicon-chevron-right"></i></a>

            <ul class="nav nav-stacked collapse" id="menu25">
				 <li><a href="${pageContext.request.contextPath}/admin/consumo/buscar"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                <li><a href="${pageContext.request.contextPath}/admin/consumo/list-consumo"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
            </ul>
         </li> 
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu6"> Detalle Servicio <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu6">
				 <li><a href="${pageContext.request.contextPath}/admin/detalleServicio/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/detalleServicio/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>

             </ul>
         </li>         
        
       
         <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu8"> Empleado <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu8">
                 <li><a href="${pageContext.request.contextPath}/admin/empleado/buscar"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>
                 <li><a href="${pageContext.request.contextPath}/admin/empleado/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             </ul>
         </li> 
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu9"> Empresa <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu9">
 					<li><a href="${pageContext.request.contextPath}/admin/empresa/lista"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>

             </ul>
         </li>  
         
       
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu11"> Habitación <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu11">

				<li><a href="${pageContext.request.contextPath}/admin/habitacion/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                <li><a href="${pageContext.request.contextPath}/admin/habitacion/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             
             </ul>
         </li>   
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu12"> Horario Servicio <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu12">
				 <li><a href="${pageContext.request.contextPath}/admin/horarioServicio/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/horarioServicio/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             </ul>
         </li> 
         
          <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu13"> Hotel <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu13">
                 <li><a href="${pageContext.request.contextPath}/admin/hotel/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/hotel/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             </ul>
         </li>  
         
         <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu14"> Incidente <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu14">
                 <li><a href="${pageContext.request.contextPath}/admin/incidente/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/incidente/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
             </ul>
         </li>  
         
       
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu18"> Persona <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu18">
				 <li><a href="${pageContext.request.contextPath}/admin/persona/lista"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>

             </ul>
         </li> 
         
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu19"> Reserva <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu19">
				<li><a href="${pageContext.request.contextPath}/admin/reserva/lista"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>
				<li><a href="${pageContext.request.contextPath}/admin/periodo"><i class="glyphicon glyphicon-cog"></i> Carga de periodos</a></li>
				

             </ul>
         </li> 
       
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu21"> Servicio <i class="glyphicon glyphicon-chevron-right"></i></a>

             <ul class="nav nav-stacked collapse" id="menu21">
				 <li><a href="${pageContext.request.contextPath}/admin/servicio/add"><i class="glyphicon glyphicon-cog"></i> Agregar</a></li>               
                 <li><a href="${pageContext.request.contextPath}/admin/servicio/list"><i class="glyphicon glyphicon-cog"></i> Lista</a></li>

             </ul>
         </li>   
                                                                                                                                              
     </ul>

     
 </div>
        <!-- /col-3 -->
