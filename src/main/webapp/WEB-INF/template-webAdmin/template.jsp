<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Bootstrap 3 Admin</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="${pageContext.request.contextPath}/css/estiloIndexAdmin.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
		
		<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/ico/favicon.ico"> 
		<!--  Dynamic tables Bootstrap style -->
		<link href="${pageContext.request.contextPath}/css/jquery.bdt.css" rel="stylesheet">
		
		<!-- Font Awesome 4 -->		
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		
		<!-- /.modal -->
		
		<!-- script references -->
		<script src="${pageContext.request.contextPath}/js/accounting.min.js"></script>
		
		<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/scripts.js"></script>
		
		<!--  Dynamic tables Bootstrap style -->
		<script src="${pageContext.request.contextPath}/js/jquery.bdt.js"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.sortelements.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/datepicker.css">
		
		<!-- DatePicker -->
	        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css">
	        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css">
	        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css">
	        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
	    
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-timepicker-addon.css">
	    	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui-timepicker-addon.js"></script>
	    		

		<!--     Validacion -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
		
	</head>
	<body>
		<div class="content">
			<!-- header -->
			<tiles:insertAttribute name="header-admin" />
			<!-- /Header -->
		    
			<!-- Main -->
			<div class="col-sm-offset-3">
				<tiles:insertAttribute name="body-admin" />
			</div>
			<!-- /Main -->
		</div>
		
		
		<div class="modal" id="addWidgetModal">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		                <h4 class="modal-title">Add Widget</h4>
		            </div>
		            <div class="modal-body">
		                <p>Add a widget stuff here..</p>
		            </div>
		            <div class="modal-footer">
		                <a href="#" data-dismiss="modal" class="btn">Close</a>
		                <a href="#" class="btn btn-primary">Save changes</a>
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dalog -->
		</div>
		
	</body>
</html>