<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="content text-center">
	<h2>Periodos</h2>
	<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 thumbnail">
		<form id="periodo-form" method="post" action="${pageContext.request.contextPath}/admin/periodo">
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>�ltima fecha cargada: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="ultimaFecha" id="ultimaFecha"  value="${ultimaFecha }" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Nuevo periodo: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="nuevaFecha" id="nuevaFecha"  value="${fechaMinima}" required/>
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Cantidad Dias: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="cantidadDias" id="cantidadDias"  value="" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6">
			    	<button type="submit" class="btn btn-block">Confirmar</button>
		    	</div>
			</div>
		</form>
	</div>
</div>
<div id="dialog" title="Mensaje" class="text-center">
  <p></p>
</div>
	
<script>
var fechaMinimaDate = new Date($('#nuevaFecha').val());
$(document).ready(function(){
	$(document).keydown(function(e) { if (e.keyCode == 8 || e.keyCode == 46) $('input').focus(); });
	cargaInfo($('#nuevaFecha').val());
});

$("#nuevaFecha").datepicker({
	dateFormat: "yy-mm-dd",
	minDate: fechaMinimaDate,
	onSelect: function(dateText) {
		cargaInfo($('#nuevaFecha').val());
	}
});

function desabilitaEdicionManualDeFechas(){
	$("#nuevaFecha").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#nuevaFecha").keypress(function(e){
		e.preventDefault();
	});
}
function cargaInfo(fecha){
	console.log('FECHA: '+fecha);
	$.ajax({
		  url: '${pageContext.request.contextPath}/admin/info-periodo',
		  type: 'POST',
		  data: { "fechaFin": fecha} ,
	      success: function (response) {
	    	  	console.log(response);
	            if(response.error != null){
	            	
	            	$('#dialog').html("problemas al cargar los datos.");
		            $('#dialog').dialog({
		            	title: 'Lo sentimos.',
		            	buttons : {
		                    Ok: function() {
		                        $(this).dialog("close"); //closing on Ok click
		                    }
		                },
		            });
	            }else{
	            	$('#ultimaFecha').val(response.ultimaFecha);
	            	$('#cantidadDias').val(response.cantidadDias);
	            }
	        },
	        error: function () {
	            $('#dialog').html("problemas al cargar los datos.");
	            $('#dialog').dialog({
	            	title: 'Lo sentimos.',
	            	buttons : {
	                    Ok: function() {
	                        $(this).dialog("close"); //closing on Ok click
	                    }
	                }
	            });
	        }
		});
};

$("#periodo-form").on('submit',function(){
	 $('#dialog').html("<img alt='cargando...' src='${pageContext.request.contextPath}/images/ajax-loader.gif' />");
     $('#dialog').dialog({
     	title: 'Porfavor espere'
     });
});
</script>     