<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST"  action="${pageContext.request.contextPath}/admin/servicio/edit/${ser.getId()}">
					<h1>Actualizar Servicio</h1>
					<br>
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
							
							<tr>
								<td>Nombre Servicio:</td>
								<td><input type = "text"  name="nombre" value="${ser.getNombre()}" required//></td>
							
							</tr>
							<tr>
								<td>Estado Servicio:</td>								
								<td>
								
   									<c:if test='${ser.getEstado() == "1"}'>
                             			 <input type="radio" name="estado" value="1"     checked="checked"/>Disponible <input type="radio" name="estado" value="0" />No Disponible     
  									 </c:if>
   									<c:if test='${ser.getEstado() != "1"}'>
                             			 <input type="radio" name="estado" value="1" />Disponible <input type="radio" name="estado" value="0"     checked="checked"/>No Disponible     
  									 </c:if>					
																		
								</td>
							
							</tr>
							<tr>
								<td>Id detalle servicio:</td>
								<td>								
								<select name="id_detalle_servicio">
								<c:forEach items="${listaDetalle}" var="listaDetalle">
								<option value="${listaDetalle.getId()}">${listaDetalle.getDescripcion()}</option>
								</c:forEach>
							</select>           						    
								</td>						
							</tr>
							
							<tr>
						<td>Id Horario Servicio:</td>
						<td>
							<select name="id_horario_servicio">
								<c:forEach items="${listaHorario}" var="listaHorario">
								<option value="${listaHorario.getId()}">${listaHorario.getHora_Inicio()}</option>
								</c:forEach>
							</select>
						</td>
					</tr>	
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
<script>
$('.dynamic-table').bdt();
</script>