<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="visible-sm">
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<section id="login">
	<div class="container">
		<div class="text-center section-header">
			<h2 class="section-title">Ingresar</h2>
			${message}
			
		</div>
		<div class="row text-center">
		
			<div class="col-md-offset-4 col-md-4 col-sm-offset-2 col-sm-8 text-center thumbnail">
				<div class="form-group">
					<form method="POST" id="login" class="login" action="${pageContext.request.contextPath}/ingreso">
						<div class="text-left row col-md-offset-4 col-md-7 col-xs-offset-4 col-sm-offset-4 col-sm-7 input-group">
							<div class="input-group">
								<label>Usuario: </label><br>
								<input type = "text" id="usuario" name="usuario" />
								
							</div>
							<br/>
							<div class="input-group">
								<label>Contraseņa: </label><br>
								<input type = "password" id="pass" name="pass" />

							</div>
						</div>
						<br>
						<div class="row col-md-offset-1 col-md-5">
							<a href="${pageContext.request.contextPath}/empresa/registro" >Registro empresa</a>
						</div>	
						
						<div class="row col-md-offset-1 col-md-5">
							<a href="${pageContext.request.contextPath}/persona/registro" >Registro persona</a>
						</div>	
						<br>
						<br/>
						<input type="submit" value="Ingresar" class="btn btn-primary btn-block" />
									
								
					</form>
				</div>
			</div>
			<br/>
			<br/>
		
		</div>
	</div>
</section>

<script type="text/javascript">


   $(document).ready(function() {

                $(".login").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                usuario:   {required: true},
                                pass:   {required: true} 
                        },
                        messages: {
                                usuario: 	 {
                                        required: "Campo requerido"
                                },
                                pass: 	 {
                                        required: "Campo requerido"
                                },
                        }
                });

        });
  
</script>
	