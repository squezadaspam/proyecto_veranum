<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>


<h1>Lista Formulario Consumo</h1>
</br>

<div class="col-md-offset-7 col-sm-offset7 col-md-3 col-sm-3">
	<form Method="POST" action="${pageContext.request.contextPath}/admin/consumo/add-consu">
	        <input type="hidden" name="id_usuario" value="${usuario.getId()}"/>
	        <input type="submit" value="A�adir Servicio" />
	</form>
</div>

</br>
<div class="col-md-offset-7 col-sm-offset7 col-md-3 col-sm-3">
	<form Method="POST" action="${pageContext.request.contextPath}/admin/consumo/pago">
	        <input type="hidden" name="id_usuario" value="${usuario.getId()}"/>
	        <input type="submit" value="Pagar" />
	</form>
</div>
<div class="col-md-offset-1 col-sm-offset1 col-md-3 col-sm-3">
	<p> TOTAL: ${precio }</p>
</div>



<table class="tabla-dinamica table tumbnail">
	
	<thead>
        <tr>
            <th>Servicio</th>
            <th>Fecha inicio servicio</th>
            <th>Fecha fin servicio</th>
            <th>Fecha de registro</th>
            <th>Precio</th>
            <th>Accion</th>
        </tr>
    </thead>
    	   
    <tbody>
    
    	<c:forEach items="${listaConsumo}" var="consumo">
    		
    		<tr>
    			<c:forEach items="${listaServicio}" var="servicio">
    				<c:choose>
    					<c:when test="${consumo.getId_servicio() == servicio.getId() }">
    						<td>${servicio.getNombre() }</td>
    					</c:when>
    				</c:choose>
    			</c:forEach>
	            
	            <c:forEach items="${listaServicio}" var="servicio">
	            	<c:choose>
	            		<c:when test="${consumo.getId_servicio() == servicio.getId() }">
			            	<c:forEach items="${listaHorario}" var="horario">
			    				<c:choose>
			    					<c:when test="${servicio.getId_horario_servicio() == horario.getId() }">
			    						<td>${horario.getHora_InicioFormateada() }</td>
			    						<td>${horario.getHora_FinFormateada() }</td>
			    					</c:when>
			    				</c:choose>
		    				</c:forEach>
		    			</c:when>
	    			</c:choose>
    			</c:forEach>
    			
	            <td>${consumo.getFecha().toString() }</td>
	            
	            <c:forEach items="${listaServicio}" var="servicio">
	            	<c:choose>
	            		<c:when test="${consumo.getId_servicio() == servicio.getId() }">
			            	<c:forEach items="${listaDetalle}" var="detalle">
			    				<c:choose>
			    					<c:when test="${servicio.getId_detalle_servicio() == detalle.getId() }">
			    						<td>${detalle.getMonto() }</td>
			    					</c:when>
			    				</c:choose>
		    				</c:forEach>
		    			</c:when>
	    			</c:choose>
    			</c:forEach>
    			
	            <td>
	            	<button type="submit" OnClick="submitForm(${consumo.getId()})" value="eliminar">Eliminar</button>
					
	            </td>
	        </tr>
    	
    	</c:forEach>
    
    </tbody>

</table>

<form Method="POST" id="form-send-to-delete">
	<input type="hidden" name="idUs" value="${usuario.getId() }">
</form>
<script>
$('.tabla-dinamica').bdt();

function submitForm(conId){
	$("#form-send-to-delete").attr("action", "${pageContext.request.contextPath}/admin/consumo/borrar/"+conId);
	$("#form-send-to-delete").submit();
}
</script>