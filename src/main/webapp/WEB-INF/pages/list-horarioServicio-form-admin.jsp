<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>


<h1>Listar</h1>
</br>
<table class="tabla-dinamica table">

	
	
	<th>Fecha Inicio</th>
	
	<th>Fecha Fin</th>
	
	<th>Editar</th>

	<c:forEach items="${listadoHorarioServicio}" var="listadoHorarioServicio">
		<tr>
			
			<td>${listadoHorarioServicio.getHora_Inicio() }</td>
			<td>${listadoHorarioServicio.getHora_Fin() }</td>
			<td><a href="/veranum/admin/horarioServicio/edit/${listadoHorarioServicio.getId() }">link</a></td>
			
			<td><a href="${pageContext.request.contextPath}/admin/horarioServicio/edit/${listadoHorarioServicio.getId() }"><i class="fa fa-pencil-square-o"></i></a></td>
		</tr>
	</c:forEach>
</table>
