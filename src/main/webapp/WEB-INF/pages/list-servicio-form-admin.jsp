<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>


<h1>Listar</h1>
</br>
<table class="tabla-dinamica table">

	
	
	
	<th>Nombre</th>
	
	<th>Estado Servicio</th>
	
	<th>Id Detalle Servicio</th>
	
	<th>Id Horario Servicio</th>
	
	<th>Editar</th>

	<c:forEach items="${listadoServicio}" var="listadoServicio">
		<tr>
		
			
			<td>${listadoServicio.getNombre() }</td>
			<td>${listadoServicio.getEstado() }</td>
			<td>${listadoServicio.getId_detalle_servicio()}</td>
			<td>${listadoServicio.getId_horario_servicio()}</td>
			
			
			<td><a href="${pageContext.request.contextPath}/admin/servicio/edit/${listadoServicio.getId() }"><i class="fa fa-pencil-square-o"></i>link</a></td>
		</tr>
	</c:forEach>
</table>