<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="content text-center">
	<h2>Extender reserva</h2>
	<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 thumbnail">
		<form id="extend-form" method="post" action="${pageContext.request.contextPath}/admin/reserva/extender/confirmacion">
			<input type="hidden" name="idUsuario" id="idUsuario" value="${reserva.getIdUsuario()}" />
			<input type="hidden" name="tipoHabitacion" id="tipoHabitacion"  value="${tipoHabitacion.getId()}" />
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Codigo: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="codigo" id="codigo"  value="${reserva.getCodigo()}" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Fecha Inicio: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="fechaInicioAntigua" id="fechaInicioAntigua"  value="${reserva.getFechaInicioFormateada()}" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Fecha Fin: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" name="fechaInicio" id="fechaInicio"  value="${reserva.getFechaFinFormateada()}" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Cantidad Ni�os: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text"  name="cantidadNinos" id="cantidadNinos" value="${reserva.getNinos()}" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Cantidad Adultos: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text"  name="cantidadAdultos" id="cantidadAdultos" value="${reserva.getAdultos()}" />
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
			    	<label>Nueva Fecha Fin: </label>
		    	</div>
		    	<div class="col-sm-4 col-md-4">
			    	<input type="text" class="form-control datepickerFin" name="fechaFin" id="fechaFin" required/>
		    	</div>
			</div>
			<br>
			<div class="row">
		    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6">
			    	<button type="submit" class="btn btn-block">Confirmar</button>
		    	</div>
			</div>
		</form>
	</div>
</div>
	
<input type="hidden" class="form-control datepickerInicio" name="fechaInicio">
<input type="hidden" id="fechInicio" value="${reserva.getFechaInicio()}">
<script>
$(document).ready(function(){
	$(document).keydown(function(e) { if (e.keyCode == 8 || e.keyCode == 46) $('input').focus(); });
	$('.datepickerInicio').val($('#fechInicio').val());
	
	desabilitaEdicionManualDeFechas();
});
var fechaMin = new Date('${minDate}');
var fechaMax = new Date('${maxDate}');
$(".datepickerFin").datepicker({
	dateFormat: "dd/mm/yy",
	minDate: fechaMin,
	maxDate: fechaMax
	
});

function desabilitaEdicionManualDeFechas(){
	$("#fechaInicio").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaInicio").keypress(function(e){
		e.preventDefault();
	});
	$("#fechaInicioAntigua").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaInicioAntigua").keypress(function(e){
		e.preventDefault();
	});
	$("#fechaFin").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaFin").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").keypress(function(e){
		e.preventDefault();
	});
	$("#codigo").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#codigo").keypress(function(e){
		e.preventDefault();
	});
	$("#idUsuario").bind("cut copy paste", function(e){
		e.preventDefault();
	});
	$("#idUsuario").keypress(function(e){
		e.preventDefault();
	});
}
function submitForm(){
	$("#extend-form").submit();
}
</script>     