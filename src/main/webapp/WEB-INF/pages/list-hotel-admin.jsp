<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1>Listar Hoteles</h1>
</br>
<table class="tabla-dinamica table">
<header>
	<th>Nombre</th>
	<th>Categor�a (Estrellas)</th>
	<th>Editar</th>
</header>
	<c:forEach items="${listadoHoteles}" var="listadoHoteles">
		<tr>
			<td>${listadoHoteles.getNombre() }</td>
			<td>${listadoHoteles.getCategoria() }</td>
			<td><a href="${pageContext.request.contextPath}/admin/hotel/edit/${listadoHoteles.getId() }"><i class="fa fa-pencil-square-o"></i></a></td>
		</tr>
	</c:forEach>
</table>
</div>
<script >

$(".tabla-dinamica").bdt();

</script>
		

