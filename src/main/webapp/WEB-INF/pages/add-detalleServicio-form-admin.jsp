<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>

<h1>Agrgar detalles de servicios</h1>
<p>Agregue un nuevo detalle</p>

<form method="POST" id="formid" action="${pageContext.request.contextPath}/admin/detalleServicio/add">
	<h4><font color="red">${message}</font></h4>
			<table class="dynamic-table">
				<tbody>
					<tr>
						<td>Descripci�n del Servicio:</td>
						<td><input type="text" id="descripcion" name="descripcion" /></td>
					</tr>
					<tr>
						<td>Monto:</td>
						<td><input type="number" id="monto" name="monto" /></td>
					</tr>
					<tr>
						<td><input type="submit" id="add" name="add" value="Guardar"/></td>
					</tr>
				</tbody>
			</table>
</form>


<script type="text/javascript">
	$(document).ready(function() {
    

   	 $("#formid").validate({
        rules: {
            descripcion: { required: true, minlength: 2},
            
            monto: { required: true},
            
        },
        messages: {
            descripcion: "Debe introducir descripcion.",
           
            monto: "Debe introducir solo n�meros."
          
        		  }
    	});
	});
</script>
		
		


			
		