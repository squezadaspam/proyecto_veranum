<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<div class="title">
	<h2>Habitaciones</h2>
</div>
<div class="table-responsive">
	<h4><font color="red">${message}</font></h4>
					<br>
	<table class="dynamic-table table">
    <thead>
        <tr>
            <th>Numero</th>
            <th>Estado</th>
            <th>Tipo</th>
		
        </tr>
    </thead>
    
   
    <tbody>
	     <c:forEach items="${lista}" var="lista" varStatus="status">
	        <tr>
	        	        	
	            <td>${ lista.getNumero() }</td>
	            <td>
	            <c:if test="${ lista.getEstado() == 1}">
                     Disponible
  				</c:if>	
  				<c:if test="${ lista.getEstado() != 1}">
                     No Disponible
  				</c:if>            
	            </td>	            
	            <td><c:forEach items="${tipos}" var="tipos" varStatus="status">
	            		<c:if test='${tipos.getId() == lista.getIdTipoHabitacion()}'>
                             		      ${tipos.getTipo()}
  						 </c:if>
	            	</c:forEach>	            
	            </td>
	            <td><element title="Edita"><a href="${pageContext.request.contextPath}/admin/habitacion/edit/${lista.getId()}"><i class="fa fa-pencil-square-o"></i></a></element> <element title="Eliminar"><a href="${pageContext.request.contextPath}/admin/habitacion/delete/${lista.getId()}" onclick="return confirmar('${pageContext.request.contextPath}/habitacion/delete/${lista.getId()}')"><i class="fa fa-eraser"></i></a></element>
	            	          
	          	
	        </tr>
	        
	 	</c:forEach>
    </tbody>

	</table>

</div>  
<script type="text/javascript">
$('.dynamic-table').bdt();
function confirmar(url)
{
	if(confirm('�Est�s seguro de Eliminar el registro?'))
	{
		window.location=url;
	}
	else
	{
		return false;
	}	
}
</script>

