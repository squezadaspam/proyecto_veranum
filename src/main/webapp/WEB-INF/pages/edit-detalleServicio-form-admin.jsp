<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST"  action="${pageContext.request.contextPath}/admin/detalleServicio/edit/${detalleServicio.getId()}">
					<h1>Actualizar DetalleServicio</h1>
					<br>
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
						
							<tr>
								<td>Descripcion:</td>
								<td><input type = "text"  name="descripcion" value="${detalleServicio.getDescripcion()}" required//></td>
							
							</tr>
							<tr>
								<td>Monto:</td>
								<td><input type = "number"  name="monto" value="${detalleServicio.getMonto()}" required//></td>
							
							</tr>
							
							
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>