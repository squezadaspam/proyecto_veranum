<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="title">
	<h2>Persona</h2>
</div>
<table class="dynamic-table table table-condensed">
    <thead>
        <tr>
            <th>id usuario</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>email</th>
            <th>calle</th>
            <th>Codigo Postal</th>
            <th>Numero Depto.</th>
            <th>Numero de Casa</th>
            <th>Detalles</th>
        </tr>
    </thead>
    
   
    <tbody>
	<c:set var="cont" value="1"/>     
	<c:forEach var="persona" items="${personas}">
		<c:if test="${cont == 1}">
			<tr>
				<td>${persona[0]}</td>
				<td>${persona[1]}</td>
				<td>${persona[2]}</td>
				<td>${persona[3]}</td>
				<td>${persona[4]}</td>
				<td>${persona[5]}</td>
				<td>${persona[6]}</td>
				<td>${persona[7]}</td>
				<td><a href="${pageContext.request.contextPath}/admin/persona/info/${persona[0]}">Detalle</a></td>
			</tr>	
			<c:set var="cont" value="${persona[0]}"/>
		</c:if>
		
		<c:if test="${cont != persona[0]}">
			<tr>
				<td>${persona[0]}</td>
				<td>${persona[1]}</td>
				<td>${persona[2]}</td>
				<td>${persona[3]}</td>
				<td>${persona[4]}</td>
				<td>${persona[5]}</td>
				<td>${persona[6]}</td>
				<td>${persona[7]}</td>
				<td><a href="${pageContext.request.contextPath}/admin/persona/info/${persona[0]}">Detalle</a></td>
			</tr>	
			<c:set var="cont" value="${persona[0]}"/>
		</c:if>
	</c:forEach>
	            
    </tbody>

</table>
<form id="cancelarForm" method="POST" action="${pageContext.request.contextPath}/admin/reserva/cancelar/${reserva.getId()}">
</form>
<script>
function submitForm(id){
	var url = "${pageContext.request.contextPath}/admin/reserva/cancelar/"+id;
	$("#cancelarForm").attr("action", url);
	$("#cancelarForm").submit();
}

$('.dynamic-table').bdt();
</script>     