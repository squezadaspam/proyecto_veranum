<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


		<h1>Editar</h1>
		<p></p>
		<p>${message}</p><br/>
        <div class="form-group">
		<form  method="POST"  action="${pageContext.request.contextPath}/admin/cargoAdicional/editar/${temporada.getId()}" onsubmit="return validacion()">
					<table>
						<tbody>
							<tr>
								<td>Descripción:</td>
								<td><input type = "text" id="nombre" name="nombre" value="${temporada.getNombre()}" placeholder="Descripción" title="Una breve descripción" /></td>
							
							</tr>
							<tr>
								<td>Fecha Inicio:</td>
								<td>
								
									<input type="text" class="form-control datepickerInicio" value="${temporada.getFechaInicioFormateada()}" name="fechaInicio" id="datepickerInicio">			    
									
								
								</td>
							
							</tr>
							<tr>
								<td>Fecha Fin:</td>
								<td>		
								
								<input type="text" class="form-control datepickerFin" value="${temporada.getFechaFinFormateada()}" name="fechaFin" id="datepickerFin">
										    
								</td>
							
							</tr>
							<tr>
								<td>Factor:</td>
								<td><input type = "text" id="cargo" name="cargo" value="${temporada.getCargo()}" placeholder="Cargo a aplicar" title="Ejemplo: 0.3" /></td>
							
							</tr>
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
			</div>

  <input type="hidden" id="maxDate" value="2016-12-31"/>
   <script>

	$(document).ready(function(){
		desabilitaEdicionManualDeFechas();
		$('.datepickerFin').prop("disable", true);
	});
	
	var maxFechaArray = $("#maxDate").val().split("-");
	var maxDate = new Date(maxFechaArray[0],maxFechaArray[1] - 1,maxFechaArray[2]);
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
	$('.datepickerInicio').datepicker({
		dateFormat: "dd/mm/yy",
		minDate: nowTemp,
		maxDate: maxDate,
		onClose: function( selectedDate, inst ) {          
		      //$(".datepickerFin").datepicker("option", "minDate", selectedDate);
			$(".datepickerFin").datepicker("option", "minDate", selectedDate),
			desabilitaFechaFin()
		}
	   
	});
	
   
	$(".datepickerFin").datepicker({
		dateFormat: "dd/mm/yy",
		maxDate: maxDate
	});
	
	function desabilitaFechaFin(){
		if($('.datepickerInicio').val() != ""){
			$('.datepickerFin').prop("disabled", false);
			$( ".datepickerFin" ).datepicker( "setDate", nowTemp );
			$( ".datepickerFin" ).datepicker( "show");
		} else {
			$('.datepickerFin').prop("disabled", false);
		}
	}
   
	function desabilitaEdicionManualDeFechas(){
		$(".datepickerInicio").bind("cut copy paste", function(e){
			e.preventDefault();
		});
		$(".datepickerInicio").keypress(function(e){
			e.preventDefault();
		});
		$(".datepickerFin").bind("cut copy paste", function(e){
			e.preventDefault();
		});
		$(".datepickerFin").keypress(function(e){
			e.preventDefault();
		});
	}
  
   </script>
   
    <script>
   
   function validacion() {
	   
	   valor = document.getElementById("nombre").value;
	   if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if( valor.length > 4 ) {
		 alert('Error en la longitud del factor, largo maximo 3. Ejemplo: 3.5 = 350% 0.9 = 90%');
	     return false;
	   }
	   
	   valor = document.getElementById("datepickerInicio").value;
	   if( valor == null || valor.length == 0 ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("datepickerFin").value;
	   if( valor == null || valor.length == 0) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if(!/^([0-9])[.][0-9]*$/.test(valor) ) {
		   
		 alert('Error en el valor del cargo. Ejemplo: 0.9 0.95');
	     return false;
	     
	   }
	   
   }
   
   
   </script>