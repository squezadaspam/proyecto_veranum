<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="title">
	<h2>Cargos adicionales</h2>
</div>
<div class="table-responsive">
	
	<table class="dynamic-table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descripción</th>
            <th>Fecha Inicio</th>
            <th>Fecha Termino</th>
            <th>Factor</th>

        </tr>
    </thead>
    
   
    <tbody>
	     <c:forEach items="${temporada}" var="temporada">
	        <tr>
	            <td>${temporada.getId()}</td>
	            <td>${temporada.getNombre()}</td>
	            <td>${temporada.getFechaInicioFormateada()}</td>
	            <td>${temporada.getFechaFinFormateada()}</td>
	            <td>${temporada.getCargo()}</td>
	            <td>
		            <element title="Editar"><a href="${pageContext.request.contextPath}/admin/cargoAdicional/editar/${temporada.getId()}"><i class="fa fa-pencil-square-o"></i></a></element>
		            <element title="Eliminar"><a href="${pageContext.request.contextPath}/admin/cargoAdicional/borrar/${temporada.getId()}"><i class="fa fa-eraser"></i></a></element>
	            </td>
	        </tr>
	        
	 	</c:forEach>
    </tbody>

</table>

</div>  
		
<script>

$('.dynamic-table').bdt();
</script>  
