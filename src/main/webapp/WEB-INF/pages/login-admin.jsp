<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="col-sm-9">
	<div class="form-group">
		<form method="POST" action="/admin/login">
			<div class="row col-md-offset-4 col-md-4 col-xs-12 input-group">
				<div class="input-group">
					<label for="usuario">Username</label>
					<input class="form-control" placeholder="Usuario" id="usuario" />
					<!-- <form:errors path="usuario" cssClass="error" /> -->
				</div>
				<br/>
				<div class="input-group">
					<label for="exampleInputPassword1">Password</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
					<!-- <form:errors path="pass" cssClass="error" /> -->
				</div>
			</div>
			
			<div class="row col-md-offset-3 col-md-3">
				<a href="forgotPassword" >Olvid� mi password</a>
			</div>	
			
			<div class="row col-md-offset-1 col-md-4">
				<a href="register" >Registro</a>
			</div>	
			
			<br/>
			<div class="row col-md-offset-5 col-md-2 col-xs-12 input-group">
				<input type="submit" value="Entrar" class="btn btn-primary btn-block" />	
			</div>
		</form>
	</div>
</div>