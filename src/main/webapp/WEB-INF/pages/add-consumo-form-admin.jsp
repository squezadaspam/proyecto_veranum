<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>

<h2>Consumo:</h2>
<h4><font color="red">${message}</font></h4>
			
<div class="content">
	<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 thumbnail">
			
		<table class="tabla-dinamica table-responsive table">

			<thead>
		        <tr>
		            <th>Servicio</th>
		            <th>Fecha inicio servicio</th>
		            <th>Fecha fin servicio</th>
		            <th>Precio</th>
		            <th>Accion</th>
		        </tr>
		    </thead>
		    	   
		    <tbody>
		    
		    	<c:forEach items="${listaServicio}" var="servicio">
		    		
		    		<tr>
		    			
    					<td>${servicio.getNombre() }</td>
	    				
			            
		            	<c:forEach items="${listaHorario}" var="horario">
		    				<c:choose>
		    					<c:when test="${servicio.getId_horario_servicio() == horario.getId() }">
		    						<td>${horario.getHora_InicioFormateada() }</td>
		    						<td>${horario.getHora_FinFormateada() }</td>
		    					</c:when>
		    				</c:choose>
	    				</c:forEach>
			    			
			            
			            
		            	<c:forEach items="${listaDetalle}" var="detalle">
		    				<c:choose>
		    					<c:when test="${servicio.getId_detalle_servicio() == detalle.getId() }">
		    						<td>${detalle.getMonto() }</td>
		    					</c:when>
		    				</c:choose>
	    				</c:forEach>
				    			
		    			
			            <td>
			            	<button class="btn btn-action" id="agregarConsumo" Onclick="submitForm(${servicio.getId()})" value="">Agregar a la Boleta</button>
			            </td>
			        </tr>
		    	
		    	</c:forEach>
		    
		    </tbody>
		
		</table>
		
	</div>
</div>
<form Method="POST" id="form-send-to-boucher">
	<input type="text" name="idUs" value="${usuario.getId() }">
</form>
<script>
$('.tabla-dinamica').bdt();

function submitForm(serId){
	$("#form-send-to-boucher").attr("action", "${pageContext.request.contextPath}/admin/consumo/agregar/"+serId);
	$("#form-send-to-boucher").submit();
}

</script>