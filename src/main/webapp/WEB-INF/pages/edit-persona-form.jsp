<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<br>
<br>
<br>
<br>

<div class="text-center">
	<h1>Perfil del usuario</h1>
	<p>${message}<br/>
</div>

<section class="perfil-persona">
	<div class="container">
		<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">
			
			<form method="POST" id="personaForm">
				
				<div class="col-md-6 col-sm-6">
				
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" value="${nombre}" id="nombre" name="nombre" />
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Apellido:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="apellido" value="${apellido}" name="apellido"  />
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Run:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" value="${run}" id="run" name="run" readonly="readonly"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Fecha de nacimiento:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type="text" name="fec_nac" value="${fec_nac}" id="fec_nac" required="required"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Email:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="email" value="${email}" name="email"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Telefonos:</label>
						</div>
						<br>
						
						<c:forEach items="${listaTelefono}" var="listaTelefono">
							<div class="col-md-10 col-sm-10">
								<label  id = "NombreTelefono"> Telefono: ${ listaTelefono.getNombre()} </label>
								<input type = "text" id="telefono" value="${listaTelefono.getNumero()}" name="telefono"/>�Eliminar Telefono? <input type="checkbox" name="ChbTelefono" value="${ listaTelefono.getNombre()}"/>
							</div>
						</c:forEach>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<button class="add_field_button">Agregar Telefono</button>
						</div>
						<br>
					
					    <div class="row">
					    	<div class="col-md-6 col-sm-6">
					    		<label>Nombre Telefono</label>
					    	</div>
					    	
					    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					    		<label>Numero Telefono</label>
					    	</div>
						    
					    </div>
					    <div class="input_fields_wrap">
						    <div class="row text-center">
						    	<div class="col-md-6 col-sm-6">
						    		<input type="text" size="20" name="NombreTelefonoNuevo"  placeholder="EJ: CASA">
						    	</div>
						    	
						    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
						    		<input type="text" name="NumeroTelefonoNuevo"  size="20" placeholder="EJ: +569 11111111">
						    	</div>
						    </div>
							<br>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Calle:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="calle" name="calle" value="${calle}" />
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Codigo postal:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="codigo_postal" name="codigo_postal" value="${codigoPostal}"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Numero de casa:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_casa" name="num_casa" value="${numCasa}"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Numero de departamento :</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_depto" name="num_depto" value="${numDepto}" placeholder="Opcional"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Ciudad:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="ciudad" name="ciudad" value="${ciudad}"/>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Pais:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<select name = "pais" >
		                    <option value="<c:out value="${paisActual.id}" />"><c:out value="${paisActual.nombre}" /></option>
		                    <option value="0" > --- Select --- </option>
			                    <c:forEach items="${listadoPais}" var="pais">
							        <option value="<c:out value="${pais.id}" />"><c:out value="${pais.nombre}" /></option>
							    </c:forEach>
		                    </select>
				
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre Usuario:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type= "text" name = "nom_user" id = "nom_user" value="${nom_user}" readonly="readonly"/>
				
						</div>
					</div>
					<br>
				</div>
				
				<br>
				
				<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<input type="submit" id="edit" name="edit" value="Modificar" />
						</div>
					</div>
				</div>
		
			</form>
		</div>
	</div>
</section>
<script>
    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='row text-center'><div class='col-md-6 col-sm-6'><input type='text' size='20' name='NombreTelefonoNuevo' placeholder='EJ: CASA'></div><div class='col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5'><input type='text' name='NumeroTelefonoNuevo' size='20' placeholder='EJ: +569 11111111'></div><a href='#' class='remove_field'>Remove</a></div>"); //add input box
            //$(wrapper).append('<div><label>Nombre Telefono: </label> <input type="text" size="20" name="NumeroTelefono" placeholder="EJ: CASA"/> <label>Numero Telefono: </label> <input type="text" size="20" name="NombreTelefono" placeholder="EJ: +569 11111111"/> <a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;        
    })
});
    
  $(function() {
	   $( "#fec_nac" ).datepicker({
	   changeMonth: true,
       changeYear: true,     
       yearRange:'-90:+0',
	   dateFormat: "yy-mm-dd",
	   maxDate: "0"
	});
	 
  });
</script>

    <!-- 	VALIDACION DE FORMULARIO -->

<script type="text/javascript">

	var deferred = new $.Deferred(),
	promise = deferred.promise();
   $(document).ready(function() {

                $("#personaForm").validate({
                submitHandler: function(form,e) {
				    promise.done( function(){form.submit();});
					e.preventDefault();
				    return ConfirmDialog('Atenci�n', '�Esta seguro que desea modificar sus datos?');
				},
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                nombre:   {required: true, minlength: 4, maxlength: 45, formatoPalabra:true, valueNotEquals: ""},
                                run:    {required: true, formatoRunPersona : true},
                                calle: 	 {required: true, minlength: 4, maxlength: 100, valueNotEquals: ""},
                                codigo_postal:  	 {required: true, minlength: 4, maxlength: 10, valueNotEquals: ""},
                                email: 	 {required: true,  email: true},
                                num_casa: {required: true, number:true},
                                ciudad:  {required: true, minlength: 4, maxlength: 45, valueNotEquals: ""},
                                pais: {valueNotEqualsPais: "0"},
                                nom_user: 	 {required: true, minlength: 4, valueNotEquals: ""},
                                NombreTelefonoNuevo: {minlength: 4},
                                NumeroTelefonoNuevo: {formatoTelefono : true},
                                telefono: {required: true, formatoTelefono : true},
                                fec_nac :{required: true, date:true},
                                apellido : {required: true,minlength: 4, maxlength: 45, formatoPalabra:true, valueNotEquals: ""}
      
                        },
                        messages: {
                                nombre: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                 apellido: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                email:		 {
                                        required: "Campo requerido",
                                        email:	  "Formato no valido"
                                },
                                calle:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 100 caracteres"
                                },
                                codigo_postal:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 10 caracteres"
                                },
                                run:		 {
                                        required: "Campo requerido"
                                },
                                num_casa:		 {
                                        required: "Campo requerido",
                                        number: "Solo n�meros"
                                },
                                ciudad:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                nom_user:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                NombreTelefonoNuevo: {
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                telefono: {
                                        required: "Campo requerido"
                                },
                                fec_nac: { required: "Campo requerido"}
                        }
                });
                //para seleccionar un elemento del combobox diferente al elemento 0
			$.validator.addMethod("valueNotEqualsPais", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un pais");
			 $.validator.addMethod("valueNotEquals", function(value, element, arg){
			  return arg != $.trim(value) && $.trim(value).length > 3;
			 }, "Campo requerido, debe tener minimo 4 caracteres");
			 // valida formato telefono
			 $.validator.addMethod("formatoTelefono", function(value, element){
			  return this.optional(element) || /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test(value) ;
			 }, "Formato del telefono invalido, EJ: +569XXXXXXXX");
			  //valida formato run persona
			  $.validator.addMethod("formatoRunPersona", function(value, element){
			  return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value) ;
			 }, "Formato de run invalido, EJ: XX.XXX.XXX-X");
			 //valida la contrase�a que tenga letra minuscula, mayuscula, numero, caracter especial y 8 digitos minimo
 			  $.validator.addMethod("formatoPass", function(value, element){
			  return this.optional(element) || /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value) ;
			 }, "La contrase�a debe tener al menos: <br>-Una letra mayuscula <br>-Una letra minuscula <br>-Un n�mero o un caracter especial <br>-Longitud minima de 8 caracteres");
 			// valida que no tenga numeros, y que pueda tener tildes.
 			  $.validator.addMethod("formatoPalabra", function(value, element){
			  return this.optional(element) || /^[a-zA-Z������������������������_\s]+$/.test(value) ;
			 }, "Formato invalido");
			 
	 		
        });
  
      function ConfirmDialog(title,message){
        var confirmdialog = $('<div></div>').appendTo('body')
        .html('<div><h6>'+message+'</h6></div>')
        .dialog({
          modal: true, title: title, zIndex: 10000, autoOpen: false,
          width: 'auto', resizable: false,
          buttons: {
            Si: function(){
              deferred.resolve();
              $(this).dialog("close");
            },
            No: function(){
              $(this).dialog("close");
            }
          },
          close: function(event, ui){
            $(this).remove();
            return false;
          }
        });
        return confirmdialog.dialog("open");
      }
</script>

		


