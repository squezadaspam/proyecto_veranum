<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST"  action="${pageContext.request.contextPath}/admin/horarioServicio/edit/${horarioServicio.getId()}">
					<h1>Actualizar HorarioServicio</h1>
					
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
						
							<tr>
								<td>Fecha Inicio:</td>
								<td><input type="text" id="fechaInicio" class="datepickerInicio" name="fecha_inicio" value="${horarioServicio.getHora_Inicio()}" required//></td>
								
							</tr>
							<tr>
								<td>Fecha Fin:</td>
								<td><input type = "text" id="fechaFin" class="datepickerFin" name="fecha_fin" value="${horarioServicio.getHora_Fin()}" required//></td>
							
							</tr>
							
				
							
							
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
<script>
		
		
		
		
		
		$(document).ready(function(){
			desabilitaEdicionManualDeFechas();
			$('.datepickerFin').prop("disabled", true);
		});
		
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	   
		$('.datepickerInicio').datetimepicker({
			onClose: function( selectedDate, inst ) {          
				$(".datepickerFin").datepicker("option", "minDate", selectedDate),
				desabilitaFechaFin()
			}
		});
	   
		$(".datepickerFin").datetimepicker({
		});
		
		function desabilitaFechaFin(){
			if($('.datepickerInicio').val() != ""){
				$('.datepickerFin').prop("disabled", false);
				$( ".datepickerFin" ).datepicker( "setDate", nowTemp );
				$( ".datepickerFin" ).datepicker( "show");
			} else {
				$('.datepickerFin').prop("disabled", true);
			}
		}
	   
		function desabilitaEdicionManualDeFechas(){
			$(".datepickerInicio").bind("cut copy paste", function(e){
				e.preventDefault();
			});
			$(".datepickerInicio").keypress(function(e){
				e.preventDefault();
			});
			$(".datepickerFin").bind("cut copy paste", function(e){
				e.preventDefault();
			});
			$(".datepickerFin").keypress(function(e){
				e.preventDefault();
			});
		}
			
			
		
		
		
		
		
		
		
		
		</script>