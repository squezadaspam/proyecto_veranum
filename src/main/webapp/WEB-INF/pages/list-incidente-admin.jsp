<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1>Listado de Incidentes</h1>
</br>
<table class="tabla-dinamica table">
<header>
	<th>Descripción</th>
	<th>Fecha de Registro</th>
	<th>Fecha de Suceso</th>
	<th>Hotel</th>
	<th>Editar</th>
</header>
	<c:forEach items="${listadoIncidentes}" var="incidentes">
		<tr>
			<td>${incidentes.getDescripcion() }</td>
			<td>${incidentes.getFecha_registro() }</td>
			<td>${incidentes.getFecha_suceso() }</td>
			
			<c:forEach items="${listadoHoteles}" var="hotel">
				<c:choose>
					<c:when test="${incidentes.getId_hotel() == hotel.getId() }">
						<td>${hotel.getNombre() }</td>
					</c:when>
				</c:choose>
			</c:forEach>			

			<td><a href="${pageContext.request.contextPath}/admin/incidente/edit/${incidentes.getId() }"><i class="fa fa-pencil-square-o"></i></a></td>
		</tr>
	</c:forEach>
</table>
</div>
<script >

$(".tabla-dinamica").bdt();

</script>
		

