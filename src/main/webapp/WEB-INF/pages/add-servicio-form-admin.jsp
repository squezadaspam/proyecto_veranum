<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1>Agregar Servicio</h1>
<br>

<form method="POST" id="servicio" action="${pageContext.request.contextPath}/admin/servicio/add">
			<h4><font color="red">${message}</font></h4>
			
		    <br>
			<table class="dynamic-table">
			
				<tbody>
					<tr>
						<td>Nombre:</td>
						<td><input type = "text"  name="nombre" id="nombre" required/></td>
					</tr>
					<tr>
						<td>Estado:</td>
						<td><input type="radio" name="estado" value="1" checked>Disponible
							<input type="radio" name="estado" value="0">No Disponible</td>
					</tr>
					<tr>
						<td>Id Detalle Servicio:</td>
						<td>
							<select name="id_detalle_servicio">
								<c:forEach items="${listaDetalle}" var="listaDetalle">
								<option value="${listaDetalle.getId()}">${listaDetalle.getDescripcion()}</option>
								</c:forEach>
							</select>
						</td>
					</tr>	
						<tr>
						<td>Id Horario Servicio:</td>
						<td>
							<select name="id_horario_servicio">
								<c:forEach items="${listaHorario}" var="listaHorario">
								<option value="${listaHorario.getId()}">${listaHorario.getHora_Inicio()}</option>
								</c:forEach>
							</select>
						</td>
					</tr>	
								
					<tr>
						<td><input type="submit" value="Agregar" /></td>
						<td></td>
					</tr>						
					
				</tbody>
			</table>
		</form>
		<script>$('.dynamic-table').bdt();</script>
		
