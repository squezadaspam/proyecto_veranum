<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="title">
	<h2>Reservas</h2>
</div>
<table class="dynamic-table table">
    <thead>
        <tr>
            <th>codigo</th>
            <th>Fecha Inicio</th>
            <th>Fecha Termino</th>
            <th>Habitaci�n</th>
            <th>Usuario</th>
            <th>Estado</th>
            <th>Acci�n</th>
        </tr>
    </thead>
    
   
    <tbody>
	     <c:forEach items="${reservas}" var="reserva">
	        <tr>
	            <td>${reserva.getCodigo()}</td>
	            <td>${reserva.getFechaInicioFormateada()}</td>
	            <td>${reserva.getFechaFinFormateada()}</td>
	            <td>${reserva.getIdHabitacion()}</td>
	            <td>
	            
		            <c:forEach var="persona" items="${personas}">
	           			<c:choose>
	           				<c:when test="${persona[0].toString().trim().equals(reserva.getIdUsuario().toString().trim()) }">
	           					<a href="${pageContext.request.contextPath}/admin/persona/info/${reserva.getIdUsuario()}">${persona[1].toString() } ${persona[2].toString() }</a>
	           				</c:when>
	           			</c:choose>
					</c:forEach>
	            	
	            </td>
	            <td>${reserva.getEstado()}</td>
	            <td>
		            <element title="Ver Detalle"><a href="${pageContext.request.contextPath}/admin/reserva/${reserva.getId()}"><i class="fa fa-pencil-square-o"></i></a></element>
		        </td>
	        </tr>
	        
	 	</c:forEach>
    </tbody>

</table>
<script>
$('.dynamic-table').bdt();
</script>     