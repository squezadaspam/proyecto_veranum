<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST" id="editEmpleado" action="${pageContext.request.contextPath}/admin/empleado/edit/${empleado.getId()}">
					<h1>Edici�n de Empleado</h1>
					<br>
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
							<tr>
								<td><input type = "hidden"  name="id" value="${empleado.getId()}"  disabled/></td>
							
							</tr>
							<tr>
								<td><input type = "text"  name="nombre" value="${persona.getNombre()}"  disabled/></td>
							
							</tr>
							<tr>
								<td><input type = "text"  name="apellido" value="${persona.getApellido()}"  disabled/></td>
							
							</tr>
							<tr>
								<label>Cargo:</label>	
								<select name = "id_cargo" id ="id_cargo" >
									<option value="0" > --- Select --- </option>
									
									<c:forEach items="${cargos}" var="cargo">
										<option value="<c:out value="${cargo.getId()}" />"><c:out value="${cargo.getNombre()}" /></option>
									</c:forEach>
								</select>

							</tr>
								<label>Hotel:</label>	
								<select name = "id_hotel" id ="id_hotel" > >
									<option value="0" > --- Select --- </option>
									
									<c:forEach items="${hoteles}" var="hotel">
										<option value="<c:out value="${hotel.getId()}" />"><c:out value="${hotel.getNombre()}" /></option>
									</c:forEach>
								</select>
							<tr>
								<td>Empleado Activo:</td>								
								<td>
									<input type = "hidden" id="estado" name="estado" value="${empleado.getEstado()}"/>
									<input type="checkbox" id="estadoCheck" name="estadoCheck" value="" />
   													
								</td>
							
							</tr>
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
				
<script>

   $(document).ready(function() {

                $("#editEmpleado").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                id_cargo: {valueNotEqualsCargo: "0"},
								id_hotel: {valueNotEqualsHotel: "0"},
      
                        },
                        messages: {
                               
                                run:		 {
                                        required: "Campo requerido"
                                }
                        }
                });
                //para seleccionar un elemento del combobox diferente al elemento 0
			 $.validator.addMethod("valueNotEqualsCargo", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un Cargo");
			 
			 $.validator.addMethod("valueNotEqualsHotel", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un Hotel");
			 
			 
        });
        
$(document).ready(function(){
	if($("#estado").val()==0){
		$("#estadoCheck").attr('checked', false);
		$('#estadoCheck').val('0');
		$('#estado').val('0');
	} else {
		$("#estadoCheck").attr('checked', true);
		$('#estadoCheck').val('1');
		$('#estado').val('1');
	}
});

$("#estadoCheck").change(function() {
	
    if($("#estadoCheck").is(':checked')) {
    	$('#estadoCheck').val('1');
    	$('#estado').val('1');
    }else{
    	$('#estadoCheck').val('0');
    	$('#estado').val('0');
    }
});

</script>
				