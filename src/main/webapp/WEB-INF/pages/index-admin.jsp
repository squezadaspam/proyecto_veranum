<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="title">
	<h2>Monitorizaci�n de reservas en curso</h2>
</div>
<div class="table-responsive">
	<table class="table table-striped">
	    <thead>
	        <tr>
	            <th>codigo</th>
	            <th>Fecha Inicio</th>
	            <th>Fecha Termino</th>
	            <th>Ni�os</th>
	            <th>Adultos</th>
	            <th>Accion</th>
	        </tr>
	    </thead>
	    <tbody>
	        <tr class="tabla-reservas">
	         
	        </tr>
	    </tbody>
	</table>
</div>      
<script>
$(document).ready(function(){
	cargaTipoHabitacion();
});

function cargaTipoHabitacion(){
	$.ajax({
		  url: '${pageContext.request.contextPath}/admin/reservas-en-curso',
		  type: 'POST',
		  data: { "personas": 'persona'} ,
		  success: function (response) {
	    	  var table = '';
	    	  $.each(response,function(key, value) {
			      //$("#tipoHabitacion").append('<option value=' + value.id + '>' + value.tipo + '</option>');
			      table += '<td>'+value.codigo+'</td>';
			      table += '<td>'+value.fechaInicioFormateada+'</td>';
			      table += '<td>'+value.fechaFinFormateada+'</td>';
			      table += '<td>'+value.ninos+'</td>';
			      table += '<td>'+value.adultos+'</td>';
			      table += '<td><element title="Ver Detalle"><a href="${pageContext.request.contextPath}/admin/reserva/'+value.id+'"><i class="fa fa-pencil-square-o"></i></a></element></td>';
			     
	    	  });
	    	  $(".tabla-reservas").append(table);
	      },
	      error: function () {
			$('#dialog').html("problemas al cargar los datos.");
			$('#dialog').dialog({
				title: 'Lo sentimos.',
				buttons : {
			        Ok: function() {
			            $(this).dialog("close"); //closing on Ok click
			        }
			    }
			});
	      }
		});
}

</script>  