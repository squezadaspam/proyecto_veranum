<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<br>
<br>
<br>
<br>

<div class="text-center">
	<h1>Registro de personas</h1>
	<p>${message}<br/>
</div>

<section class="registro-persona">
	<div class="container">
		<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">
			
			<form:form method="POST" id="personaForm" commandName="contactoPersona">
				
				<div class="col-md-6 col-sm-6">
				
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="nombre" name="nombre"  />
							<form:errors path="nombre" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Apellido:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="apellido" name="apellido"  />
							<form:errors path="apellido" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Run:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="run" name="run" placeholder="EJ: XX.XXX.XXX-X"/>
							<form:errors path="run" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Fecha de nacimiento:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type="text" name="fec_nac" id="fec_nac" required="required"  />
							<form:errors path="fec_nac" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Email:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="email" name="email"/>
							<form:errors path="email" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<button class="add_field_button">Agregar Telefono</button>
						</div>
						<br>
					
					    <div class="row">
					    	<div class="col-md-6 col-sm-6">
					    		<label>Nombre Telefono</label>
					    	</div>
					    	
					    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					    		<label>Numero Telefono</label>
					    	</div>
						    
					    </div>
					    <div class="input_fields_wrap">
						    <div class="row text-center">
						    	<div class="col-md-6 col-sm-6">
						    		<input type="text" size="20" name="NombreTelefono" placeholder="EJ: CASA">
						    	</div>
						    	
						    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
						    		<input type="text" name="NumeroTelefono" size="20" placeholder="EJ: +569XXXXXXXX">
						    	</div>
						    </div>
							<br>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Calle:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="calle" name="calle" />
							<form:errors path="calle" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Codigo postal:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="codigo_postal" name="codigo_postal"/>
							<form:errors path="codigo_postal" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Numero de casa:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_casa" name="num_casa" />
							<form:errors path="num_casa" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Numero de departamento :</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_depto" name="num_depto" placeholder="Opcional"/>
							<form:errors path="num_depto" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Ciudad:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="ciudad" name="ciudad"/>
							<form:errors path="ciudad" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Pais:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<select name = "pais" id="pais">
								<option value="0" > --- Select --- </option>
								
								<c:forEach items="${listadoPais}" var="pais">
									<option value="<c:out value="${pais.id}" />"><c:out value="${pais.nombre}" /></option>
								</c:forEach>
							</select>
							<form:errors path="pais" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre Usuario:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type= "text" name = "nom_user" id = "nom_user" />
							<form:errors path="nom_user" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type="password" name="contrase�a" id="contrase�a" />
							<form:errors path="contrase�a" cssClass="error" />
						</div>
						
					</div>
					<br>
					<div class="input row">
					<div class="col-md-10 col-sm-10">
							<label>Repita la contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="claveRepetida" name="claveRepetida"  required="required"/>
						</div>
					</div>
				</div>
				
				<br>
				
				<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<input type="submit" id="add" name="add" value="Guardar" />
						</div>
					</div>
				</div>
		
			</form:form>
		</div>
	</div>
</section>
<div id="dialog">
  <p></p>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='row text-center'><div class='col-md-6 col-sm-6'><input type='text' size='20' name='NombreTelefono' placeholder='EJ: CASA'></div><div class='col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5'><input type='text' name='NumeroTelefono' size='20' placeholder='EJ: +569 11111111'></div><a href='#' class='remove_field'>Remove</a></div>"); //add input box
            //$(wrapper).append('<div><label>Nombre Telefono: </label> <input type="text" size="20" name="NumeroTelefono" placeholder="EJ: CASA"/> <label>Numero Telefono: </label> <input type="text" size="20" name="NombreTelefono" placeholder="EJ: +569 11111111"/> <a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;        
    })
});
    
  $(function() {
	   $( "#fec_nac" ).datepicker({
	   changeMonth: true,
       changeYear: true,     
       yearRange:'-90:+0',
	   dateFormat: "yy-mm-dd",
	   maxDate: "0"
	});
	 
  });
</script>

    <!-- 	VALIDACION DE FORMULARIO -->

<script type="text/javascript">


   $(document).ready(function() {

                $("#personaForm").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                nombre:   {required: true, minlength: 4, maxlength: 45, formatoPalabra:true, valueNotEquals: ""},
                                run:    {required: true, formatoRunPersona : true},
                                calle: 	 {required: true, minlength: 4, maxlength: 100, valueNotEquals: ""},
                                codigo_postal:  	 {required: true, minlength: 4, maxlength: 10, valueNotEquals: ""},
                                email: 	 {required: true,  email: true},
                                num_casa: {required: true, number:true},
                                ciudad:  {required: true, minlength: 4, maxlength: 45, valueNotEquals: ""},
                                pais: {valueNotEqualsPais: "0"},
                                nom_user: 	 {required: true, minlength: 4, valueNotEquals: ""},
                                contrase�a: 	 {required: true, formatoPass: true, valueNotEquals: ""},
                                claveRepetida: {required: true, equalTo: "#contrase�a"},
                                NombreTelefono: {required: true, minlength: 4, valueNotEquals: ""},
                                NumeroTelefono: {required: true, formatoTelefono : true},
                                fec_nac :{required: true, date:true},
                                apellido : {required: true,minlength: 4, maxlength: 45, formatoPalabra:true, valueNotEquals: ""}
      
                        },
                        messages: {
                                nombre: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                 apellido: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                email:		 {
                                        required: "Campo requerido",
                                        email:	  "Formato no valido"
                                },
                                calle:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 100 caracteres"
                                },
                                codigo_postal:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 10 caracteres"
                                },
                                run:		 {
                                        required: "Campo requerido"
                                },
                                num_casa:		 {
                                        required: "Campo requerido",
                                        number: "Solo n�meros"
                                },
                                ciudad:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                nom_user:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                contrase�a:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                claveRepetida:{
                                		required: "Campo requerido",
                                		equalTo: "No coinciden las claves"
                                },
                                NombreTelefono: {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                NumeroTelefono: {
                                        required: "Campo requerido"
                                },
                                fec_nac: { 
                                		required: "Campo requerido",
                                		date: "Ingrese una fecha valida"}
                        }
                });
                //para seleccionar un elemento del combobox diferente al elemento 0
			$.validator.addMethod("valueNotEqualsPais", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un pais");
			 $.validator.addMethod("valueNotEquals", function(value, element, arg){
			  return arg != $.trim(value) && $.trim(value).length > 3;
			 }, "Campo requerido, debe tener minimo 4 caracteres");
			 // valida formato telefono
			 $.validator.addMethod("formatoTelefono", function(value, element){
			  return this.optional(element) || /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test(value) ;
			 }, "Formato del telefono invalido, EJ: +569XXXXXXXX");
			  //valida formato run persona
			  $.validator.addMethod("formatoRunPersona", function(value, element){
			  return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value) ;
			 }, "Formato de run invalido, EJ: 163660XX-X");
			 //valida la contrase�a que tenga letra minuscula, mayuscula, numero, caracter especial y 8 digitos minimo
 			  $.validator.addMethod("formatoPass", function(value, element){
			  return this.optional(element) || /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value) ;
			 }, "La contrase�a debe tener al menos: <br>-Una letra mayuscula <br>-Una letra minuscula <br>-Un n�mero o un caracter especial <br>-Longitud minima de 8 caracteres");
 			// valida que no tenga numeros, y que pueda tener tildes.
 			  $.validator.addMethod("formatoPalabra", function(value, element){
			  return this.optional(element) || /^[a-zA-Z������������������������_\s]+$/.test(value) ;
			 }, "Formato invalido");
			 
	 		
        });
        
  $('#personaForm').on('submit', function(e){
  	if(!VerificaRut($('#run').val())){
  		e.preventDefault();
  		$('#dialog').dialog({
  			title: 'Run no v�lido'
  		});
  		$('#dialog').html('Ingrese nuevamente el Run.');
  	}
  });
        
 function VerificaRut(rut) {
    if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
        var caracteres = new Array();
        var serie = new Array(2, 3, 4, 5, 6, 7);
        var dig = rut.toString().substr(rut.toString().length - 1, 1);
        rut = rut.toString().substr(0, rut.toString().length - 2);

        for (var i = 0; i < rut.length; i++) {
            caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
        }

        var sumatoria = 0;
        var k = 0;
        var resto = 0;

        for (var j = 0; j < caracteres.length; j++) {
            if (k == 6) {
                k = 0;
            }
            sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
            k++;
        }

        resto = sumatoria % 11;
        dv = 11 - resto;

        if (dv == 10) {
            dv = "K";
        }
        else if (dv == 11) {
            dv = 0;
        }

        if (dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase())
            return true;
        else
            return false;
    }
    else {
        return false;
    }
}
  
</script>

		

		