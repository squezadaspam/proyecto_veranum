<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<div class="row text-center">
		<br>
		<br>
		<br>
		<div class="sm-md-10 col-md-10 text-center thumbnail">
			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
				<h1><i class="fa fa-exclamation-triangle"></i></h1>
				<br>
				<p>
					<c:choose>
						<c:when test="${!message.equals('') }">
							${message }
						</c:when>
						<c:otherwise>
							Problemas al cargar la petición. Porfavor intente nuevamente.
						</c:otherwise>
					</c:choose>
				</p>
			</div>
		</div>
		<br/>
		<br/>
	
	</div>
</div>
