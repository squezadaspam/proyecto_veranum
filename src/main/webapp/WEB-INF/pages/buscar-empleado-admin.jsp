<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">
		<div class="col-md-offset-2 col-sm-offset-2 col-md-8 col-sm-8 text-center">
			
			<h1>B�squeda de Empleado</h1>
			
			<p>${message}</p>
			
				<form method="POST" id="personaEmpleadoForm" action="">
				
					
					
					<br>
					<br>
					<div class="col-md-offset-3 col-sm-offset-3 col-md-5 col-sm-5">
						<div class="col-md-2 col-sm-2">
							<label>RUN:</label>
						</div>
						<div class="col-md-10 col-sm-10">
							<input type = "text"  name="buscarRun" id="buscarRun" placeholder="EJ: 1636609X-X" required/>
						</div>
					</div>
					<br>
					<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
						<br>
						<div class="input row">
							<div class="col-md-10 col-sm-10">
								<input class="btn btn-block" type="submit" value="Buscar" />
							</div>
						</div>
					</div>
					
				</form>
		</div>
	</div>
</div>

		
<div id="dialog">
  <p></p>
</div>
<script>

$("#personaEmpleadoForm").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                buscarRun:    {required: true, formatoRunPersona : true}
      
                        },
                        messages: {
                                buscarRun:		 {
                                        required: "Campo requerido"
                                }
                        }
                });
               
			  $.validator.addMethod("formatoRunPersona", function(value, element){
			  return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value) ;
			 }, "Formato de run invalido, EJ: 163660XX-X");
			 

 $('#personaEmpleadoForm').on('submit', function(e){
  	if(!VerificaRut($('#buscarRun').val())){
  		e.preventDefault();
  		$('#dialog').dialog({
  			title: 'Run no v�lido'
  		});
  		$('#dialog').html('Ingrese nuevamente el Run.');
  	}
  });
        
 function VerificaRut(rut) {
    if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
        var caracteres = new Array();
        var serie = new Array(2, 3, 4, 5, 6, 7);
        var dig = rut.toString().substr(rut.toString().length - 1, 1);
        rut = rut.toString().substr(0, rut.toString().length - 2);

        for (var i = 0; i < rut.length; i++) {
            caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
        }

        var sumatoria = 0;
        var k = 0;
        var resto = 0;

        for (var j = 0; j < caracteres.length; j++) {
            if (k == 6) {
                k = 0;
            }
            sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
            k++;
        }

        resto = sumatoria % 11;
        dv = 11 - resto;

        if (dv == 10) {
            dv = "K";
        }
        else if (dv == 11) {
            dv = 0;
        }

        if (dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase())
            return true;
        else
            return false;
    }
    else {
        return false;
    }
}
  
</script>

		



