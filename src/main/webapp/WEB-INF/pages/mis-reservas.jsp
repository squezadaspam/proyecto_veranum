<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link href="${pageContext.request.contextPath}/css/mis-reservas.css" rel="stylesheet">
<div class="title">
	<h2>Mis reservas</h2>
</div>

<section class="demo-2">
	<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 contenido">
		<div class="list-group">
			
		</div>
	</div>
</section>

<script>
$( document ).ready(function(){
	$.post( "${pageContext.request.contextPath}/mis-reservas", function( reservas ) {
		for(var i in reservas){
			var estructura = "<a href='${pageContext.request.contextPath}/reserva/"+reservas[i].codigo+"' class='list-group-item active'><h4 class='list-group-item-heading'>"+reservas[i].codigo+"</h4><p class='list-group-item-text'>fecha inicio: "+reservas[i].fechaInicioFormateada+"\nfecha fin: "+reservas[i].fechaFinFormateada+"</p></a><br>";
			$(".list-group").append(estructura);
		}
	});
});
</script>