<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1>Lista de Empleados</h1>
</br>
<p>${message}</p>
<table class="tabla-dinamica table">
<header>
	<th>Nombre</th>
	<th>Apellido</th>
	<th>Cargo</th>
	<th>Hotel</th>
	<th>Estado</th>
	<th>Editar</th>
</header>
	<c:forEach items="${listadoEmpleados}" var="empleado">
		<tr>
			<c:forEach items="${listadoPersonas}" var="persona">
				<c:choose>
					<c:when test="${empleado.getId_persona() == persona.getId() }">
						<td>${persona.getNombre()}</td>
						<td>${persona.getApellido()}</td>
					</c:when>
				</c:choose>
			</c:forEach>
			
			<c:forEach items="${listadoCargos}" var="cargo">
				<c:choose>
					<c:when test="${empleado.getId_cargo() == cargo.getId() }">
						<td>${cargo.getNombre() }</td>
					</c:when>
				</c:choose>
			</c:forEach>
			
			<c:forEach items="${listadoHoteles}" var="hotel">
				<c:choose>
					<c:when test="${empleado.getId_hotel() == hotel.getId() }">
						<td>${hotel.getNombre() }</td>
					</c:when>
				</c:choose>
			</c:forEach>
			
			
			<td>${empleado.getEstado() == 1 ? "Activo" : "Inactivo" }</td>
			<td><a href="${pageContext.request.contextPath}/admin/empleado/edit/${empleado.getId() }"><i class="fa fa-pencil-square-o"></i></a></td>
		</tr>
		</tr>
	</c:forEach>
</table>
</div>
<script >

$(".tabla-dinamica").bdt();

</script>
		
