<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1>Agregar Hotel </h1>
		
<p>${message}</p>

<form action="${pageContext.request.contextPath}/admin/hotel/add" method="POST">
	
	<label>Nombre:</label>
	<input type="text" id="nombre" name="nombre" required="required">
	<br>
	<label>Categor�a:</label>
	<input type="number"  id="categoria" name="categoria" value="1" min="1" max="5"> Estrella(s)
	<br>

	<input type="submit" value="Guardar" />
</form>