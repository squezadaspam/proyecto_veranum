<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<br>
<br>
<br>
<br>
<div class="text-center">
		<h1>Cambiar clave</h1>
		<p>Aqui usted puede cambiar su clave</p>
		<p>${message}</p>	
		
</div>
<section class="cambioClave-persona" >
	<div class="container">
		<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">	
		<form:form method="POST" id="cambiarClaveForm">
			<div class="col-md-6 col-sm-6">
			
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Contrase�a actual:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="claveVieja" name="claveVieja" required="required" />
							<form:errors path="claveVieja" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
					<div class="col-md-10 col-sm-10">
							<label>Contrase�a nueva:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="claveNueva" name="claveNueva"  required="required"/>
							<form:errors path="claveNueva" cssClass="error" />
						</div>
					</div>
					<br>
					<div class="input row">
					<div class="col-md-10 col-sm-10">
							<label>Repita la contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="claveNuevaRepetida" name="claveNuevaRepetida"  required="required"/>
							<form:errors path="claveNuevaRepetida" cssClass="error" />
						</div>
					</div>
					<br>	
					<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<input type="submit" id="CambiarClave" name="CambiarClave" value="Modificar clave" />
						</div>
					</div>
				</div>

				</div>     
			
		</form:form>
		</div>
		</div>

</section>

<script type="text/javascript">

var deferred = new $.Deferred(),
	promise = deferred.promise();
   $(document).ready(function() {

                $("#cambiarClaveForm").validate({
                submitHandler: function(form,e) {
				    promise.done( function(){form.submit();});
					e.preventDefault();
				    return ConfirmDialog('Atenci�n', '�Esta seguro que desea cambiar su clave?');
				},
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                        	claveVieja: 	 			{required: true},
                        	claveNueva: 	 			{required: true, formatoPass: true},
                        	claveNuevaRepetida: 	 	{required: true, equalTo : "#claveNueva"}
                        	
                        },
                        messages: {
                                claveVieja:		 {
                                        required: "Campo requerido"
                                },
                                claveNueva:		 {
                                        required: "Campo requerido"
                                },
                                claveNuevaRepetida:		 {
                                        required: "Campo requerido",
                                        equalTo: "No coinciden las claves"
                                }
                       }
                });
                //valida la contrase�a que tenga letra minuscula, mayuscula, numero, caracter especial y 8 digitos minimo
 			  $.validator.addMethod("formatoPass", function(value, element){
			  return this.optional(element) || /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value) ;
			 }, "La contrase�a debe tener al menos: <br>-Una letra mayuscula <br>-Una letra minuscula <br>-Un n�mero o un caracter especial <br>-Longitud minima de 8 caracteres");

	});
      
      function ConfirmDialog(title,message){
        var confirmdialog = $('<div></div>').appendTo('body')
        .html('<div><h6>'+message+'</h6></div>')
        .dialog({
          modal: true, title: title, zIndex: 10000, autoOpen: false,
          width: 'auto', resizable: false,
          buttons: {
            Si: function(){
              deferred.resolve();
              $(this).dialog("close");
            },
            No: function(){
              $(this).dialog("close");
            }
          },
          close: function(event, ui){
            $(this).remove();
            return false;
          }
        });
        return confirmdialog.dialog("open");
      }
</script>