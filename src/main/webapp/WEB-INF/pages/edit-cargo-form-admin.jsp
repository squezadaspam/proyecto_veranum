<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST"  action="${pageContext.request.contextPath}/admin/cargo/edit/${cargo.getId()}">
					<h1>Editar Cargo</h1>
					<br>
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
							<tr>
								<td><input type = "hidden"  name="id" value="${cargo.getId()}"  disabled/></td>
							
							</tr>
							<tr>
								<td>Nombre:</td>
								<td><input type = "text"  name="nombre" value="${cargo.getNombre()}" required//></td>
							
							</tr>
							<tr>
								<td>Descripción:</td>
								<td><input type = "text"  name="descripcion" value="${cargo.getDescripcion()}" required//></td>
							
							</tr>
							<tr>
								<td>Activo:</td>								
								<td>
									<input type = "hidden" id="estado" name="estado" value="${cargo.getEstado()}"/>
									<input type="checkbox" id="estadoCheck" name="estadoCheck" value="" />
   													
								</td>
							
							</tr>
							
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
				
<script>

$(document).ready(function(){
	if($("#estado").val()==0){
		$("#estadoCheck").attr('checked', false);
		$('#estadoCheck').val('0');
		$('#estado').val('0');
	} else {
		$("#estadoCheck").attr('checked', true);
		$('#estadoCheck').val('1');
		$('#estado').val('1');
	}
});

$("#estadoCheck").change(function() {
	
    if($("#estadoCheck").is(':checked')) {
    	$('#estadoCheck').val('1');
    	$('#estado').val('1');
    }else{
    	$('#estadoCheck').val('0');
    	$('#estado').val('0');
    }
});
</script>
				