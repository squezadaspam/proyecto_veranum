<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
var options = {
		symbol : "$",
		decimal : ",",
		thousand: ".",
		precision : 0,
		format: "%s%v"
	};
</script>
<section class="demo-2">
	<div class="content text-center">
		<h2>Detalle de la reserva</h2>
		<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 thumbnail">
			<c:choose>
			    <c:when test="${ reserva != null }">
					
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
					    	<label>C�digo: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled value="${reserva.getCodigo() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Ni�os: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="cantidadAdultos" id="cantidadAdultos" value="${reserva.getNinos() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Adultos: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="cantidadNinos" id="cantidadNinos" value="${reserva.getAdultos() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Fecha inicio: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${reserva.getFechaInicioFormateada() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Fecha Fin: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${reserva.getFechaFinFormateada() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Nombre: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled value="${nombre }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Precio: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled id="precio" value="">
				    	</div>
					</div>
					<br>
					<div class="row thumbnail col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Opciones </label>
				    	</div>
				    	<br>
				    	<div class="col-sm-2 col-md-2">
				    		<label>Estado</label>
					    	<input type = "hidden" id="estado" name="estado" value="${reserva.getEstado()}"/>
							<input type="checkbox" onClick="submitForm(${reserva.getId()})" id="estadoCheck" name="estadoCheck" value="" />
				    	</div>
				    	<div class="col-sm-2 col-md-2">
				    		<label>Extender</label>
					    	<element title="Extender"><a href="${pageContext.request.contextPath}/admin/reserva/extender/${reserva.getId()}"><i class="fa fa-pencil-square-o"></i></a></element>
				    	</div>
					</div>
			    </c:when>
			    <c:otherwise>
			        <label>Problemas al cargar la reserva. Porfavor intentelo mas tarde. </label>
			    </c:otherwise>
			</c:choose>
		</div>
	</div>
</section>
<form method="POST" id="cancelarForm" action="">
</form>
<script>


$(document).ready(function(){
	$("#precio").val(accounting.formatMoney("${reserva.getPrecio() }", options));
	if($("#estado").val()==0){
		$("#estadoCheck").attr('checked', false);
		$('#estadoCheck').val('0');
		$('#estado').val('0');
	} else {
		$("#estadoCheck").attr('checked', true);
		$('#estadoCheck').val('1');
		$('#estado').val('1');
	}
	
	$(document).keydown(function(e) { if (e.keyCode == 8 || e.keyCode == 46) $('input').focus(); });
	desabilitaEdicionManualDeInputs();
});

$("#estadoCheck").change(function() {
	
    if($("#estadoCheck").is(':checked')) {
    	$('#estadoCheck').val('1');
    	$('#estado').val('1');
    }else{
    	$('#estadoCheck').val('0');
    	$('#estado').val('0');
    }
});

function submitForm(id){
	var url = "${pageContext.request.contextPath}/admin/reserva/cambiar-estado/"+id;
	$("#cancelarForm").attr("action", url);
	$("#cancelarForm").submit();
}

function desabilitaEdicionManualDeInputs(){
	$("#fechaInicio").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaInicio").keypress(function(e){
		e.preventDefault();
	});
	$("#fechaFin").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaFin").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").keypress(function(e){
		e.preventDefault();
	});
	$("#codigo").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#codigo").keypress(function(e){
		e.preventDefault();
	});
	$("#idUsuario").bind("cut copy paste", function(e){
		e.preventDefault();
	});
	$("#idUsuario").keypress(function(e){
		e.preventDefault();
	});
	$("#Precio").bind("cut copy paste", function(e){
		e.preventDefault();
	});
	$("#Precio").keypress(function(e){
		e.preventDefault();
	});
}
</script>