<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<section class="demo-2">

	<div class="container text-center">
		<div class="container contact-info">
			<div class="row">
				<div class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
					<c:choose>
			    		<c:when test="${ title != null || !title.equals('') }">
							<h1>${title}</h1>
						</c:when>
						<c:otherwise>
					        <h1>Realizado.</h1>
					    </c:otherwise>	
					</c:choose>
					<c:choose>
			    		<c:when test="${ message != null || !message.equals('') }">
							<p>${message}</p>
						</c:when>
						<c:otherwise>
					        <p>Transacción realizada exitosamente. </p>
					    </c:otherwise>	
					</c:choose>				
				</div>
			</div>
		</div>
	</div>
</section>