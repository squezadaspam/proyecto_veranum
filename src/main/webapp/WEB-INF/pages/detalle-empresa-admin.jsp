<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="content text-center">
	<h2>Detalle de la Empresa</h2>
	<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 thumbnail">
	<c:set var="cont" value = "1" />
		<c:forEach var="empresa" items="${empresa}">
		<c:if test="${cont == 1 }">
			<c:choose>
			    <c:when test="${ empresa[0].toString().trim().equals(id.toString().trim()) }">
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
					    	<label>id: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled value="${empresa[0]}">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Nombre: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[1] }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Email: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[2] }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Nombre Calle: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[3] }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Codigo Postal: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[4] }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Numero Depto: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[5] }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Numero de Casa: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${empresa[6] }">
				    	</div>
					</div>
					
					<c:forEach var="listaTelefono" items="${listaTelefono}">
						<c:choose>
						    <c:when test="${ listaTelefono[0].toString().trim().equals(id.toString().trim()) }">
						    	<br>
								<div class="row">
							    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
										<label>Nombre telefono </label>
							    	</div>
							    	<div class="col-sm-4 col-md-4">
										<input disabled value="${listaTelefono[8] }">
							    	</div>
								</div>
								<br>
								<div class="row">
							    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
										<label>Numero telefono </label>
							    	</div>
							    	<div class="col-sm-4 col-md-4">
										<input disabled value="${listaTelefono[9] }">
							    	</div>
								</div>
						    </c:when>
					    </c:choose>
				    </c:forEach>
				    <c:set var="cont" value="2"/>
			    </c:when>
			    <c:otherwise>
			        
			    </c:otherwise>
			</c:choose>
			</c:if>
		</c:forEach>
		
	</div>
</div>
	
<script>
</script>