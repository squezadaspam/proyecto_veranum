<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1>Seleccione Cargo y Hotel para esta Persona: </h1>
	

<form action="${pageContext.request.contextPath}/admin/empleado/add-empleado" id="empleadoform" method="POST">
	
	<input name="id_persona" type="hidden" value="${persona.getId() }" />
	<label>Nombre:</label>
	<input name="nombrePersona" value="${persona.getNombre() }" disabled/>
	<br>
	<label>Apellido:</label>
	<input name="apellidoPersona" value="${persona.getApellido() }" disabled/>
	<br>
	<label>E-Mail:</label>
	<input name="emailPersona" value="${contacto.getEmail() }" disabled/>
	<br>
	<br>
	<br>
	<label>Cargo:</label>	
	<select name = "id_cargo" id = "id_cargo">
		<option value="0" > --- Select --- </option>
		
		<c:forEach items="${cargos}" var="cargo">
			<option value="<c:out value="${cargo.getId()}" />"><c:out value="${cargo.getNombre()}" /></option>
		</c:forEach>
	</select>
	<label>Hotel:</label>	
	<select name = "id_hotel" id = "id_hotel"> >
		<option value="0" > --- Select --- </option>
		
		<c:forEach items="${hoteles}" var="hotel">
			<option value="<c:out value="${hotel.getId()}" />"><c:out value="${hotel.getNombre()}" /></option>
		</c:forEach>
	</select>
	
	<input type="submit" value="Guardar" />
</form>

    <!-- 	VALIDACION DE FORMULARIO -->

<script type="text/javascript">


   $(document).ready(function() {

                $("#empleadoform").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                id_cargo: {valueNotEqualsCargo: "0"},
								id_hotel: {valueNotEqualsHotel: "0"},
      
                        },
                        messages: {
                               
                                run:		 {
                                        required: "Campo requerido"
                                }
                        }
                });
                //para seleccionar un elemento del combobox diferente al elemento 0
			 $.validator.addMethod("valueNotEqualsCargo", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un Cargo");
			 
			 $.validator.addMethod("valueNotEqualsHotel", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un Hotel");
			 
			 
			  //valida formato run persona
			  $.validator.addMethod("formatoRunPersona", function(value, element){
			  return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value) ;
			 }, "Formato de run invalido, EJ: 163660XX-X");
        });
        
  $('#personaForm').on('submit', function(e){
  	if(!VerificaRut($('#run').val())){
  		e.preventDefault();
  		$('#dialog').dialog({
  			title: 'Run no v�lido'
  		});
  		$('#dialog').html('Ingrese nuevamente el Run.');
  	}
  });
        
 function VerificaRut(rut) {
    if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
        var caracteres = new Array();
        var serie = new Array(2, 3, 4, 5, 6, 7);
        var dig = rut.toString().substr(rut.toString().length - 1, 1);
        rut = rut.toString().substr(0, rut.toString().length - 2);

        for (var i = 0; i < rut.length; i++) {
            caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
        }

        var sumatoria = 0;
        var k = 0;
        var resto = 0;

        for (var j = 0; j < caracteres.length; j++) {
            if (k == 6) {
                k = 0;
            }
            sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
            k++;
        }

        resto = sumatoria % 11;
        dv = 11 - resto;

        if (dv == 10) {
            dv = "K";
        }
        else if (dv == 11) {
            dv = 0;
        }

        if (dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase())
            return true;
        else
            return false;
    }
    else {
        return false;
    }
}
  
</script>




