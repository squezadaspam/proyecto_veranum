<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>$('.dynamic-table').bdt();</script>


<h1>Agregar habitacion</h1>
<br>

<form class="" method="POST" action="add">
			<h4><font color="red">${message}</font></h4>
					<br>
			<table class="dynamic-table">
				<tbody>
					<tr>
						<td>Numero:</td>
						<td><input type = "text"  name="numero" required/></td>
					</tr>
					<tr>
						<td>Estado:</td>
						<td><input type="radio" name="estado" value="1" checked>Disponible
							<input type="radio" name="estado" value="0">No Disponible</td>
					</tr>
					<tr>
						<td>Tipo:</td>
						<td>
							<select name="id_tipo_habitacion">
								<c:forEach items="${tipos}" var="tipos">
								<option value="${tipos.getId()}">${tipos.getTipo()}</option>
								</c:forEach>
							</select>
						</td>
					</tr>					
					<tr>
						<td><input type="submit" value="Agregar" /></td>
						<td></td>
					</tr>						
					
				</tbody>
			</table>
		</form>

<p><a href="${pageContext.request.contextPath}/index.html">Home page</a></p>
