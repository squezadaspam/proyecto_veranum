<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<form  method="POST"  action="${hab.getId()}">
					<h1>Actualizar Habitacion</h1>
					<br>
					<h4><font color="red">${message}</font></h4>
					<br>
					<table class="dynamic-table">
						<tbody>
							<tr>
								<td>Id:</td>
								<td><input type = "text"  name="id" value="${hab.getId()}"  disabled/></td>
							
							</tr>
							<tr>
								<td>Numero:</td>
								<td><input type = "text"  name="numero" value="${hab.getNumero()}" required//></td>
							
							</tr>
							<tr>
								<td>Estado:</td>								
								<td>
								
   									<c:if test='${hab.getEstado() == "1"}'>
                             			 <input type="radio" name="estado" value="1"     checked="checked"/>Disponible <input type="radio" name="estado" value="0" />No Disponible     
  									 </c:if>
   									<c:if test='${hab.getEstado() != "1"}'>
                             			 <input type="radio" name="estado" value="1" />Disponible <input type="radio" name="estado" value="0"     checked="checked"/>No Disponible     
  									 </c:if>					
																		
								</td>
							
							</tr>
							<tr>
								<td>Tipo:</td>
								<td>								
								<select name="id_tipo_habitacion">
									<c:forEach items="${tipos}" var="tipos" varStatus="status">
	            							<c:if test='${tipos.getId() != hab.getIdTipoHabitacion()}'>
                             		     			<option value="${tipos.getId()}">${tipos.getTipo()}</option> 
  						 					</c:if>	            						
											<c:if test='${tipos.getId() == hab.getIdTipoHabitacion()}'>
                             		     			<option value="${tipos.getId()}" selected>${tipos.getTipo()}</option> 
  						 					</c:if>	            						
	            					</c:forEach>	
  						 		</select>	            						    
								</td>						
							</tr>
							
	
							<tr>
									<td><input type="submit" value="Modificar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
<script>
$('.dynamic-table').bdt();
</script>
				