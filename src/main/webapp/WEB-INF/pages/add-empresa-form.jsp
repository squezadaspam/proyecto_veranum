<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<br>
<br>
<br>
<br>

<div class="text-center">
	<h1>Registro de empresa</h1>
	<p>${message}<br/>
</div>

<section class="registro-persona">
	<div class="container">
		<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">
			
			<form method="POST" id="empresaForm">
				
				<div class="col-md-6 col-sm-6">
				
				<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre empresa:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="nombreEmp" name="nombreEmp"   />
						</div>
					</div>
					<br>			
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre contacto:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="nombre" name="nombre"   />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Rut:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="rut" name="rut"/>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Email:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="email" name="email"/>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<button class="add_field_button">Agregar Telefono</button>
						</div>
						<br>
					
					    <div class="row">
					    	<div class="col-md-6 col-sm-6">
					    		<label>Nombre Telefono</label>
					    	</div>
					    	
					    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					    		<label>Numero Telefono</label>
					    	</div>
						    
					    </div>
					    <div class="input_fields_wrap">
						    <div class="row text-center">
						    	<div class="col-md-6 col-sm-6">
						    		<input type="text" size="20" name="NombreTelefono" placeholder="EJ: CASA" required="required">
						    	</div>
						    	
						    	<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
						    		<input type="text" name="NumeroTelefono" size="20" placeholder="EJ: +569 11111111" required="required">
						    	</div>
						    </div>
							<br>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Calle:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="calle" name="calle" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Codigo postal:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="codigo_postal" name="codigo_postal"/>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Numero de empresa:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_casa" name="num_casa" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre y/o numero de oficina :</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="num_depto" name="num_depto" placeholder="Opcional"/>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Ciudad:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="ciudad" name="ciudad"/>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Pais:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<select name = "pais" id="pais">
								<option value="0" > --- Select --- </option>
								
								<c:forEach items="${listadoPais}" var="pais">
									<option value="<c:out value="${pais.id}" />"><c:out value="${pais.nombre}" /></option>
								</c:forEach>
							</select>
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre Usuario:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type= "text" name = "nom_user" id = "nom_user" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type="password" name="contrase�a" id="contrase�a" />
						</div>
					</div>
					<br>
					<div class="input row">
					<div class="col-md-10 col-sm-10">
							<label>Repita la contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="claveRepetida" name="claveRepetida"  required="required"/>
						</div>
					</div>
				</div>
				
				<br>
				
				<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<input type="submit" id="add" name="add" value="Guardar" />
						</div>
					</div>
				</div>
		
			</form>
		</div>
	</div>
</section>
<script>
    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='row text-center'><div class='col-md-6 col-sm-6'><input type='text' size='20' name='NombreTelefono' placeholder='EJ: CASA'></div><div class='col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5'><input type='text' name='NumeroTelefono' size='20' placeholder='EJ: +569 11111111'></div><a href='#' class='remove_field'>Remove</a></div>"); //add input box
            //$(wrapper).append('<div><label>Nombre Telefono: </label> <input type="text" size="20" name="NumeroTelefono" placeholder="EJ: CASA"/> <label>Numero Telefono: </label> <input type="text" size="20" name="NombreTelefono" placeholder="EJ: +569 11111111"/> <a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;        
    })
});
 
</script>

     <!-- 	VALIDACION DE FORMULARIO -->

<script type="text/javascript">


   $(document).ready(function() {

                $("#empresaForm").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                nombre:   {required: true, minlength: 4, maxlength: 45, valueNotEquals: ""},
                                nombreEmp:   {required: true, minlength: 4, maxlength: 45, valueNotEquals: ""},
                                rut:    {required: true, formatoRutEmpresa : true},
                                calle: 	 {required: true, minlength: 4, maxlength: 100, valueNotEquals: ""},
                                codigo_postal:  	 {required: true, minlength: 4, maxlength: 10, valueNotEquals: ""},
                                email: 	 {required: true,  email: true},
                                num_casa: {required: true, number:true},
                                ciudad:  {required: true, minlength: 4, maxlength: 45, valueNotEquals: ""},
                                pais: {valueNotEqualsPais: "0"},
                                nom_user: 	 {required: true, minlength: 4, valueNotEquals: ""},
                                contrase�a: 	 {required: true, formatoPass: true, valueNotEquals: ""},
                                claveRepetida: {required: true, equalTo: "#contrase�a"},
                                NombreTelefono: {required: true, minlength: 4, valueNotEquals: ""},
                                NumeroTelefono: {required: true, formatoTelefono : true}
      
                        },
                        messages: {
                                nombre: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                nombreEmp: 	 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                email:		 {
                                        required: "Campo requerido",
                                        email:	  "Formato no valido"
                                },
                                calle:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 100 caracteres"
                                },
                                codigo_postal:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 10 caracteres"
                                },
                                rut:		 {
                                        required: "Campo requerido"
                                },
                                num_casa:		 {
                                        required: "Campo requerido",
                                        number: "Solo n�meros"
                                },
                                ciudad:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres",
                                        maxlength: "Debe tener maximo 45 caracteres"
                                },
                                nom_user:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                contrase�a:		 {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                claveRepetida:{
                                		required: "Campo requerido",
                                		equalTo: "No coinciden las claves"
                                },
                                NombreTelefono: {
                                        required: "Campo requerido",
                                        minlength: "Debe tener minimo 4 caracteres"
                                },
                                NumeroTelefono: {
                                        required: "Campo requerido"
                                },
                        }
                });
			$.validator.addMethod("valueNotEqualsPais", function(value, element, arg){
			  return arg != value;
			 }, "Seleccione un pais");
			 $.validator.addMethod("valueNotEquals", function(value, element, arg){
			  return arg != $.trim(value) && $.trim(value).length > 3;
			 }, "Campo requerido, debe tener minimo 4 caracteres");
			 $.validator.addMethod("formatoTelefono", function(value, element){
			  return this.optional(element) || /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test(value) ;
			 }, "Formato del telefono invalido, EJ: +569XXXXXXXX");
			  
			  $.validator.addMethod("formatoRutEmpresa", function(value, element){
			  return this.optional(element) || /([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{1}|[0-9]{9}-1|[0-9]{9})/.test(value) ;
			 }, "Formato de rut de la empresa invalido, EJ: XXX.XXX.XXX-X");
			 
 			  $.validator.addMethod("formatoPass", function(value, element){
			  return this.optional(element) || /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value) ;
			 }, "La contrase�a debe tener al menos: <br>-Una letra mayuscula <br>-Una letra minuscula <br>-Un n�mero o un caracter especial <br>-Longitud minima de 8 caracteres");

	 
        });
  
</script>

		