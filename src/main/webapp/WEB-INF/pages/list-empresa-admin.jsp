<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="title">
	<h2>Empresa</h2>
</div>
<table class="dynamic-table table table-condensed">
    <thead>
        <tr>
            <th>id usuario</th>
            <th>Nombre</th>
            <th>email</th>
            <th>calle</th>
            <th>Codigo Postal</th>
            <th>Numero oficina</th>
            <th>Numero de empresa</th>
            <th>Detalles</th>
        </tr>
    </thead>
    
   
    <tbody>
	<c:set var="cont" value="1"/>     
	<c:forEach var="empresa" items="${empresa}">
		<c:if test="${cont == 1}">
			<tr>
				<td>${empresa[0]}</td>
				<td>${empresa[1]}</td>
				<td>${empresa[2]}</td>
				<td>${empresa[3]}</td>
				<td>${empresa[4]}</td>
				<td>${empresa[5]}</td>
				<td>${empresa[6]}</td>
				<td><a href="${pageContext.request.contextPath}/admin/empresa/info/${empresa[0]}">Detalle</a></td>
			</tr>	
			<c:set var="cont" value="${empresa[0]}"/>
		</c:if>
		
		<c:if test="${cont != empresa[0]}">
			<tr>
				<td>${empresa[0]}</td>
				<td>${empresa[1]}</td>
				<td>${empresa[2]}</td>
				<td>${empresa[3]}</td>
				<td>${empresa[4]}</td>
				<td>${empresa[5]}</td>
				<td>${empresa[6]}</td>
				<td><a href="${pageContext.request.contextPath}/admin/empresa/info/${empresa[0]}">Detalle</a></td>
			</tr>	
			<c:set var="cont" value="${empresa[0]}"/>
		</c:if>
	</c:forEach>
	            
    </tbody>

</table>
<form id="cancelarForm" method="POST" action="${pageContext.request.contextPath}/admin/reserva/cancelar/${reserva.getId()}">
</form>
<script>
function submitForm(id){
	var url = "${pageContext.request.contextPath}/admin/reserva/cancelar/"+id;
	$("#cancelarForm").attr("action", url);
	$("#cancelarForm").submit();
}

$('.dynamic-table').bdt();
</script>  