<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1>Agregar Incidente </h1>
		
<p>${message}</p>

<form action="${pageContext.request.contextPath}/admin/incidente/add" id="incidenteForm" method="POST">
	
	<label>Descripción:</label>
	<input type="text" id="descripcion" name="descripcion" id="descripcion" required="required">
	<br>

	<label>Fecha de Suceso:</label>
	<input type="text" name="fecha_suceso" id="fecha_suceso" required="required" readonly />
	
	<label>Hotel:</label>
	<select name = "id_hotel" ID = "id_hotel" >
		<option value="0" > --- Select --- </option>
		
		<c:forEach items="${hoteles}" var="hotel">
			<option value="<c:out value="${hotel.getId()}" />"><c:out value="${hotel.getNombre()}" /></option>
		</c:forEach>
	</select>

	<input type="submit" value="Guardar" />
</form>




<script type="text/javascript">
    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='row text-center'><div class='col-md-6 col-sm-6'><input type='text' size='20' name='NombreTelefono' placeholder='EJ: CASA'></div><div class='col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5'><input type='text' name='NumeroTelefono' size='20' placeholder='EJ: +569 11111111'></div><a href='#' class='remove_field'>Remove</a></div>"); //add input box
            //$(wrapper).append('<div><label>Nombre Telefono: </label> <input type="text" size="20" name="NumeroTelefono" placeholder="EJ: CASA"/> <label>Numero Telefono: </label> <input type="text" size="20" name="NombreTelefono" placeholder="EJ: +569 11111111"/> <a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;        
    })
});
    
  $(function() {

	   $( "#fecha_suceso" ).datepicker({
	   changeMonth: true,
       changeYear: true,     
       yearRange:'-90:+0',
	   dateFormat: "yy-mm-dd",
	   maxDate: "0"
	});
	 
  });
</script>

    <!-- 	VALIDACION DE FORMULARIO -->

<script type="text/javascript">


   $(document).ready(function() {

                $("#incidenteForm").validate({
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
								id_hotel: {valueNotEqualsHotel: "0"},
      							fecha_suceso :{required: true, date:true} 
                        },
                        messages: {
                                		
                                fecha_suceso: { 
                                		required: "Campo requerido",
                                		date: "Ingrese una fecha valida"},
                                		
                             	descripcion: {
                                        required: "Campo requerido"
                                }
                        }
                });

			 
	 		
        });
        
 $.validator.addMethod("valueNotEqualsHotel", function(value, element, arg){
 return arg != value;
 }, "Seleccione un Hotel");

</script>
