<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<br>
<br>
<br>
<br>
<div class="text-center">
		<h1>Deshabilita tu usuario</h1>
		<p>Aqui usted puede deshabilitar su usuario</p>
		<p>${message}</p>
</div>		
<section class="cambioClave-persona">
	<div class="container">
		<div class="col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 thumbnail">	
			<form method="POST" class="desactivaUsuario">
			<div class="col-md-6 col-sm-6">
			
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Nombre:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "text" id="usuario" name="usuario" />
						</div>
					</div>
					<br>
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<label>Contrase�a:</label>
						</div>
						<br>
						<div class="col-md-10 col-sm-10">
							<input type = "password" id="pass" name="pass" />
						</div>
					</div>
					<br>
					<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 row">
					
					<div class="input row">
						<div class="col-md-10 col-sm-10">
							<input type="submit" id="disable" name="disable" value="Deshabilitar" />
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
var deferred = new $.Deferred(),
promise = deferred.promise();
$(document).ready(function() {

                $(".desactivaUsuario").validate({
                submitHandler: function(form,e) {
				    promise.done( function(){form.submit();});
					e.preventDefault();
				    return ConfirmDialog('Atenci�n', '�Esta seguro que desea desactivar su cuenta?');
				},
                        errorContainer: "#errores",
                        errorLabelContainer: "#errores ul",
                        wrapper: "li",
                        errorElement: "em",
                        rules: {
                                usuario:   {required: true},
                                pass:   {required: true} 
                        },
                        messages: {
                                usuario: 	 {
                                        required: "Campo requerido"
                                },
                                pass: 	 {
                                        required: "Campo requerido"
                                },
                        }
                });

        });

      
    function ConfirmDialog(title,message){
        var confirmdialog = $('<div></div>').appendTo('body')
        .html('<div><h6>'+message+'</h6></div>')
        .dialog({
          modal: true, title: title, zIndex: 10000, autoOpen: false,
          width: 'auto', resizable: false,
          buttons: {
            Si: function(){
              deferred.resolve();
              $(this).dialog("close");
            },
            No: function(){
              $(this).dialog("close");
            }
          },
          close: function(event, ui){
            $(this).remove();
            return false;
          }
        });
        return confirmdialog.dialog("open");
      }
</script>