<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>




		<h1>Pagina para agregar cargo adicional</h1>
		<p>Aca tu puedes agregar un nuevo Cargo</p>
		<p>${message}</p><br/>
		<div class="form-group">
              <form  method="POST"  action="${pageContext.request.contextPath}/admin/cargoAdicional/agregar.html" onsubmit="return validacion()">
					<table>
						<tbody>
							<tr>
								<td>Descripción:</td>
								<td><input type = "text" id="nombre" name="nombre" placeholder="Descripción" title="Una breve descripción" /></td>
							
							</tr>
							<tr>
								<td>Fecha Inicio:</td>
								<td>								
									<div class="input-group date">
										    <input type="text" class="form-control datepickerInicio " name="fechaInicio" id="datepickerInicio">
										    <div class="input-group-addon">
										        <span class="glyphicon glyphicon-th"></span>
										    </div>
										</div>      			
                    					
                    			</td>
							
							</tr>
							<tr>
								<td>Fecha Fin:</td>
								<td>		
								
								    <div class="input-group date">
										    <input type="text" class="form-control datepickerFin" name="fechaFin" id="datepickerFin" >
										    <div class="input-group-addon">
										        <span class="glyphicon glyphicon-th"></span>
										    </div>
									</div>
										    
								</td>
							
							</tr>
							<tr>
								<td>Factor:</td>
								<td><input type = "text" id="cargo" name="cargo" placeholder="Cargo a aplicar" title="Ejemplo: 0.3" /></td>
							
							</tr>
	
							<tr>
									<td><input type="submit" value="Guardar" /></td>
									
							</tr>
						</tbody>
					</table>
				</form>
		</div>
		


    <input type="hidden" id="maxDate" value="2016-12-31"/>
   <script>

	$(document).ready(function(){
		desabilitaEdicionManualDeFechas();
		$('.datepickerFin').prop("disable", true);
	});
	
	var maxFechaArray = $("#maxDate").val().split("-");
	var maxDate = new Date(maxFechaArray[0],maxFechaArray[1] - 1,maxFechaArray[2]);
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
	$('.datepickerInicio').datepicker({
		dateFormat: "dd/mm/yy",
		minDate: nowTemp,
		maxDate: maxDate,
		onClose: function( selectedDate, inst ) {          
		      //$(".datepickerFin").datepicker("option", "minDate", selectedDate);
			$(".datepickerFin").datepicker("option", "minDate", selectedDate),
			desabilitaFechaFin()
		}
	   
	});
	
	 $.datepicker.regional[ "fr" ];
   
	$(".datepickerFin").datepicker({
		dateFormat: "dd/mm/yy",
		maxDate: maxDate
	});
	
	function desabilitaFechaFin(){
		if($('.datepickerInicio').val() != ""){
			$('.datepickerFin').prop("disabled", false);
			$( ".datepickerFin" ).datepicker( "setDate", nowTemp );
			$( ".datepickerFin" ).datepicker( "show");
		} else {
			$('.datepickerFin').prop("disabled", false);
		}
	}
   
	function desabilitaEdicionManualDeFechas(){
		$(".datepickerInicio").bind("cut copy paste", function(e){
			e.preventDefault();
		});
		$(".datepickerInicio").keypress(function(e){
			e.preventDefault();
		});
		$(".datepickerFin").bind("cut copy paste", function(e){
			e.preventDefault();
		});
		$(".datepickerFin").keypress(function(e){
			e.preventDefault();
		});
	}
  
   </script>
   
   <script>
   
   function validacion() {
	   
	   valor = document.getElementById("nombre").value;
	   if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if( valor.length > 4 ) {
		 alert('Error en la longitud del factor, largo maximo 4. Ejemplo: 3.5 = 350% 0.95 = 95%');
	     return false;
	   }
	   
	   valor = document.getElementById("datepickerInicio").value;
	   if( valor == null || valor.length == 0 ) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("datepickerFin").value;
	   if( valor == null || valor.length == 0) {
		 alert('Rellene Todos los campos');
	     return false;
	   }
	   
	   valor = document.getElementById("cargo").value;
	   if(!/^([0-9])[.][0-9]*$/.test(valor) ) {
		   
		 alert('Error en el valor del cargo. Ejemplo: 0.9 0.95');
	     return false;
	     
	   }
	   
   }
   
   
   </script>
