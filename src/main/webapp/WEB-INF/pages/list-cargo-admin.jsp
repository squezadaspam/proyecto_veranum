<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1>Listar Cargos</h1>
</br>
<table class="tabla-dinamica table">
<header>
	<th>Nombre</th>
	<th>Descripción</th>
	<th>Estado</th>
	<th>Editar</th>
</header>
	<c:forEach items="${listadoCargos}" var="listadoCargos">
		<tr>
			<td>${listadoCargos.getNombre() }</td>
			<td>${listadoCargos.getDescripcion() }</td>
			<td>${listadoCargos.getEstado()==1 ? "Activado" : "Desactivado" }</td>
			<td><a href="${pageContext.request.contextPath}/admin/cargo/edit/${listadoCargos.getId() }"><i class="fa fa-pencil-square-o"></i></a></td>
		</tr>
	</c:forEach>
</table>
</div>
<script >

$(".tabla-dinamica").bdt();

</script>
		

