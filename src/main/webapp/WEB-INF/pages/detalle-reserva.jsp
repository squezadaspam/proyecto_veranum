<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="title">
	<h2>Mis reservas</h2>
</div>
<script>
var options = {
		symbol : "$",
		decimal : ",",
		thousand: ".",
		precision : 0,
		format: "%s%v"
	};
</script>
<section class="demo-2">
	<div class="content text-center">
		<h2>Detalle de la reserva</h2>
		<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 thumbnail">
			<c:choose>
			    <c:when test="${ reserva != null }">
					
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
					    	<label>C�digo: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled value="${reserva.getCodigo() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Ni�os: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="cantidadAdultos" id="cantidadAdultos" value="${reserva.getNinos() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Adultos: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="cantidadNinos" id="cantidadNinos" value="${reserva.getAdultos() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Fecha inicio: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${reserva.getFechaInicioFormateada() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Fecha Fin: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled value="${reserva.getFechaFinFormateada() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Tipo habitaci�n: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="tipoHabitacion" id="tipoHabitacion" value="${tipoHabitacion.getTipo() }">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Precio: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
							<input disabled name="precio" id="precio" value="">
				    	</div>
					</div>
					<br>
					<div class="row">
				    	<div class="col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2">
							<label>Nombre: </label>
				    	</div>
				    	<div class="col-sm-4 col-md-4">
					    	<input disabled value="${nombre }">
				    	</div>
					</div>
			    </c:when>
			    <c:otherwise>
			        <label>Error al cargar el c�digo </label>
			    </c:otherwise>
			</c:choose>
		</div>
	</div>
</section>

<script>
$(document).ready(function(){
	$(document).keydown(function(e) { if (e.keyCode == 8 || e.keyCode == 46) $('input').focus(); });
	desabilitaEdicionManualDeInputs();
	$("#precio").val(accounting.formatMoney("${reserva.getPrecio() }", options));
});

function desabilitaEdicionManualDeInputs(){
	$("#fechaInicio").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaInicio").keypress(function(e){
		e.preventDefault();
	});
	$("#fechaFin").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#fechaFin").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadAdultos").keypress(function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#cantidadNinos").keypress(function(e){
		e.preventDefault();
	});
	$("#codigo").bind("cut copy paste delete", function(e){
		e.preventDefault();
	});
	$("#codigo").keypress(function(e){
		e.preventDefault();
	});
	$("#idUsuario").bind("cut copy paste", function(e){
		e.preventDefault();
	});
	$("#idUsuario").keypress(function(e){
		e.preventDefault();
	});
}
</script>