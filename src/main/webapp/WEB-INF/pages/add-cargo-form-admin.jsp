<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<h1>Agregar Cargos</h1>

<p>${message}</p>

<div class="form-group">

	<form:form method="POST" commandName="cargo">
		Nombre del Cargo:
		<input type="text" id="nombre" name="nombre" required="required">
		<form:errors path="nombre"></form:errors>
		
		Descripción:
		<input type="text" id="descripcion" name="descripcion" required="required">
		<form:errors path="descripcion"></form:errors>
										
		<input type="submit" value="Guardar" />
			
	</form:form>
</div>


