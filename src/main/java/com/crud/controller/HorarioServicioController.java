package com.crud.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.DetalleServicio;
import com.crud.model.HorarioServicio;

import com.crud.service.HorarioServicioService;


@Controller
public class HorarioServicioController {
	
	@Autowired
	private HorarioServicioService horarioServicioService;
	
	@RequestMapping(value="/admin/horarioServicio/add", method=RequestMethod.GET )
	public ModelAndView addHorarioServicioPage(){
		
		ModelAndView modelAndView = new ModelAndView("add-horarioServicio-form-admin");
	
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/horarioServicio/add", method=RequestMethod.POST )
	public ModelAndView edditingHorarioServicio(@RequestParam("fecha_inicio") String fechainicio,
												@RequestParam("fecha_fin") String fechafin){
		
		ModelAndView modelAndView = new ModelAndView("add-horarioServicio-form-admin");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
		HorarioServicio horarioServicio = new HorarioServicio();
		LocalDateTime in =  LocalDateTime.parse(fechainicio, formatter);
		LocalDateTime fn =  LocalDateTime.parse(fechafin, formatter);
		
		horarioServicio.setHora_Inicio(in);
		horarioServicio.setHora_Fin(fn);
			
		horarioServicioService.addHorarioServicio(horarioServicio);
			
		String message = "horario servicio agregado correctamente";
		modelAndView.addObject("message",message);
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="/admin/horarioServicio/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editHorarioServicioPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-horarioServicio-form-admin");
		
		int intId = Integer.parseInt(id);
		HorarioServicio horarioServicio = horarioServicioService.getHorarioServicioInt(intId);
		
		modelandview.addObject("horarioServicio", horarioServicio);
		
		return modelandview;
	}
									  
	
	@RequestMapping(value="/admin/horarioServicio/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edditingHorarioServicio(@PathVariable("id") String id,
												@RequestParam("fecha_inicio") String fechainicio,
												@RequestParam("fecha_fin") String fechafin){
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
		int idParsed = Integer.parseInt(id.trim());
	
		ModelAndView modelAndView = new ModelAndView("edit-horarioServicio-form-admin");
		
		HorarioServicio horarioServicio = horarioServicioService.getHorarioServicio(idParsed);
		LocalDateTime in =  LocalDateTime.parse(fechainicio, formatter);
		LocalDateTime fn =  LocalDateTime.parse(fechafin, formatter);
		
		horarioServicio.setHora_Inicio(in);
		horarioServicio.setHora_Fin(fn);
		
		horarioServicioService.updateHorarioServicio(horarioServicio);
		
		String message = "horario servicio Modificado";
		
		modelAndView.addObject("message",message);
		
		return modelAndView;
	}

	@RequestMapping(value="/admin/horarioServicio/list", method=RequestMethod.GET)
	public ModelAndView listHorarioServicioPage(){
		
		ModelAndView modelandview = new ModelAndView("list-horarioServicio-form-admin");
		
		List<HorarioServicio> listadoHorarioServicio = horarioServicioService.getHorarioServicio();
		
		modelandview.addObject("listadoHorarioServicio", listadoHorarioServicio);
		
		return modelandview;
	}
}