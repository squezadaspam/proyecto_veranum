package com.crud.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Cargo;
import com.crud.service.CargoService;


@Controller
@RequestMapping(value="/admin/cargo")
@SessionAttributes("NomUser")
public class CargoController {

	@Autowired
	private CargoService cargoService;
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView addCargoPage(){
		
		ModelAndView modelandview = new ModelAndView("add-cargo-form-admin");
		
		return modelandview;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ModelAndView addingCargo(@ModelAttribute("cargo") Cargo cargo){
		
		if(!cargoService.getCargoPorNombre(cargo.getNombre())){
			
			ModelAndView modelandview = new ModelAndView("add-cargo-form-admin");
			
			cargo.setEstado(1);
			cargoService.addCargo(cargo);
			
			modelandview.addObject("message", "El nuevo Cargo se insert� correctamente");
			
			return modelandview;
			
		}else{
			ModelAndView modelAndView = new ModelAndView("add-cargo-form-admin");
			
			String message = "Este cargo ya existe, intente con otro nombre";
			modelAndView.addObject("message",message);
			
			return modelAndView;
		}
	}
	
	
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView listCargoPage(){
		
		ModelAndView modelandview = new ModelAndView("list-cargo-admin");
		
		List<Cargo> listadoCargo = cargoService.getCargos();
		
		modelandview.addObject("listadoCargos", listadoCargo);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editCargoPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-cargo-form-admin");
		
		int intId = Integer.parseInt(id);
		Cargo cargo = cargoService.getCargoInt(intId);
		
		modelandview.addObject("cargo", cargo);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editingCargoPage(@PathVariable("id") String id,
										 @RequestParam("nombre") String nombre,
										 @RequestParam("descripcion") String descripcion,
										 @RequestParam("estado") String estado){
		
		ModelAndView modelandview= new ModelAndView("edit-cargo-form-admin");
		
		int idParsed = Integer.parseInt(id);
		int estadoParsed = Integer.parseInt(estado);
		Cargo cargo = cargoService.getCargoInt(idParsed);
		
		try{
			
			cargo.setNombre(nombre);
			cargo.setDescripcion(descripcion);
			cargo.setEstado(estadoParsed);
			cargoService.updateCargo(cargo);
			
			modelandview = editCargoPage(id);
			
			modelandview.addObject("message", "Cargo actualizado");
		}catch(Exception ex){	
			
			modelandview = editCargoPage(id);
			modelandview.addObject("message", "No se pudo actualizar el cargo");
		}		
		return modelandview;
	}
}
