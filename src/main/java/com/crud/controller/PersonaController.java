package com.crud.controller;



import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.mapping.Fetchable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.dao.UsuarioDAO;
import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empresa;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.model.Usuario;
import com.crud.service.UsuarioService;

import oracle.sql.DATE;

@Controller
@SessionAttributes("NomUser")
public class PersonaController {

	@Autowired
	private UsuarioService usuarioService;
	
	String titulo = "";
	String mensaje = "";
		
	@RequestMapping(value="/persona/registro", method=RequestMethod.GET)
	public ModelAndView addPersonaPage(@ModelAttribute("NomUser") Usuario verificaSession){
		
		ModelAndView modelandview = new ModelAndView("add-persona-form");
		
		if(!verificaSession.getNom_user().equals("Invitado")){
			
			titulo = "Lo sentimos";
			mensaje = "Usted ya tiene una cuenta";
			
			return redireccionamiento(titulo, mensaje, "reserva-fail");
		}
		
		List<Pais> pais = usuarioService.getListadoPais();
		
		modelandview.addObject("listadoPais", pais);
		
		return modelandview;
		
	}
	
	@RequestMapping(value="/persona/registro", method=RequestMethod.POST)
	public ModelAndView addingPersona(@RequestParam("nombre") String nombre,
									  @RequestParam("apellido") String apellido,
									  @RequestParam("run") String run,
									  @RequestParam("contraseña") String contraseña,
									  @RequestParam("nom_user") String nom_user,
									  @RequestParam("pais") String idPais,
									  @RequestParam("ciudad") String ciudad,
									  @RequestParam("num_depto") String num_depto,
									  @RequestParam("num_casa") String num_casa,
									  @RequestParam("codigo_postal") String codigo_postal,
									  @RequestParam("calle") String calle,
									  @RequestParam("email") String email,
									  @RequestParam("fec_nac") String fec_nac,
									  @RequestParam("NumeroTelefono") String[] NumeroTelefono,
									  @RequestParam("NombreTelefono") String[] NombreTelefono){
		
		if(!usuarioService.NombreUsuario(nom_user.trim())){
			
				ModelAndView modelAndView = new ModelAndView("index");
				
				//agregamos los atributos al objeto Usuario
				Usuario usuario =  new Usuario();
				usuario.setEstado(1);
				usuario.setId_rol(2);
				usuario.setNom_user(nom_user.trim());
				
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String contraseñaHash = passwordEncoder.encode(contraseña);
				
				usuario.setPass(contraseñaHash);
				usuarioService.addUsuario(usuario);
									
				Ciudad objCiudad = new Ciudad();
				objCiudad.setId_pais(Integer.parseInt(idPais));
				objCiudad.setNombre(ciudad.trim());
				usuarioService.addCiudad(objCiudad);
				
				Direccion direccion = new Direccion();
				direccion.setCalle(calle.trim());
				direccion.setCod_postal(codigo_postal.trim());
				direccion.setId_ciudad(objCiudad.getId());
				direccion.setNum_depto(num_depto.trim());
				direccion.setNumero(Integer.parseInt(num_casa));
				usuarioService.addDireccion(direccion);
						
				Contacto contacto= new Contacto();
				contacto.setEmail(email);
				contacto.setNombre(nombre.trim());
				contacto.setId_direccion(direccion.getId());
				contacto.setId_usuario(usuario.getId());
				usuarioService.addContacto(contacto);
				
				//Ingreso de telefonos
				for (int i = 0; i < NombreTelefono.length; i++) {
					Telefono objTelefono = new Telefono();
					objTelefono.setNombre(NombreTelefono[i].trim());
					objTelefono.setNumero(NumeroTelefono[i]);
					objTelefono.setEstado(1);
					objTelefono.setId_contacto(contacto.getId());
					usuarioService.addTelefono(objTelefono);
				}
				
				Persona persona = new Persona();
				persona.setId_contacto(contacto.getId());
				persona.setApellido(apellido.trim());
				persona.setNombre(nombre.trim());
				persona.setRun(run);
				persona.setFec_nac(Date.valueOf(fec_nac));
				usuarioService.addUsuarioPersona(persona);
				
				mensaje = "Usuario ingresado correctamente";
				titulo = "Bienvenido";
				
				return redireccionamiento(titulo, mensaje, "success");
		}else{
			
			mensaje = "El nombre de usuario ya existe";
			titulo = "Lo sentimos,";
			
			return redireccionamiento(titulo, mensaje, "reserva-fail");
		}
	}
	
	
	//Inicia la session con un valor por defecto (esto evita errores)
	@ModelAttribute("NomUser")
	public Usuario populateForm(){
		
		Usuario user = new Usuario();
		user.setNom_user("Invitado");
		
	    return user; 
	}
	
	/************ M O D I F I C A R ***********/
	
	@RequestMapping(value="/persona/perfil", method=RequestMethod.GET)
	public ModelAndView editPersonaPage(@ModelAttribute("NomUser") Usuario usuario){
	
		ModelAndView modelAndView = new ModelAndView("edit-persona-form");	
		
		Contacto cont = usuarioService.getContacto(usuario.getNom_user());
		Pais pais = usuarioService.getPais(usuario.getNom_user());
		Persona per = usuarioService.getPersona(usuario.getNom_user());
		Ciudad ciudad = usuarioService.getCiudad(usuario.getNom_user());
		Direccion direccion= usuarioService.getDireccion(usuario.getNom_user());
		List<Telefono> ListadaTelefono = usuarioService.getTelefono(usuario.getNom_user());
		Usuario user = usuarioService.getUsuario(usuario.getNom_user());
		List<Pais> paises = usuarioService.getListadoPais();
		
		modelAndView.addObject("nombre", cont.getNombre());
		modelAndView.addObject("apellido", per.getApellido());
		modelAndView.addObject("email", cont.getEmail());
		modelAndView.addObject("run", per.getRun());
		modelAndView.addObject("fec_nac", per.getFec_nac());
		modelAndView.addObject("listaTelefono", ListadaTelefono);
		modelAndView.addObject("calle", direccion.getCalle());
		modelAndView.addObject("codigoPostal",direccion.getCod_postal());
		modelAndView.addObject("numCasa", direccion.getNumero());
		modelAndView.addObject("numDepto", direccion.getNum_depto());
		modelAndView.addObject("ciudad", ciudad.getNombre());
		modelAndView.addObject("listadoPais", paises);
		modelAndView.addObject("paisActual", pais);
		modelAndView.addObject("nom_user", user.getNom_user());
		
		return modelAndView;
	}
	
	@RequestMapping(value="/persona/perfil", method=RequestMethod.POST)
	public ModelAndView edditingPersona(@ModelAttribute("NomUser") Usuario usuario,
									    @RequestParam("telefono") String[] telefono,
									  	@RequestParam(value = "NumeroTelefonoNuevo", required=false) String[] NumeroTelefonoNuevo,
									    @RequestParam(value = "NombreTelefonoNuevo", required=false) String[] NombreTelefonoNuevo,
										@RequestParam(value = "ChbTelefono", required=false) String[] ChbTelefono,
										@RequestParam("nombre") String nombre,
										@RequestParam("apellido") String apellido,
										@RequestParam("pais") String idPais,
										@RequestParam("ciudad") String ciudad,
										@RequestParam("num_depto") String num_depto,
									    @RequestParam("num_casa") String num_casa,
									    @RequestParam("codigo_postal") String codigo_postal,
									    @RequestParam("calle") String calle,
									    @RequestParam("email") String email,
									    @RequestParam("nom_user") String userName){
		
		Contacto ObjContacto = usuarioService.getContacto(usuario.getNom_user());
		Pais ObjPais = usuarioService.getPais(usuario.getNom_user());
		Ciudad ObjCiudad = usuarioService.getCiudad(usuario.getNom_user());
		Direccion ObjDireccion= usuarioService.getDireccion(usuario.getNom_user());
		List<Telefono> ObjTelefono = usuarioService.getTelefono(usuario.getNom_user());
		Usuario ObjUsuario = usuarioService.getUsuario(usuario.getNom_user());
		Persona ObjPersona = usuarioService.getPersona(usuario.getNom_user());
		
		Integer CantidadNumerosNuevos = 0;
		
		if(NumeroTelefonoNuevo != null && NombreTelefonoNuevo != null){
			
			if(NumeroTelefonoNuevo.length > 0 && NombreTelefonoNuevo.length > 0){
				
				for (int i = 0; i < NombreTelefonoNuevo.length; i++) {
					
					if(!NombreTelefonoNuevo[i].trim().isEmpty() && !NumeroTelefonoNuevo[i].trim().isEmpty()){
						
						Telefono objTelefono = new Telefono();
						objTelefono.setNombre(NombreTelefonoNuevo[i].trim());
						objTelefono.setNumero(NumeroTelefonoNuevo[i]);
						objTelefono.setEstado(1);
						objTelefono.setId_contacto(ObjContacto.getId());
						usuarioService.addTelefono(objTelefono);
						CantidadNumerosNuevos++;
					}
				}
			}
		}
		
		for (int i = 0; i < ObjTelefono.size(); i++){
			
			if(ChbTelefono != null){
				
				if((ChbTelefono.length < telefono.length) || (CantidadNumerosNuevos > 0)){
					
					for (int j = 0; j < ChbTelefono.length; j++) {
						
						if(ChbTelefono[j].equals(ObjTelefono.get(i).getNombre())){
							
							ObjTelefono.get(i).setEstado(0);
							usuarioService.updateTelefono(ObjTelefono.get(i), usuario.getNom_user());
							break;
						}
					}
				}else{
					
					ModelAndView modelandView = editPersonaPage(usuario);
					modelandView.addObject("message","NO puede eliminar todos los telefonos");
					
					return modelandView;
				}
			}
			if(!telefono[i].equals(ObjTelefono.get(i).getNumero())){
				ObjTelefono.get(i).setNumero(telefono[i]);
				usuarioService.updateTelefono(ObjTelefono.get(i), usuario.getNom_user());
			}
		}
		
		ObjPersona.setNombre(nombre.trim());
		ObjPersona.setApellido(apellido.trim());
		ObjDireccion.setCalle(calle.trim());
		ObjDireccion.setCod_postal(codigo_postal.trim());
		ObjDireccion.setNum_depto(num_depto.trim());
		ObjCiudad.setNombre(ciudad.trim());
		ObjDireccion.setNumero(Integer.parseInt(num_casa));
		ObjCiudad.setNombre(ciudad.trim());
		ObjCiudad.setId_pais(Integer.parseInt(idPais));
		ObjContacto.setEmail(email);
		
		boolean estado = usuarioService.updateUsuario(ObjUsuario, ObjContacto, ObjPais, ObjCiudad, ObjDireccion, ObjPersona);
		
		if(estado){
			
			mensaje = "Usuario modificado correctamente";
			titulo = "¡Exito!";
			
			return redireccionamiento(titulo, mensaje, "success");
			
		}else{
			
			titulo = "Lo sentimos";
			mensaje = "No se pudo Modificar, intentelo de nuevo";
			
			return redireccionamiento(titulo, mensaje, "reserva-fail");
		}
	}
	
	
	public ModelAndView redireccionamiento(String titulo, String mensaje, String pagina){
		
		ModelAndView modelandview = new ModelAndView(pagina);
		
		if(!titulo.isEmpty())
			
			modelandview.addObject("title", titulo);
		
		if(!mensaje.isEmpty())
			
			modelandview.addObject("message", mensaje);
		
		return modelandview;
	}
	
	@RequestMapping(value="/admin/persona/lista")
	public ModelAndView listPersonsPage(){
		
		ModelAndView modelAndView = new ModelAndView("list-persona-admin");
		
		List informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario(); 
		
		modelAndView.addObject("personas", informacionPersona);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/persona/info/{id}", method=RequestMethod.GET)
	public ModelAndView detallePersona(@PathVariable("id") String id){
		
		ModelAndView modelAndView = new ModelAndView("detalle-persona-admin");
		
		try {
			
			List informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario(); 
			
			modelAndView.addObject("personas", informacionPersona);
			modelAndView.addObject("id", id);
			modelAndView.addObject("listaTelefono", informacionPersona);
			
		} catch(Exception e){
			
			modelAndView.setViewName("index");
		}
		
		return modelAndView;
	}
}
