package com.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Cargo;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Hotel;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.service.UsuarioService;

@Controller
@RequestMapping(value="/admin/hotel")
@SessionAttributes("NomUser")
public class HotelController {

	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView addHotelPage(){
		
		ModelAndView modelandview = new ModelAndView("add-hotel-form-admin");
			
		return modelandview;
	}

	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ModelAndView addingHotelPage(@RequestParam("nombre") String nombre,
									    @RequestParam("categoria") String categoria){
		
		ModelAndView modelandview = new ModelAndView("add-hotel-form-admin");
		
		if(!usuarioService.getHotelPorNombre(nombre)){
			
			Hotel hotel = new Hotel();
			int categ = Integer.parseInt(categoria);
			
			hotel.setNombre(nombre);
			hotel.setCategoria(categ);
			usuarioService.addHotelUsuario(hotel);
			
			modelandview.addObject("message", "Hotel se insert� correctamente");
			
			return modelandview;
			
		}else{
			
			modelandview.addObject("message", "Este Hotel ya existe");
			
			return modelandview;
		}	
	}	
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView listHotelPage(){
		
		ModelAndView modelandview = new ModelAndView("list-hotel-admin");
		
		List<Hotel> listadoHotel = usuarioService.getListadoHoteles();
		
		modelandview.addObject("listadoHoteles", listadoHotel);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editHotelPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-hotel-form-admin");
		
		int intId = Integer.parseInt(id);
		Hotel hotel = usuarioService.getHotelUsuario(intId);
		
		modelandview.addObject("hotel", hotel);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editingCargoPage(@PathVariable("id") String id,
										 @RequestParam("nombre") String nombre,
										 @RequestParam("categoria") String categoria){
		
		ModelAndView modelandview= new ModelAndView("edit-hotel-form-admin");
		
		int idParsed = Integer.parseInt(id);
		int categ = Integer.parseInt(categoria);
		Hotel hotel = usuarioService.getHotelUsuario(idParsed);
		
		try{
			
			hotel.setNombre(nombre);
			hotel.setCategoria(categ);
			
			usuarioService.updateHotelUsuario(hotel);
			
			modelandview = editHotelPage(id);
			
			modelandview.addObject("message", "Hotel actualizado");
			
		}catch(Exception ex){	
			
			modelandview = editHotelPage(id);
			modelandview.addObject("message", "No se pudo actualizar el Hotel");
		}	
		
		return modelandview;
	}

}
