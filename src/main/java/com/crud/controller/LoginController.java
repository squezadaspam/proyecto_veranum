package com.crud.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empresa;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Rol;
import com.crud.model.Telefono;
import com.crud.model.Usuario;
import com.crud.service.UsuarioService;

@Controller
@RequestMapping(value="/")
@SessionAttributes("NomUser")
public class LoginController {
	
/************************ L O G I N **************************/	
	
	@Autowired
	private UsuarioService usuarioService;
	
	String titulo = "";
	String mensaje = "";
	
	@RequestMapping(value="/ingreso", method=RequestMethod.GET)
	  public ModelAndView login(){   
		
	      ModelAndView modelAndView = new ModelAndView("login");
		  return modelAndView;
	  }

	  @RequestMapping(value="/ingreso", method=RequestMethod.POST)
	  public ModelAndView login(@RequestParam("usuario") String usuario,
			  					@RequestParam("pass") String pass){
	  
		Usuario userLogin = usuarioService.login(usuario,pass); 
		
		//valida si encontro al usuario
		if(userLogin != null){
			
			if(userLogin.getId_rol() == 1 || userLogin.getId_rol() == 4){
				
				ModelAndView modelAndView = new ModelAndView("index-admin");
				modelAndView.addObject("NomUser", userLogin);
				
				return modelAndView;
			}else{
				
				ModelAndView modelAndView = new ModelAndView("index");
				modelAndView.addObject("NomUser", userLogin);
				
				return modelAndView;
			}
		}
		
		mensaje = "Usuario y/o contrase�a son invalidas";
		return redireccionamiento("", mensaje, "login");
	 }
	  
	  /************************ L O G O U T **************************/	 
	  
	  @RequestMapping(value="/salir", method=RequestMethod.GET)
	  public ModelAndView logout(ModelMap model, HttpSession session){   
		  
	      ModelAndView modelAndView = new ModelAndView();
	      
	      model.remove("NomUser");
	      session.removeAttribute("NomUser");
	      
	      modelAndView.setViewName("redirect:/index");
	      
	      return modelAndView;
	  }
	  
	  /************************ D E S H A B I L I T A R   U S U A R I O **************************/	
	  
	  
	  @RequestMapping(value="/dar-de-baja", method=RequestMethod.GET)
	  public ModelAndView disableUser(@ModelAttribute("NomUser") Usuario verificaSession){     
	      
	      if(!verificaSession.getNom_user().equals("Invitado")){
	    	  
	    	  ModelAndView modelAndView = new ModelAndView("disable-user");
	    	  return modelAndView;
	    	  
	      }
	      
	      titulo = "Lo sentimos";
	      mensaje = "Debe iniciar sesi�n";
	      
	      return redireccionamiento(titulo, mensaje, "reserva-fail");
	      
	  }
	  
	  @RequestMapping(value="/dar-de-baja", method=RequestMethod.POST)
	  public ModelAndView disableUser(@RequestParam("usuario") String usuario,
			  					      @RequestParam("pass") String pass,
			  					      @ModelAttribute("NomUser") Usuario userLog,
			  					      ModelMap model,
			  					      HttpSession session){
	  
		if(userLog.getNom_user().equals(usuario.trim())){  
			
			if(usuarioService.CambiarEstadoUsuario(usuario.trim(), pass)){
				
				ModelAndView modelAndView = new ModelAndView();
			      model.remove("NomUser");
			      session.removeAttribute("NomUser");
			      modelAndView.setViewName("redirect:/index");
			      return modelAndView;
			}
		}
		
		titulo = "�Atenci�n!";
		mensaje = "Usuario y/o  contrase�a invalidas";
		
		return redireccionamiento(titulo, mensaje, "reserva-fail");
	  }
	  
	  public ModelAndView redireccionamiento(String titulo, String mensaje, String pagina){
		  
		ModelAndView modelandview = new ModelAndView(pagina);
		
		if(!titulo.isEmpty())
			
			modelandview.addObject("title", titulo);
		
		if(!mensaje.isEmpty())
			
			modelandview.addObject("message", mensaje);
		
		return modelandview;
	  }
	  
	  @RequestMapping(value="/CambiarClave", method=RequestMethod.GET)
	  public ModelAndView getCambiarClave(@ModelAttribute("NomUser") Usuario usuario){
		  
		ModelAndView modelAndView = new ModelAndView("cambiar-clave");
		
		return modelAndView;
	  }
	  
	  @RequestMapping(value="/CambiarClave", method=RequestMethod.POST)
		public ModelAndView cambiarClave(@ModelAttribute("NomUser") Usuario usuario,
										 @RequestParam("claveVieja") String claveVieja,
										 @RequestParam("claveNueva") String claveNueva){
		  
		  BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		  
		  if(passwordEncoder.matches(claveVieja, usuario.getPass())){
			  
			usuario.setPass(claveNueva);
			
			if(usuarioService.cambiarClave(usuario)){
				
					titulo = "�Exito!";
					mensaje = "Contrase�a modificada correctamente";
					return redireccionamiento(titulo, mensaje, "success");
			}
		  }
		  
		titulo = "�Lo sentimos!";
		mensaje = "Clave actual incorrecta";
		
		return redireccionamiento(titulo, mensaje, "reserva-fail");
	  }
}
