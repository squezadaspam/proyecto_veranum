package com.crud.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empresa;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Rol;
import com.crud.model.Telefono;
import com.crud.model.Usuario;
import com.crud.service.UsuarioService;
import com.crud.service.UsuarioServiceImpl;

@Controller
@SessionAttributes("NomUser")
public class EmpresaController {
	
	/*************  A G R E G A R  ***********/
	
	@Autowired
	private UsuarioService usuarioService;
	
	String titulo = "";
	String mensaje = "";
	
	@RequestMapping(value="/empresa/registro", method=RequestMethod.GET)
	public ModelAndView addEmpresaPage(@ModelAttribute("NomUser") Usuario verificaSession){
		
		ModelAndView modelandview = new ModelAndView("add-empresa-form");
		
		if(!verificaSession.getNom_user().equals("Invitado")){
			
			titulo = "Lo sentimos";
			mensaje = "Usted ya tiene una cuenta";
			
			return redireccionamiento(titulo, mensaje, "reserva-fail");
		}
		
		List<Pais> pais = usuarioService.getListadoPais();
		modelandview.addObject("listadoPais", pais);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/empresa/registro", method=RequestMethod.POST)
	public ModelAndView addingEmpresa(@RequestParam("nombre") String nombre,
									  @RequestParam("nombreEmp") String nombreEmp,
									  @RequestParam("rut") String rut,
									  @RequestParam("contraseña") String contraseña,
									  @RequestParam("nom_user") String nom_user,
									  @RequestParam("pais") String idPais,
									  @RequestParam("ciudad") String ciudad,
									  @RequestParam("num_depto") String num_depto,
									  @RequestParam("num_casa") String num_casa,
									  @RequestParam("codigo_postal") String codigo_postal,
									  @RequestParam("calle") String calle,
									  @RequestParam("email") String email,
									  @RequestParam("NumeroTelefono") String[] NumeroTelefono,
									  @RequestParam("NombreTelefono") String[] NombreTelefono){
		
		if(!usuarioService.NombreUsuario(nom_user.trim())){
			
				ModelAndView modelAndView = new ModelAndView("index");
				
				//agregamos los atributos al objeto Usuario
				Usuario usuario =  new Usuario();
				usuario.setEstado(1);
				usuario.setId_rol(3);
				usuario.setNom_user(nom_user.trim());
				
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String contraseñaHash = passwordEncoder.encode(contraseña);
				
				usuario.setPass(contraseñaHash);
				usuarioService.addUsuario(usuario);
				
				Ciudad objCiudad = new Ciudad();
				objCiudad.setId_pais(Integer.parseInt(idPais));
				objCiudad.setNombre(ciudad.trim());
				usuarioService.addCiudad(objCiudad);
				
				Direccion direccion = new Direccion();
				direccion.setId_ciudad(objCiudad.getId());
				direccion.setCalle(calle.trim());
				direccion.setCod_postal(codigo_postal.trim());
				direccion.setNum_depto(num_depto.trim());
				direccion.setNumero(Integer.parseInt(num_casa));
				
				usuarioService.addDireccion(direccion);
				
				Contacto contacto= new Contacto();
				contacto.setEmail(email);
				contacto.setNombre(nombre.trim());
				contacto.setId_direccion(direccion.getId());
				contacto.setId_usuario(usuario.getId());
				usuarioService.addContacto(contacto);
				
				//esto agrega X cantidad de telefono
				for (int i = 0; i < NombreTelefono.length; i++) {
					if(!NombreTelefono[i].trim().isEmpty() && !NumeroTelefono[i].trim().isEmpty()){
						Telefono objTelefono = new Telefono();
						objTelefono.setNombre(NombreTelefono[i].trim());
						objTelefono.setNumero(NumeroTelefono[i]);
						objTelefono.setEstado(1);
						objTelefono.setId_contacto(contacto.getId());
						usuarioService.addTelefono(objTelefono);
					}
				}
										
				Empresa empresa =  new Empresa();
				empresa.setId_contacto(contacto.getId());
				empresa.setRut(rut);
				empresa.setNombre(nombreEmp.trim());
				usuarioService.addUsuarioEmpresa(empresa);
				
				mensaje = "Usuario ingresado correctamente";
				titulo = "Bienvenido";
				
				return redireccionamiento(titulo, mensaje, "success");
			}else{
				titulo = "Lo sentimos,";
				mensaje = "El nombre de usuario ya existe";
				
				return redireccionamiento(titulo, mensaje, "reserva-fail");
			}
	}
	
	/************ M O D I F I C A R ***********/
	
	//Inicia la session con un valor por defecto (esto evita errores)
	@ModelAttribute("NomUser")
	public Usuario populateForm(){
		
		Usuario user = new Usuario();
		user.setNom_user("Invitado");
		
	    return user; 
	}
	
	@RequestMapping(value="/empresa/perfil", method=RequestMethod.GET)
	public ModelAndView editEmpresaPage(@ModelAttribute("NomUser") Usuario usuario){

			ModelAndView modelAndView = new ModelAndView("edit-empresa-form");
				
			Contacto cont = usuarioService.getContacto(usuario.getNom_user());
			Pais pais = usuarioService.getPais(usuario.getNom_user());
			Empresa emp = usuarioService.getEmpresa(usuario.getNom_user());
			Ciudad ciudad = usuarioService.getCiudad(usuario.getNom_user());
			Direccion direccion= usuarioService.getDireccion(usuario.getNom_user());
			List<Telefono> listadoTelefono = usuarioService.getTelefono(usuario.getNom_user());
			Usuario user = usuarioService.getUsuario(usuario.getNom_user());
			List<Pais> paises = usuarioService.getListadoPais();
			
			modelAndView.addObject("nombre", cont.getNombre());
			modelAndView.addObject("nombreEmp", emp.getNombre());
			modelAndView.addObject("email", cont.getEmail());
			modelAndView.addObject("rut", emp.getRut());
			modelAndView.addObject("listaTelefono", listadoTelefono);
			modelAndView.addObject("calle", direccion.getCalle());
			modelAndView.addObject("codigoPostal",direccion.getCod_postal());
			modelAndView.addObject("numCasa", direccion.getNumero());
			modelAndView.addObject("numDepto", direccion.getNum_depto());
			modelAndView.addObject("ciudad", ciudad.getNombre());
			modelAndView.addObject("listadoPais", paises);
			modelAndView.addObject("paisActual", pais);
			modelAndView.addObject("nom_user", user.getNom_user());
			
			return modelAndView;
	}
			
	@RequestMapping(value="/empresa/perfil", method=RequestMethod.POST)
	public ModelAndView edditingEmpresa(@ModelAttribute("NomUser") Usuario usuario,
									    @RequestParam("telefono") String[] telefono,
									    @RequestParam(value = "NumeroTelefonoNuevo", required=false) String[] NumeroTelefonoNuevo,
									    @RequestParam(value = "NombreTelefonoNuevo", required=false) String[] NombreTelefonoNuevo,
									    @RequestParam(value = "ChbTelefono", required=false) String[] ChbTelefono,
									    @RequestParam("pais") String idPais,
									    @RequestParam("nombre") String nombre,
									    @RequestParam("ciudad") String ciudad,
									    @RequestParam("num_depto") String num_depto,
									    @RequestParam("num_casa") String num_casa,
									    @RequestParam("codigo_postal") String codigo_postal,
									    @RequestParam("calle") String calle,
									    @RequestParam("email") String email,
									    @RequestParam("nom_user") String userName){
		
			Contacto ObjContacto = usuarioService.getContacto(usuario.getNom_user());
			Pais ObjPais = usuarioService.getPais(usuario.getNom_user());
			Ciudad ObjCiudad = usuarioService.getCiudad(usuario.getNom_user());
			Direccion ObjDireccion= usuarioService.getDireccion(usuario.getNom_user());
			List<Telefono> ObjTelefono = usuarioService.getTelefono(usuario.getNom_user());
			Usuario ObjUsuario = usuarioService.getUsuario(usuario.getNom_user());
			
			Integer CantidadNumerosNuevos = 0;
			if(NumeroTelefonoNuevo != null && NombreTelefonoNuevo != null){
				
				if(NumeroTelefonoNuevo.length > 0 && NombreTelefonoNuevo.length > 0){
					
					for (int i = 0; i < NombreTelefonoNuevo.length; i++) {
						
						if(!NombreTelefonoNuevo[i].trim().isEmpty() && !NumeroTelefonoNuevo[i].trim().isEmpty()){
							
							Telefono objTelefono = new Telefono();
							objTelefono.setNombre(NombreTelefonoNuevo[i].trim());
							objTelefono.setNumero(NumeroTelefonoNuevo[i]);
							objTelefono.setEstado(1);
							objTelefono.setId_contacto(ObjContacto.getId());
							usuarioService.addTelefono(objTelefono);
							CantidadNumerosNuevos++;
						}
					}
				}
			}
			
			for (int i = 0; i < ObjTelefono.size(); i++) {
				
				if(ChbTelefono != null){
					
					if((ChbTelefono.length < telefono.length) || (CantidadNumerosNuevos > 0)){
						
						for (int j = 0; j < ChbTelefono.length; j++){
							
							if(ChbTelefono[j].equals(ObjTelefono.get(i).getNombre())){
								
								ObjTelefono.get(i).setEstado(0);
								usuarioService.updateTelefono(ObjTelefono.get(i), usuario.getNom_user());
								break;
							}
						}
					}else{
						
						ModelAndView modelandView = editEmpresaPage(usuario);
						modelandView.addObject("message","NO puede eliminar todos los telefonos");
						
						return modelandView;
					}
				}
				
				if(!telefono[i].equals(ObjTelefono.get(i).getNumero())){
					
					ObjTelefono.get(i).setNumero(telefono[i]);
					usuarioService.updateTelefono(ObjTelefono.get(i), usuario.getNom_user());
				}
			}

			ObjDireccion.setCalle(calle.trim());
			ObjDireccion.setCod_postal(codigo_postal.trim());
			ObjDireccion.setNum_depto(num_depto.trim());
			ObjCiudad.setNombre(ciudad.trim());
			ObjCiudad.setId_pais(Integer.parseInt(idPais));
			ObjDireccion.setNumero(Integer.parseInt(num_casa));
			ObjCiudad.setId_pais(Integer.parseInt(idPais));
			ObjContacto.setNombre(nombre.trim());
			ObjContacto.setEmail(email);
			
			boolean estado = usuarioService.updateUsuario(ObjUsuario, ObjContacto, ObjPais, ObjCiudad, ObjDireccion, null);
			
			if(estado){
				
				titulo = "¡Exito!";
				mensaje = "Usuario modificado correctamente";
				
				return redireccionamiento(titulo, mensaje, "success");
				
			}else{
				
				titulo = "Lo sentimos";
				mensaje = "No se pudo Modificar, intentelo de nuevo";
				
				return redireccionamiento(titulo, mensaje, "reserva-fail");
			}
	}
	
	
	@RequestMapping(value="/admin/empresa/lista")
	public ModelAndView listPersonsPage(){
		
		ModelAndView modelAndView = new ModelAndView("list-empresa-admin");
		
		List informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario(); 
		modelAndView.addObject("empresa", informacionEmpresa);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/empresa/info/{id}", method=RequestMethod.GET)
	public ModelAndView detallePersona(@PathVariable("id") String id){
		
		ModelAndView modelAndView = new ModelAndView("detalle-empresa-admin");
		
		try{
			
			List informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario(); 
			modelAndView.addObject("empresa", informacionEmpresa);
			modelAndView.addObject("id", id);
			modelAndView.addObject("listaTelefono", informacionEmpresa);
			
		} catch(Exception e){
			
			modelAndView.setViewName("index");
		}

		return modelAndView;
	}
	
	
	public ModelAndView redireccionamiento(String titulo, String mensaje, String pagina){
		
		ModelAndView modelandview = new ModelAndView(pagina);
		
		if(!titulo.isEmpty())
			
			modelandview.addObject("title", titulo);
		
		if(!mensaje.isEmpty())
			
			modelandview.addObject("message", mensaje);
		
		return modelandview;
	}
	
	

}
