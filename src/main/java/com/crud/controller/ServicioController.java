package com.crud.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.DetalleServicio;
import com.crud.model.Servicio;
import com.crud.service.DetalleServicioService;
import com.crud.service.*;
import com.crud.model.*;


import com.crud.service.ServicioService;



@Controller


public class ServicioController  {
	
	@Autowired
	private ServicioService ServicioService;
	
	@Autowired
	private DetalleServicioService ServicioDetalle;
	
	@Autowired
	private HorarioServicioService ServicioHorario;
	
	@RequestMapping(value="/admin/servicio/add", method=RequestMethod.GET)
	public ModelAndView addServicioPage(){
		
		ModelAndView modelandview = new ModelAndView("add-servicio-form-admin");
		
		List<Servicio> listaServicio =  ServicioService.getServicio();
		List<DetalleServicio> listaDetalle = ServicioDetalle.getDetalleServicio();
		List<HorarioServicio> listaHorario = ServicioHorario.getHorarioServicio();
		
		modelandview.addObject("listaServicio", listaServicio);
		modelandview.addObject("listaDetalle", listaDetalle);
		modelandview.addObject("listaHorario", listaHorario);
		
		return modelandview;
	}
	
	@RequestMapping(value="/admin/servicio/add", method=RequestMethod.POST)
	public ModelAndView addingServicio(@RequestParam("nombre") String nombre,
									   @RequestParam("estado") String estado,
									   @RequestParam("id_detalle_servicio") String id_detalle,
									   @RequestParam("id_horario_servicio") String id_horario){	
		
		Servicio ser = new Servicio();
		
		ModelAndView modelAndView = new ModelAndView("add-servicio-form-admin");	
		
		try{	
			
			int estadoParsed = Integer.parseInt(estado);
			ser.setNombre(nombre);
			ser.setEstado(estadoParsed);
			ser.setId_detalle_servicio(Integer.parseInt(id_detalle));
			ser.setId_horario_servicio(Integer.parseInt(id_horario));
			ServicioService.addServicio(ser);
			
			modelAndView.addObject("message","Servicio agregado");
			
		}catch(Exception ex){	
			
			modelAndView.addObject("message","Problemas al procesar la solicitud");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/servicio/edit/{id}", method=RequestMethod.GET )
	public ModelAndView editServicioPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-servicio-form-admin");
		
		int idParsed = Integer.parseInt(id.trim());
		Servicio ser =  ServicioService.getServicioInt(idParsed);
		List<DetalleServicio> listaDetalle = ServicioDetalle.getDetalleServicio();
		List<HorarioServicio> listaHorario = ServicioHorario.getHorarioServicio();
		
		modelandview.addObject("ser", ser);
		modelandview.addObject("listaDetalle", listaDetalle);
		modelandview.addObject("listaHorario", listaHorario);	
		
	    return modelandview;
	}
	
	@RequestMapping(value="/admin/servicio/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editServicioPage(@PathVariable("id") String id,
										 @RequestParam("nombre") String nombre,
										 @RequestParam("estado") String estado,
										 @RequestParam("id_detalle_servicio") String id_detalle_servicio,
										 @RequestParam("id_horario_servicio") String id_horario_servicio){		
		ModelAndView modelandview;	
		
		try{
			
			int idParsed = Integer.parseInt(id);
			int estadoParsed = Integer.parseInt(estado);
			Servicio ser = ServicioService.getServicio(idParsed);	
			
			ser.setNombre(nombre);
			ser.setEstado(estadoParsed);
			ser.setId_detalle_servicio(Integer.parseInt(id_detalle_servicio));
			ser.setId_horario_servicio(Integer.parseInt(id_horario_servicio));
			ServicioService.updateServicio(ser);	
			
			modelandview = editServicioPage(id);
			modelandview.addObject("message", "Servicio actualizado");
			
		}catch(Exception ex){	
			
			modelandview = editServicioPage(id);
			modelandview.addObject("message", "Problemas al procesar la solicitud");
		}
		
		return modelandview;
	}
	
	@RequestMapping(value="/admin/servicio/list", method=RequestMethod.GET)
	public ModelAndView listServicioPage(){
		
		ModelAndView modelandview = new ModelAndView("list-servicio-form-admin");	
		
		List<Servicio> listadoServicio = ServicioService.getServicio();
		
		modelandview.addObject("listadoServicio", listadoServicio);
		
		return modelandview;
	}
}

