package com.crud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Usuario;

@Controller
@SessionAttributes("NomUser")
public class WebAdminController {

	// m�todo da valor null por defecto a la session
	@ModelAttribute("NomUser")
	public Usuario populateForm(){
		
		Usuario user = new Usuario();
		user.setNom_user("Invitado");
		
	    return user;
	}
	
	@RequestMapping(value={"/admin", "/admin/index", "/admin/"})
	public ModelAndView mainPage(@ModelAttribute("NomUser") Usuario usuario){
		
		if(!usuario.getNom_user().equals("Invitado") && usuario.getId_rol() == 1 || usuario.getId_rol() == 4){
			
			ModelAndView modelandview = new ModelAndView("index-admin");
			modelandview.addObject("NomUser", usuario);
			
			return modelandview;
			
		}else{
			
			return redireccionamiento("","","index");
		}
	}
	
	
	public ModelAndView redireccionamiento(String titulo, String mensaje, String pagina){
		
		ModelAndView modelandview = new ModelAndView(pagina);
		
		if(!titulo.isEmpty())
			
			modelandview.addObject("title", titulo);
		
		if(!mensaje.isEmpty())
			
			modelandview.addObject("message", mensaje);
		
		return modelandview;
	}
}