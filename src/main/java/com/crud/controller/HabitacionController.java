package com.crud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Habitacion;
import com.crud.service.HabitacionService;
import com.crud.model.TipoHabitacion;
@Controller
@RequestMapping(value="/admin/habitacion")
public class HabitacionController {

	@Autowired
	private HabitacionService habitacion;
	
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView addHabitacionPage(){
		
		ModelAndView modelandview = new ModelAndView("add-habitacion-form-admin");
		
		List<TipoHabitacion> tipos = habitacion.getTipos();
		
		modelandview.addObject("tipos", tipos);
		
		return modelandview;
	}
	
	
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ModelAndView addingHabitacion(@RequestParam("numero") String numero,
										 @RequestParam("estado") String estado,
										 @RequestParam("id_tipo_habitacion") String id_tipo){	
		
		Habitacion hab = new Habitacion();
		
		ModelAndView modelAndView;		
		
		try{	
			int estadoParsed = Integer.parseInt(estado);
			hab.setNumero(Integer.parseInt(numero));
			hab.setEstado(estadoParsed);
			hab.setIdTipoHabitacion(Integer.parseInt(id_tipo));
			habitacion.addHabitacion(hab);		
			modelAndView = addHabitacionPage();
			modelAndView.addObject("message","Habitación agregada");
			
		}catch(Exception ex){
			
			modelAndView = addHabitacionPage();
			modelAndView.addObject("message","Problemas al procesar la solicitud");
		}
		
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView deletePersonaPage(@PathVariable("id") String id){	
		
		ModelAndView modelAndView;  
		
		try{
			
			int idDelete = Integer.parseInt(id);
			habitacion.deleteHabitacion(idDelete);		
			modelAndView = listHabitacionePage();
			modelAndView.addObject("message", "Habitación Eliminada");
			
		}catch(Exception ex){
			
			modelAndView = listHabitacionePage();
			modelAndView.addObject("message", "Problemas al procesar la solicitud");
		}	
		return modelAndView;				
	}
		
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editHabitacionPage(@PathVariable("id") String id,
										   @RequestParam("numero") String numero,
										   @RequestParam("estado") String estado,
										   @RequestParam("id_tipo_habitacion") String id_tipo_habitacion){	
		
		ModelAndView modelandview;	
		
		try{
			
			int idParsed = Integer.parseInt(id);
			int estadoParsed = Integer.parseInt(estado);
			Habitacion hab = habitacion.getHabitacion(idParsed);			
			hab.setNumero(Integer.parseInt(numero));
			hab.setEstado(estadoParsed);
			hab.setIdTipoHabitacion(Integer.parseInt(id_tipo_habitacion));			
			habitacion.updateHabitacion(hab);			
			modelandview = editHabForm(id);
			modelandview.addObject("message", "Habitación actualizada");
			
		}catch(Exception ex){	
			
			modelandview = editHabForm(id);
			modelandview.addObject("message", "Problemas al procesar la solicitud");
		}		
		return modelandview;
	}
	
	
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET )
	public ModelAndView editHabForm(@PathVariable("id") String id){
		
		ModelAndView modelAndView = new ModelAndView("edit-habitacion-form-admin");		
		
		int intId = Integer.parseInt(id);			
		Habitacion hab = habitacion.getHabitacion(intId);
		List<TipoHabitacion> tipos = habitacion.getTipos();
		
		modelAndView.addObject("hab", hab);
		modelAndView.addObject("tipos", tipos);		
		
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView listHabitacionePage(){
		
		ModelAndView modelandview = new ModelAndView("list-habitacion-admin");		
		
		List<Habitacion> lista = habitacion.listarHabitaciones();
		List<TipoHabitacion> tipos = habitacion.getTipos();
		
		modelandview.addObject("lista", lista);
		modelandview.addObject("tipos", tipos);
		
		return modelandview;
	}
	
	
	
}

