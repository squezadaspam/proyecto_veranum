package com.crud.controller;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Cargo;
import com.crud.model.Empleado;
import com.crud.model.Hotel;
import com.crud.model.Incidente;
import com.crud.model.Persona;
import com.crud.service.IncidenteService;
import com.crud.service.UsuarioService;

@Controller
@RequestMapping(value="/admin/incidente")
@SessionAttributes("NomUser")
public class IncidenteController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private IncidenteService incidenteService;
	
	@RequestMapping(value="/add", method=RequestMethod.GET )
	public ModelAndView addIncidentePage(){
		
		ModelAndView modelandview = new ModelAndView("add-incidente-form-admin");
	
		List<Hotel> hoteles = usuarioService.getListadoHoteles(); 
		
		modelandview.addObject("hoteles", hoteles);
		
		return modelandview;
	}
	
	
	@RequestMapping(value="/add", method=RequestMethod.POST )
	public ModelAndView addingIncidente(@RequestParam("descripcion") String descripcion,
									    @RequestParam("id_hotel") String id_hotel,
									    @RequestParam("fecha_suceso") String fecha_suceso){
		
		ModelAndView modelAndView = new ModelAndView("add-incidente-form-admin");
		
		Incidente incidente = new Incidente();

		int idHotel = Integer.parseInt(id_hotel);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		
		incidente.setFecha_registro(date);
		incidente.setFecha_suceso(Date.valueOf(fecha_suceso));
		incidente.setDescripcion(descripcion);	
		incidente.setId_hotel(idHotel);

		incidenteService.addIncidente(incidente);

		modelAndView = addIncidentePage();
		
		modelAndView.addObject("message", "Incidente agregado correctamente");
		
		return modelAndView;
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView listIncidentePage(){
		
		ModelAndView modelandview = new ModelAndView("list-incidente-admin");
		
		List<Incidente> listadoIncidente = incidenteService.getIncidentes();
		List<Hotel> listadoHotel = usuarioService.getListadoHoteles();
		
		modelandview.addObject("listadoIncidentes", listadoIncidente);
		modelandview.addObject("listadoHoteles", listadoHotel);
		
		return modelandview;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editIncidentePage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-incidente-form-admin");
		
		int intId = Integer.parseInt(id);
		List<Hotel> hoteles = usuarioService.getListadoHoteles();
		Incidente incidente = incidenteService.getIncidenteInt(intId);

		modelandview.addObject("incidente", incidente);
		modelandview.addObject("hoteles", hoteles);

		return modelandview;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editingIncidentePage(@PathVariable("id") String id,
											 @RequestParam("descripcion") String descripcion,
											 @RequestParam("id_hotel") String id_hotel,
											 @RequestParam("fecha_suceso") String fecha_suceso){
		
		ModelAndView modelandview= new ModelAndView("edit-incidente-form-admin");
		
		int idParsed = Integer.parseInt(id);
		int idHotel = Integer.parseInt(id_hotel);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		Incidente incidente = incidenteService.getIncidenteInt(idParsed);

		try{

			incidente.setFecha_registro(date);
			incidente.setFecha_suceso(Date.valueOf(fecha_suceso));
			incidente.setDescripcion(descripcion);	
			incidente.setId_hotel(idHotel);
			
			incidenteService.updateIncidente(incidente);
			
			modelandview = editIncidentePage(id);
			
			modelandview.addObject("message", "Incidente actualizado");
			
		}catch(Exception ex){	
			
			modelandview = editIncidentePage(id);
			modelandview.addObject("message", "No se pudo actualizar el Incidente");
		}	
		
		return modelandview;
	}
}
