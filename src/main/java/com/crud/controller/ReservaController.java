package com.crud.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Habitacion;
import com.crud.model.Pais;
import com.crud.model.Reserva;
import com.crud.model.TipoHabitacion;
import com.crud.model.Usuario;
import com.crud.reservaCore.Matrix;
import com.crud.service.EsquemaService;
import com.crud.service.HabitacionService;
import com.crud.service.HabitacionServiceImpl;
import com.crud.service.ReservaService;
import com.crud.service.TipoHabitacionService;
import com.crud.service.UsuarioService;

@Controller
@SessionAttributes(value = {"NomUser"} , types = Usuario.class)
public class ReservaController {
	
	@Autowired
	private ReservaService reservaService;
	
	@Autowired
	private HabitacionService habitacionService;
	
	@Autowired
	private TipoHabitacionService tipoHabitacionService;
	
	@Autowired
	private EsquemaService esquemaService;
		
	@Autowired
	private Matrix matrix;
	
	@Autowired
	private UsuarioService usuarioService;
	
	// m�todo da valor null por defecto a la session
	@ModelAttribute("NomUser")
	public Usuario populateForm(){
		
		Usuario usuario = new Usuario();
		usuario.setNom_user("Invitado");
		
	    return usuario; // populates form for the first time if its null
	}
		
	@RequestMapping(value="/admin/reserva/cambiar-estado/{id}", method=RequestMethod.POST )
	public ModelAndView cancelarReservaForm(@PathVariable("id") String id){
		
		ModelAndView modelAndView = new ModelAndView("error-page-admin");
		try{
			// se parsea a int
			int intId = Integer.parseInt(id);
			
			// se extrae la reserva
			Reserva reserva = reservaService.getReserva(intId);
			
			if(reserva.getEstado() == 1){
				
				reserva.setEstado(0);
				
			} else if(reserva.getEstado() == 0){
				
				reserva.setEstado(1);
			}
			
			reservaService.updateReserva(reserva);
			
			modelAndView.setViewName("redirect:/admin/reserva/"+reserva.getId());
			
		}catch(Exception ex){
			
			// si hubo un error, env�a a la pagina de error
			modelAndView.addObject("message", "Problemas al procesar la cancelaci�n");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/reserva/extender/{id}", method=RequestMethod.GET )
	public ModelAndView extenderReservaForm(@PathVariable("id") String id){
		
		ModelAndView modelAndView = new ModelAndView("extender-reserva-admin");
		
		try{
			// se parsea a int
			int intId = Integer.parseInt(id);
			
			// se extrae la reserva
			Reserva reserva = reservaService.getReserva(intId);
			
			// se env�a el objeto a la vista
			modelAndView.addObject("reserva", reserva);
			modelAndView.addObject("maxDate",esquemaService.getFechaMax().toString());
			modelAndView.addObject("minDate",reserva.getFechaFinLocalDate().plusDays(2).toString());
			
			Habitacion habitacion = habitacionService.getHabitacion(reserva.getIdHabitacion());
			TipoHabitacion tipoHabitacion = tipoHabitacionService.getTipoHabitacion(habitacion.getIdTipoHabitacion());
			
			modelAndView.addObject("tipoHabitacion", tipoHabitacion);
		}catch(Exception ex){
			
			// si hubo un error, env�a a la pagina de error
			modelAndView = new ModelAndView("error-page-admin");
		}
		
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="/admin/reserva/lista")
	public ModelAndView listReservasPage(){
		
		ModelAndView modelAndView = new ModelAndView("list-reserva-admin");
		
		List<Reserva> reservas;
		
		reservas = reservaService.getReservas();
		
		List<Object[]> personas = new ArrayList<Object[]>();
		List informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario(); 
		
		modelAndView.addObject("personas", informacionPersona);
		modelAndView.addObject("reservas", reservas);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/reserva/{codigo}", method=RequestMethod.GET)
	public ModelAndView detalleReserva(@PathVariable("codigo") String codigo,
									   @ModelAttribute("NomUser") Usuario NomUser){
		
		ModelAndView modelAndView = new ModelAndView("detalle-reserva");
		
		if( NomUser.getNom_user().equals("Invitado")){
			
			modelAndView.setViewName("reserva-fail");
			modelAndView.addObject("title","Lo sentimos.");
			modelAndView.addObject("message","Debe estar logueado para entrar aqu�.");
			
		}else{
			
			Usuario usuario = usuarioService.getUsuario(NomUser.getNom_user());
			
			String nombre = "";
			
			if(usuario.getId_rol() == 2){
				
				List<Object[]> informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario();
				
				for (Object[] object : informacionPersona) {
					
					if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
						
						nombre = object[1].toString()+ " " + object[2].toString();
					}
				}
			} else if (usuario.getId_rol() == 3){
				
				List<Object[]> informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario();
				
				for (Object[] object : informacionEmpresa) {
					
					if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
						
						nombre = object[10].toString();
					}
				}
			}
			
			Reserva reserva = reservaService.getReservaPorIdUsuarioYCodigo(usuario.getId(), codigo);
			
			Habitacion habitacion = habitacionService.getHabitacion(reserva.getIdHabitacion());
			TipoHabitacion tipoHabitacion = tipoHabitacionService.getTipoHabitacion(habitacion.getIdTipoHabitacion());
			
			modelAndView.addObject("nombre", nombre);
			modelAndView.addObject("reserva", reserva);
			modelAndView.addObject("tipoHabitacion", tipoHabitacion);
		}
		
		
		return modelAndView;
	}
	
	@RequestMapping(value="/mis-reservas", method = RequestMethod.GET)
	public ModelAndView listMisReservasGet(@ModelAttribute("NomUser") Usuario NomUser){
		
		ModelAndView modelAndView = new ModelAndView("mis-reservas");
		
		if (NomUser.getNom_user().equals("Invitado")){
			
			modelAndView.setViewName("reserva-fail");
			modelAndView.addObject("title","Lo sentimos.");
			modelAndView.addObject("message","Debe estar logueado para entrar aqu�.");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/mis-reservas", method = RequestMethod.POST)
	public @ResponseBody List<Reserva> listMisReservasPost(@ModelAttribute("NomUser") Usuario NomUser){
		
		List<Reserva> reservas;
		
		if(NomUser.getNom_user().equals("Invitado")){
			
			reservas = null;
			
		}else{
			
			Usuario usuario = usuarioService.getUsuario(NomUser.getNom_user());
			reservas = reservaService.getReservasPorIdUsuario(usuario.getId());
		}
		
		return reservas;
		
	}
	
		
	@RequestMapping(value="/reserva/confirmacion", method = RequestMethod.POST)
	public ModelAndView reservasConfirmPage(@RequestParam(value="fechaInicio", required=false) String fechaInicio, 
											@RequestParam(value="fechaFin", required=false) String fechaFin,
											@RequestParam(value="cantidadAdultos", required=false) String cantidadAdultos,
											@RequestParam(value="cantidadNinos", required=false) String cantidadNinos,
											@RequestParam(value="tipoHabitacion", required=false) String tipoHabitacion,
											@ModelAttribute("NomUser") Usuario NomUser){
		
		ModelAndView modelAndView = new ModelAndView("reserva-fail");
		
		try{
			if (NomUser.getNom_user().equals("Invitado")) {
				modelAndView.addObject("title","Lo sentimos.");
				modelAndView.addObject("message","Debe estar logueado para entrar aqu�.");
			} else {
				if(!fechaInicio.equals("") && !tipoHabitacion.equals("") && !fechaFin.equals("") && !cantidadAdultos.equals("") && !cantidadNinos.equals("")){
					Usuario usuario = usuarioService.getUsuario(NomUser.getNom_user());
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					
					LocalDate fechaInicioFormated = LocalDate.parse(fechaInicio, formatter);
					LocalDate fechaFinFormated = LocalDate.parse(fechaFin, formatter);
					
					int ninos = Integer.parseInt(cantidadNinos);
					int adultos = Integer.parseInt(cantidadAdultos);
					int parsedTipoHabitacion = Integer.parseInt(tipoHabitacion);
					
					// se crea un objeto Reserva y se llena con los datos
					Reserva reserva = new Reserva();
					reserva.setFechaFin(fechaFinFormated);
					reserva.setFechaInicio(fechaInicioFormated);
					reserva.setEstado(1);
					reserva.setIdUsuario(usuario.getId());
					reserva.setNinos(ninos);
					reserva.setAdultos(adultos);
					
					TipoHabitacion tipoHabitacionControl = tipoHabitacionService.getTipoHabitacion(parsedTipoHabitacion);
					int precio = reserva.calcularPrecio(tipoHabitacionControl.getPrecio_base());
					
					reserva.setPrecio(precio);
					/*
					if(precio == esquema){
						reserva.setPrecio(precioParsed);
					}else {
						throw new Exception("No s epudo llevar a cabo su petici�n, por favor intente nuevamente.");
					}
					*/
					
					String nombre = "";
					if(usuario.getId_rol() == 2){
						List<Object[]> informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario();
						for (Object[] object : informacionPersona) {
							if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
								nombre = object[1].toString()+ " " + object[2].toString();
							}
						}
					} else if (usuario.getId_rol() == 3){
						List<Object[]> informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario();
						for (Object[] object : informacionEmpresa) {
							if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
								nombre = object[10].toString();
							}
						}
					}
					 
					 
					
					modelAndView.addObject("nombre", nombre);
					modelAndView.addObject("reserva", reserva);
					modelAndView.addObject("tipoHabitacion", tipoHabitacionControl);
					modelAndView.setViewName("confirmacion-reserva");
				} else {
					modelAndView.addObject("title","Lo sentimos.");
					modelAndView.addObject("message","Debe llenar todos los campos.");
				}
			}
			
		}catch(Exception ex){
			modelAndView.addObject("title","Lo sentimos.");
			modelAndView.addObject("message",ex.getMessage());
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/reservar", method=RequestMethod.POST)
	public ModelAndView addReservaPage(@RequestParam(value="precio", required=false) String precio,
										@RequestParam(value="fechaInicio", required=false) String fechaInicio, 
										@RequestParam(value="fechaFin", required=false) String fechaFin,
										@RequestParam(value="cantidadAdultos", required=false) String cantidadAdultos,
										@RequestParam(value="cantidadNinos", required=false) String cantidadNinos,
										@RequestParam(value="idTipoHabitacion", required=false) String idTipoHabitacion,
										@ModelAttribute("NomUser") Usuario NomUser){
		
		ModelAndView modelAndView = new ModelAndView("reserva-fail");
		// validacion de usuario
		
		try{
			if (NomUser.getNom_user().equals("Invitado")) {
				modelAndView.addObject("title","Lo sentimos");
				modelAndView.addObject("message","Antes de reservar, debes iniciar sesion.");
			} else {
				if(!fechaInicio.equals("") && !precio.equals("") && !fechaFin.equals("") && !idTipoHabitacion.equals("") && !cantidadAdultos.equals("") && !cantidadNinos.equals("")){
					if(usuarioService.getUsuario(NomUser.getNom_user()) != null){
						Usuario usuario = usuarioService.getUsuario(NomUser.getNom_user());
						
						int parseTipoHabitacion = Integer.parseInt(idTipoHabitacion);
						TipoHabitacion tipohabitacion = tipoHabitacionService.getTipoHabitacion(parseTipoHabitacion);
						
						int parsedPrecio = Integer.parseInt(precio);
						// formato de las fechas
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
						
						LocalDate fechaInicioFormated = LocalDate.parse(fechaInicio, formatter);
						LocalDate fechaFinFormated = LocalDate.parse(fechaFin, formatter);
						
						// se crea un objeto Reserva y se llena con los datos
						Reserva reserva = new Reserva();
						reserva.setFechaFin(fechaFinFormated);
						reserva.setFechaInicio(fechaInicioFormated);
						reserva.setEstado(1);
						reserva.setCodigo();
						reserva.setIdUsuario(usuario.getId());
						reserva.setAdultos(Integer.parseInt(cantidadAdultos));
						reserva.setNinos(Integer.parseInt(cantidadNinos));
						
						int precioTotal = reserva.calcularPrecio(tipohabitacion.getPrecio_base());
						reserva.setPrecio(precioTotal);
						
						reserva.setIdHabitacion(0);
						if(parsedPrecio != precioTotal){
							throw new Exception();
						}
						
						if(matrix.orderAndReserve(reserva, parseTipoHabitacion)){
							modelAndView.setViewName("reserva-success");
						} else {
							modelAndView.addObject("title","Lo sentimos");
							modelAndView.addObject("message","No fu� posible realizar la reserva. \nIntente mas tarde.");
						}
					}
				}else {
					modelAndView.addObject("title","Lo sentimos.");
					modelAndView.addObject("message","Debe llenar todos los campos.");
				}
			}
		}catch(Exception ex){
			if(ex.getMessage().equals("")){
				modelAndView.addObject("message", "Problemas al procesar la extenci�n");
			}else {
				modelAndView.addObject("message", ex.getMessage());
			}
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/reserva/extender/confirmacion", method = RequestMethod.POST)
	public ModelAndView reservasConfirmAdmin(
											@RequestParam(value="tipoHabitacion", required=false) String tipoHabitacion, 
											@RequestParam(value="fechaInicio", required=false) String fechaInicio, 
											@RequestParam(value="fechaFin", required=false) String fechaFin,
											@RequestParam(value="cantidadAdultos", required=false) String cantidadAdultos,
											@RequestParam(value="cantidadNinos", required=false) String cantidadNinos,
											@RequestParam(value="idUsuario", required=false) String idUsuario){
		
		ModelAndView modelAndView = new ModelAndView("error-page-admin");
		
		
		try{
			
			if(!tipoHabitacion.equals("") && !fechaInicio.equals("") && !idUsuario.equals("") && !fechaFin.equals("") && !cantidadAdultos.equals("") && !cantidadNinos.equals("")){
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				LocalDate fechaInicioFormated = LocalDate.parse(fechaInicio, formatter).plusDays(1);
				LocalDate fechaFinFormated = LocalDate.parse(fechaFin, formatter);
				if(fechaInicioFormated.isAfter(fechaFinFormated)){
					throw new Exception("Problemas al procesar las fechas.");
				}
				int ninos = Integer.parseInt(cantidadNinos);
				int adultos = Integer.parseInt(cantidadAdultos);
				int idUsuarioParsed = Integer.parseInt(idUsuario);
				int parsedTipoHabitacion = Integer.parseInt(tipoHabitacion);
				
				// se crea un objeto Reserva y se llena con los datos
				Reserva reserva = new Reserva();
				reserva.setFechaFin(fechaFinFormated);
				reserva.setFechaInicio(fechaInicioFormated);
				reserva.setEstado(1);
				reserva.setIdUsuario(idUsuarioParsed);
				reserva.setNinos(ninos);
				reserva.setAdultos(adultos);
				
				TipoHabitacion tipoHabitacionControl = tipoHabitacionService.getTipoHabitacion(parsedTipoHabitacion);
				
				int precio = reserva.calcularPrecio(tipoHabitacionControl.getPrecio_base());
				
				reserva.setPrecio(precio);
				
				Usuario usuario = usuarioService.getUsuario(idUsuarioParsed);
				String nombre = "";
				if(usuario.getId_rol() == 2){
					List<Object[]> informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario();
					for (Object[] object : informacionPersona) {
						if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
							nombre = object[1].toString()+ " " + object[2].toString();
						}
					}
				} else if (usuario.getId_rol() == 3){
					List<Object[]> informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario();
					for (Object[] object : informacionEmpresa) {
						if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
							nombre = object[10].toString();
						}
					}
				}
				 
				 
				modelAndView.setViewName("confirmacion-extension");
				modelAndView.addObject("nombre", nombre);
				modelAndView.addObject("reserva", reserva);
				
				modelAndView.addObject("tipoHabitacion", tipoHabitacionControl);
				
			} else {
				modelAndView.addObject("title","Lo sentimos.");
				modelAndView.addObject("message","Debe llenar todos los campos.");
			}
			
		}catch(Exception ex){
			if(ex.getMessage().equals("")){
				modelAndView.addObject("message", "Problemas al procesar la extenci�n");
			}else {
				modelAndView.addObject("message", ex.getMessage());
			}
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/reserva/extender", method=RequestMethod.POST )
	public ModelAndView extenderReservaPage(@RequestParam(value="idTipoHabitacion", required=false) String idTipoHabitacion,
											@RequestParam(value="fechaInicio", required=false) String fechaInicio, 
											@RequestParam(value="fechaFin", required=false) String fechaFin,
											@RequestParam(value="cantidadAdultos", required=false) String cantidadAdultos,
											@RequestParam(value="cantidadNinos", required=false) String cantidadNinos,
											@RequestParam(value="idUsuario", required=false) String idUsuario){
		
			ModelAndView modelAndView = new ModelAndView("error-page-admin");
		
		
		try{
			if(!fechaInicio.equals("") && !fechaFin.equals("")  && !idUsuario.equals("") && !idTipoHabitacion.equals("") && !cantidadAdultos.equals("") && !cantidadNinos.equals("")){
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				LocalDate fechaInicioFormated = LocalDate.parse(fechaInicio, formatter);
				LocalDate fechaFinFormated = LocalDate.parse(fechaFin, formatter);
				if(fechaInicioFormated.isAfter(fechaFinFormated)){
					throw new Exception("Problemas al procesar las fechas.");
				}
				int ninos = Integer.parseInt(cantidadNinos);
				int adultos = Integer.parseInt(cantidadAdultos);
				int idUsuarioParsed = Integer.parseInt(idUsuario);
				int idTipoHabitacionParsed = Integer.parseInt(idTipoHabitacion);
				
				// se crea un objeto Reserva y se llena con los datos
				Reserva nuevaReserva = new Reserva();
				nuevaReserva.setFechaFin(fechaFinFormated);
				nuevaReserva.setFechaInicio(fechaInicioFormated);
				nuevaReserva.setEstado(1);
				nuevaReserva.setIdUsuario(idUsuarioParsed);
				nuevaReserva.setNinos(ninos);
				nuevaReserva.setAdultos(adultos);
				
				TipoHabitacion tipoHabitacionControl = tipoHabitacionService.getTipoHabitacion(idTipoHabitacionParsed);
				int precio = nuevaReserva.calcularPrecio(tipoHabitacionControl.getPrecio_base());
				
				nuevaReserva.setPrecio(precio);
				
				if(matrix.orderAndReserve(nuevaReserva, idTipoHabitacionParsed)){
					System.out.println("****** SI RESERVA ");
					modelAndView.setViewName("redirect:/admin/reserva/"+nuevaReserva.getId());
				} else {
					throw new Exception("No hay Habitaciones disponibles.");
				}
			}else{
				System.out.println("****** ERROR ");
				throw new Exception("Problemas al procesar los datos. Porfavor intente nuevamente.");
			}
						
		}catch(Exception ex){
			if(ex.getMessage().equals("")){
				modelAndView.addObject("message", "Problemas al procesar la extenci�n");
			}else {
				modelAndView.addObject("message", ex.getMessage());
			}
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/reserva/{id}", method=RequestMethod.GET)
	public ModelAndView detalleReservaAdmin(@PathVariable("id") String id){
		ModelAndView modelAndView = new ModelAndView("detalle-reserva-admin");
		
		try{
			int idParsed = Integer.parseInt(id);
			Reserva reserva = reservaService.getReserva(idParsed);
			Usuario usuario = usuarioService.getUsuario(reserva.getIdUsuario());
			
			String nombre = "";
			if(usuario.getId_rol() == 2){
				List<Object[]> informacionPersona = usuarioService.getInformacionDePersonaPorIdUsuario();
				for (Object[] object : informacionPersona) {
					if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
						nombre = object[1].toString()+ " " + object[2].toString();
					}
				}
			} else if (usuario.getId_rol() == 3){
				List<Object[]> informacionEmpresa = usuarioService.getInformacionDeEmpresaPorIdUsuario();
				for (Object[] object : informacionEmpresa) {
					if(object[0].toString().trim().equals(usuario.getId().toString().trim())){
						nombre = object[10].toString();
					}
				}
			}
			Habitacion habitacion = habitacionService.getHabitacion(reserva.getIdHabitacion());
			TipoHabitacion tipoHabitacion = tipoHabitacionService.getTipoHabitacion(habitacion.getIdTipoHabitacion());
			
						
			modelAndView.addObject("nombre", nombre);
			modelAndView.addObject("reserva", reserva);
			modelAndView.addObject("tipoHabitacion", tipoHabitacion);
		
		}catch(Exception ex){
			modelAndView.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelAndView.addObject("message", "Problemas al procesar la petici�n. Porfavor intente nuevamente.");
			}else {
				modelAndView.addObject("message", ex.getMessage());
			}
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/periodo", method = RequestMethod.GET)
	public ModelAndView gestionPeriodoGet(@ModelAttribute("NomUser") Usuario NomUser){
		ModelAndView modelAndView = new ModelAndView("carga-periodos-admin");
		modelAndView.addObject("ultimaFecha",esquemaService.getFechaMax().toString());
		modelAndView.addObject("fechaMinima",esquemaService.getFechaMax().plusDays(60).toString());
		System.out.println("FEcha controller: "+esquemaService.getFechaMax().plusDays(60).toString());
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/periodo", method = RequestMethod.POST)
	public ModelAndView gestionPeriodoPOST(@RequestParam(value="nuevaFecha", required=false) String nuevaFecha){
		ModelAndView modelAndView = new ModelAndView("carga-periodos-admin");
		
		try{
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate fin = LocalDate.parse(nuevaFecha, formatter);
			
			if(esquemaService.getFechaMax().plusDays(1).isBefore(fin)){
				matrix.setInicioEsquema(esquemaService.getFechaMax().plusDays(1), fin);
				
			}
		}catch(Exception ex){
			modelAndView.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelAndView.addObject("message", "Problemas al procesar la petici�n. Porfavor intente nuevamente.");
			}else {
				modelAndView.addObject("message", ex.getMessage());
			}
		}
		modelAndView.setViewName("redirect:/admin/periodo");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/info-periodo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String getInfoPeriodos(@RequestParam(value="fechaFin", required=false) String fechaFin){
		String jsonResponse = "{";
		try{
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate fechaFinFormated = LocalDate.parse(fechaFin, formatter);
			
			jsonResponse += " \"ultimaFecha\": \" "+esquemaService.getFechaMax().toString()+"\" ";
			jsonResponse += ", \"cantidadDias\": \""+ChronoUnit.DAYS.between(esquemaService.getFechaMax(),fechaFinFormated)+"\" ";
			
		}catch(Exception ex){
			jsonResponse += "\"error\": \"Fecha inv�lida.\"";
		}
		return jsonResponse + "}";
	}
	
	@RequestMapping(value = "/admin/reservas-en-curso", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<Reserva> getReservasEnCurso(){
		List<Reserva> lista = esquemaService.getReservasEnCurso();
		return lista;
	}
	
	
}
