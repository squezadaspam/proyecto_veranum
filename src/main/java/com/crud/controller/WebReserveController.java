package com.crud.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Esquema;
import com.crud.model.Habitacion;
import com.crud.model.TipoHabitacion;
import com.crud.model.Usuario;
import com.crud.service.EsquemaService;
import com.crud.service.HabitacionService;
import com.crud.service.TipoHabitacionService;

@Controller
@SessionAttributes("NomUser")
public class WebReserveController {

	@Autowired
	EsquemaService esquemaService;
	
	@Autowired
	HabitacionService habitacionService;
	

	@Autowired
	TipoHabitacionService tipoHabitacionService;
	
	// m�todo da valor null por defecto a la session
		@ModelAttribute("NomUser")
		public Usuario populateForm() {
			Usuario user = new Usuario();
			user.setNom_user("Invitado");
		       return user; // populates form for the first time if its null
		}
	@RequestMapping(value={"/", "/index"})
	public ModelAndView mainPage(@ModelAttribute("NomUser") Usuario NomUser){
		
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("now", LocalDate.now().toString());
		modelAndView.addObject("maxDate", esquemaService.getFechaMax().toString());
		modelAndView.addObject("page", "index");
		modelAndView.addObject("NomUser", NomUser);
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/valida-tipo-habitacion", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<TipoHabitacion> getInfoPeriodos(@RequestParam(value="personas", required=false) String personas){
		List<TipoHabitacion> lista = new ArrayList<TipoHabitacion>();
		List<TipoHabitacion> listaMostrar = new ArrayList<TipoHabitacion>();
		try{
			int parsedPersonas = Integer.parseInt(personas);
			lista = (List<TipoHabitacion>)tipoHabitacionService.getTipoHabitaciones();
			
			for (TipoHabitacion tipoHabitacion : lista) {
				if(tipoHabitacion.getCapacidad_maxima() >= parsedPersonas ){
					listaMostrar.add(tipoHabitacion);
				}
			}
		}catch(Exception ex){
			
		}
		
		return listaMostrar;
	}
	
	
}
