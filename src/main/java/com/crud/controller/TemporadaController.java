package com.crud.controller;



import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Temporada;
import com.crud.service.TemporadaService;



@Controller
public class TemporadaController {
	
	@Autowired
	private TemporadaService temporadaServicio;
	

	@RequestMapping(value="/admin/cargoAdicional/agregar", method= RequestMethod.GET)
	public ModelAndView addTemporadaPage(){
		ModelAndView modelandview = new ModelAndView("add-temporada-form-admin");		
		return modelandview;
	}

	@RequestMapping(value="/admin/cargoAdicional/agregar", method=RequestMethod.POST)
	public ModelAndView addingTemporada(@RequestParam("nombre") String nombre,
			@RequestParam("fechaInicio") String fecha_ini,
			@RequestParam("fechaFin") String fecha_fin,
			@RequestParam("cargo") String cargo){
		
		ModelAndView modelAndView = new ModelAndView("add-temporada-form-admin");
		
		Temporada objTemporada = new Temporada();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		LocalDate fechaInicioFormated = LocalDate.parse(fecha_ini, formatter);
		LocalDate fechaFinFormated = LocalDate.parse(fecha_fin, formatter);
		
		objTemporada.setNombre(nombre);
		objTemporada.setFechaIni(fechaInicioFormated);
		objTemporada.setFechaFin(fechaFinFormated);
		objTemporada.setCargo(Double.parseDouble(cargo));
		objTemporada.setEstado(1);
		
		temporadaServicio.addTemporada(objTemporada);
		
		String message = "Cargo agregado correctamente";
		modelAndView.addObject("message",message);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/cargoAdicional/borrar/{id}")
	public ModelAndView deleteTemporadaPage(@PathVariable("id") String id){
		ModelAndView modelAndView = new ModelAndView("error-page-admin");
		try{
			
				int idParsed = Integer.parseInt(id);
				Temporada temporada = temporadaServicio.getTemporada(idParsed);				
				temporada.setEstado(0);
				
				temporadaServicio.updateTemporada(temporada);
			
			
			modelAndView.setViewName("redirect:/admin/cargoAdicional/listar");
			
		}catch(Exception ex){
			
			
			modelAndView.addObject("message", "Eliminar");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/cargoAdicional/editar/{id}", method=RequestMethod.GET )
	public ModelAndView editTemporadaForm(@PathVariable("id") String id){
		ModelAndView modelAndView = new ModelAndView("edit-temporada-form-admin");
		try{
			
			int intId = Integer.parseInt(id);			
			
			Temporada temporada = temporadaServicio.getTemporada(intId);
			
			modelAndView.addObject("temporada", temporada);
			
		}catch(Exception ex){
			
			// si hubo un error, env�a a la pagina de error
			modelAndView = new ModelAndView("error-page-admin");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/cargoAdicional/editar/{id}", method=RequestMethod.POST)
	public ModelAndView editTemporadaPage(@PathVariable("id") String id,@RequestParam("nombre") String nombre,
			@RequestParam("fechaInicio") String fecha_ini,
			@RequestParam("fechaFin") String fecha_fin,
			@RequestParam("cargo") String cargo){
		
		ModelAndView modelandview = new ModelAndView("edit-temporada-form-admin");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		try{
			int idParsed = Integer.parseInt(id);
			Temporada temporada = temporadaServicio.getTemporada(idParsed);
			LocalDate fechaInicioFormated = LocalDate.parse(fecha_ini, formatter);
			LocalDate fechaFinFormated = LocalDate.parse(fecha_fin, formatter);
			temporada.setNombre(nombre);
			temporada.setFechaIni(fechaInicioFormated);
			temporada.setFechaFin(fechaFinFormated);
			temporada.setCargo(Double.parseDouble(cargo));
			
			temporadaServicio.updateTemporada(temporada);
			modelandview.setViewName("redirect:/admin/cargoAdicional/listar");
		}catch(Exception ex){
			modelandview.addObject("message", "Error en la Edici�n");
		}
		
		return modelandview;
	}
	
	@RequestMapping(value="/admin/cargoAdicional/listar")
	public ModelAndView listTemporadaPage(){
		ModelAndView modelandview = new ModelAndView("list-temporada-admin");
		
		List<Temporada> temporada = temporadaServicio.getTemporada();
		modelandview.addObject("temporada", temporada);
		
		return modelandview;
	}
	
}
