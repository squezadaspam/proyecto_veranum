package com.crud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import org.springframework.web.servlet.ModelAndView;

import com.crud.model.DetalleServicio;
import com.crud.service.DetalleServicioService;


@Controller
public class DetalleServicioController  {
	

	
	@Autowired
	private DetalleServicioService detalleServicioService;
	
	@RequestMapping(value="/admin/detalleServicio/add", method=RequestMethod.GET)
	public ModelAndView addDetalleServicioPage(){
		
		ModelAndView modelandview = new ModelAndView("add-detalleServicio-form-admin");
		
		return modelandview;
	}
	
	@RequestMapping(value="/admin/detalleServicio/add", method=RequestMethod.POST)
	public ModelAndView addingDetalleServicio(@RequestParam("monto") String monto, 		
	@RequestParam("descripcion") String descripcion){
		
		ModelAndView modelAndView = new ModelAndView("add-detalleServicio-form-admin");
		
		int montoInt = Integer.parseInt(monto);
		
		DetalleServicio detalleServicio= new DetalleServicio();
		
		detalleServicio.setDescripcion(descripcion);
		detalleServicio.setMonto(montoInt);
		detalleServicioService.addDetalleServicio(detalleServicio);
					
		String message = "detalle servicio agregado correctamente";
		
		modelAndView.addObject("message",message);
		
		return modelAndView;
	
	}
	
	@RequestMapping(value="/admin/detalleServicio/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editDetalleServicioPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-detalleServicio-form-admin");
		
		int intId = Integer.parseInt(id);
		DetalleServicio detalleServicio = detalleServicioService.getDetalleServicioInt(intId);
		
		modelandview.addObject("detalleServicio", detalleServicio);
		
		return modelandview;
	}
									  
	@RequestMapping(value="/admin/detalleServicio/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edditingDetalleServicio(@PathVariable("id") String id,
											    @RequestParam("monto") String monto,		
											    @RequestParam("descripcion") String descripcion){
		
		int idParsed = Integer.parseInt(id.trim());
		int montoInt = Integer.parseInt(monto.trim());
		
		ModelAndView modelAndView = new ModelAndView("edit-detalleServicio-form-admin");
		
		DetalleServicio detalleServicio = detalleServicioService.getDetalleServicio(idParsed);
		
		detalleServicio.setMonto(montoInt);
		detalleServicio.setDescripcion(descripcion);
		
		detalleServicioService.updateDetalleServicio(detalleServicio);
		
		String message = "Detalle Servicio modificado correctamente";
		
		modelAndView.addObject("message",message);
		
		return modelAndView;
	}
		
	
	@RequestMapping(value="/admin/detalleServicio/list", method=RequestMethod.GET)
	public ModelAndView listDetalleServicioPage(){
		
		ModelAndView modelandview = new ModelAndView("list-detalleServicio-form-admin");
		
		List<DetalleServicio> listadoDetalleServicio = detalleServicioService.getDetalleServicio();
		
		modelandview.addObject("listadoDetalleServicio", listadoDetalleServicio);
		
		return modelandview;
	}
}