package com.crud.controller;

import java.sql.Date;
import java.util.List;

import org.apache.velocity.runtime.directive.Foreach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Cargo;
import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empleado;
import com.crud.model.Hotel;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.model.Usuario;
import com.crud.service.CargoService;
import com.crud.service.EmpleadoService;
import com.crud.service.HotelService;
import com.crud.service.UsuarioService;

@Controller
@RequestMapping(value="/admin/empleado")
@SessionAttributes("NomUser")
public class EmpleadoController {

	
	@Autowired
	private CargoService cargoService;

	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value="/buscar", method=RequestMethod.GET)
	public ModelAndView addEmpleadoPage(){
		
		ModelAndView modelandview = new ModelAndView("buscar-empleado-admin");
		
		return modelandview;
	}
	
	@RequestMapping(value="/buscar", method=RequestMethod.POST)
	public ModelAndView addingEmpleado(@RequestParam("buscarRun") String run){
		
		System.out.println(run);
		if(!run.equals("")){
			
			Persona persona = usuarioService.getRunPersona(run);
			
			try{
				if(persona != null){
					
					if(usuarioService.getEmpleadoIdPersona(persona.getId()) != null){
						ModelAndView md1 = new ModelAndView("buscar-empleado-admin");
						md1.addObject("message", "ya existe el Empleado");
						return md1;
					}
					ModelAndView md1 = new ModelAndView("redirect:/admin/empleado/add-empleado");
					md1.addObject("buscarRun", run);
					return md1;
						
				} else {
					ModelAndView md1 = new ModelAndView("redirect:/admin/empleado/add-persona-empleado");
					md1.addObject("buscarRun", run);
					return md1;
				}
			}catch(Exception ex){
				
			}
		}
		
		return new ModelAndView("buscar-empleado-admin");
		
	}
	
	@RequestMapping(value="/add-empleado", method=RequestMethod.GET)
	public ModelAndView addEmpleadoFormPage(@RequestParam("buscarRun") String run){
		
		ModelAndView modelandview = new ModelAndView("add-empleado-form-admin");
		
		Persona persona = usuarioService.getRunPersona(run);
		List<Cargo> cargos = cargoService.getCargosActivos();
		Contacto contacto = usuarioService.getRunContacto(run);
		Direccion direccion = usuarioService.getRunDireccion(run);
		List<Hotel> hoteles = usuarioService.getListadoHoteles();
		
		modelandview.addObject("hoteles", hoteles);
		modelandview.addObject("persona", persona);
		modelandview.addObject("cargos", cargos);
		modelandview.addObject("contacto", contacto);
		modelandview.addObject("direccion", direccion);
		
		
		return modelandview;
	}
	
	
	
	@RequestMapping(value="/add-empleado", method=RequestMethod.POST)
	public ModelAndView addingEmpleadoFormPage(@RequestParam("id_cargo") String id_cargo,
											   @RequestParam("id_persona") String id_persona,
											   @RequestParam("id_hotel") String id_hotel){
		
		ModelAndView modelandview = new ModelAndView("add-empleado-form-admin");
		int idcargo = Integer.parseInt(id_cargo);
		int idpersona = Integer.parseInt(id_persona);
		int idhotel = Integer.parseInt(id_hotel);
		
		if(!usuarioService.empleadoExiste(idpersona)){
			
			Empleado emp = new Empleado();

			emp.setId_cargo(idcargo);
			emp.setId_persona(idpersona);
			emp.setId_hotel(idhotel);
			emp.setEstado(1);
			
			usuarioService.addEmpleado(emp);
			
			modelandview = listEmpleadoPage();
			modelandview.addObject("message","Empleado agregado exitosamente");
			
			return modelandview;
		
		}else{
			
			ModelAndView modelandview1 = new ModelAndView("buscar-empleado-admin");
			modelandview1.addObject("message","Problemas al procesar la solicitud");
			
			return modelandview1;
		}
		
	}
	
	@RequestMapping(value="/add-persona-empleado", method=RequestMethod.GET)
	public ModelAndView addPersonaEmpleadoPage(@RequestParam("buscarRun") String run){
		
		ModelAndView modelandview = new ModelAndView("add-personaEmpleado-form");
		
		List<Cargo> cargos = cargoService.getCargosActivos();
		List<Pais> paises = usuarioService.getListadoPais();
		List<Hotel> hoteles = usuarioService.getListadoHoteles();
		
		modelandview.addObject("run", run);
		modelandview.addObject("cargos", cargos);
		modelandview.addObject("hoteles", hoteles);
		modelandview.addObject("listadoPais", paises);
		
		return modelandview;
	}
	
	@RequestMapping(value="/add-persona-empleado", method=RequestMethod.POST)
	public ModelAndView addingPersonaEmpleadoPage( @RequestParam("nombre") String nombre,
												   @RequestParam("apellido") String apellido,
												   @RequestParam("run") String run,
												   @RequestParam("contraseña") String contraseña,
												   @RequestParam("nom_user") String nom_user,
												   @RequestParam("pais") String idPais,
												   @RequestParam("ciudad") String ciudad,
												   @RequestParam("num_depto") String num_depto,
												   @RequestParam("num_casa") String num_casa,
												   @RequestParam("codigo_postal") String codigo_postal,
												   @RequestParam("calle") String calle,
												   @RequestParam("email") String email,
												   @RequestParam("fec_nac") String fec_nac,
												   @RequestParam("NumeroTelefono") String[] NumeroTelefono,
												   @RequestParam("NombreTelefono") String[] NombreTelefono,
												   @RequestParam("id_cargo") String id_cargo,
												   @RequestParam("id_hotel") String id_hotel){
		
		
		ModelAndView modelandview = new ModelAndView("add-personaEmpleado-form");
		
		
		if(!usuarioService.personaExiste(run)){

			//agregamos los atributos al objeto Usuario
			Usuario usuario =  new Usuario();
			usuario.setEstado(1);
			usuario.setId_rol(4);
			usuario.setNom_user(nom_user.trim());
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String contraseñaHash = passwordEncoder.encode(contraseña);
			
			usuario.setPass(contraseñaHash);
			usuarioService.addUsuario(usuario);
								
			Ciudad objCiudad = new Ciudad();
			objCiudad.setId_pais(Integer.parseInt(idPais));
			objCiudad.setNombre(ciudad);
			usuarioService.addCiudad(objCiudad);
			
			Direccion direccion = new Direccion();
			direccion.setCalle(calle);
			direccion.setCod_postal(codigo_postal);
			direccion.setId_ciudad(objCiudad.getId());
			direccion.setNum_depto(num_depto);
			direccion.setNumero(Integer.parseInt(num_casa));
			usuarioService.addDireccion(direccion);
					
			Contacto contacto= new Contacto();
			contacto.setEmail(email);
			contacto.setNombre(nombre);
			contacto.setId_direccion(direccion.getId());
			contacto.setId_usuario(usuario.getId());
			usuarioService.addContacto(contacto);
			
			//Ingreso de telefonos
			for (int i = 0; i < NombreTelefono.length; i++) {
				Telefono objTelefono = new Telefono();
				objTelefono.setNombre(NombreTelefono[i]);
				objTelefono.setNumero(NumeroTelefono[i]);
				objTelefono.setEstado(1);
				objTelefono.setId_contacto(contacto.getId());
				usuarioService.addTelefono(objTelefono);
			}
			
			Persona persona = new Persona();
			persona.setId_contacto(contacto.getId());
			persona.setApellido(apellido);
			persona.setNombre(nombre);
			persona.setRun(run);
			persona.setFec_nac(Date.valueOf(fec_nac));
			usuarioService.addUsuarioPersona(persona);
			
			//INGRESO EMPLEADO
			
			Empleado emp = new Empleado();
			int idcargo = Integer.parseInt(id_cargo);
			int idhotel = Integer.parseInt(id_hotel);
			
			emp.setId_hotel(idhotel);
			emp.setId_cargo(idcargo);
			emp.setId_persona(persona.getId());
			emp.setEstado(1);
			usuarioService.addEmpleado(emp);
			
			modelandview = listEmpleadoPage();
			modelandview.addObject("message","Empleado agregado exitosamente");
			
			return modelandview;
		
		}else{
			ModelAndView modelandview1 = new ModelAndView("buscar-empleado-admin");
			modelandview1.addObject("message","Problemas al procesar la solicitud");
			
			return modelandview1;
		}
		
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView listEmpleadoPage(){
		
		ModelAndView modelandview = new ModelAndView("list-empleado-admin");
		
		List<Empleado> listadoEmpleado = usuarioService.getListadoEmpleados();
		List<Cargo> listadoCargo = cargoService.getCargos();
		List<Persona> listadoPersona = usuarioService.getListadoPersonas();
		List<Hotel> listadoHotel = usuarioService.getListadoHoteles();
		
		modelandview.addObject("listadoHoteles", listadoHotel);
		modelandview.addObject("listadoEmpleados", listadoEmpleado);
		modelandview.addObject("listadoCargos", listadoCargo);
		modelandview.addObject("listadoPersonas", listadoPersona);
		
		return modelandview;
	}
		
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editEmpleadoPage(@PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("edit-empleado-form-admin");
		
		int intId = Integer.parseInt(id);
		List<Hotel> hoteles = usuarioService.getListadoHoteles();
		List<Cargo> cargos = cargoService.getCargosActivos();
		Empleado empleado = usuarioService.getEmpleadoUsuario(intId);
		
		
		int idPersona = empleado.getId_persona();
		Persona persona = usuarioService.getPersona(idPersona);
		
		modelandview.addObject("persona", persona);
		modelandview.addObject("cargos", cargos);
		modelandview.addObject("hoteles", hoteles);
		modelandview.addObject("empleado", empleado);
		
		return modelandview;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView editingEmpleadoPage(@PathVariable("id") String id,
											@RequestParam("id_cargo") String id_cargo,
											@RequestParam("id_hotel") String id_hotel,
											@RequestParam("estado") String estado){
		
		ModelAndView modelandview= new ModelAndView("edit-empleado-form-admin");
		
		int idParsed = Integer.parseInt(id);
		int idCargoParsed = Integer.parseInt(id_cargo);
		int estadoParsed = Integer.parseInt(estado);
		int idhotelParsed = Integer.parseInt(id_hotel);
		
		Empleado empleado = usuarioService.getEmpleadoUsuario(idParsed);
		
		
		try{

			empleado.setId_hotel(idhotelParsed);
			empleado.setId_cargo(idCargoParsed);
			empleado.setEstado(estadoParsed);
			
			usuarioService.updateEmpleadoUsuario(empleado);
		
			modelandview = editEmpleadoPage(id);
			modelandview.addObject("message", "Empleado actualizado");
			
		}catch(Exception ex){	
			
			modelandview = editEmpleadoPage(id);
			modelandview.addObject("message", "No se pudo actualizar el Empleado");
		}		
		return modelandview;
	}
}