package com.crud.controller;

import java.util.Calendar;
import java.util.List;

import org.apache.velocity.runtime.directive.Foreach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;
import com.crud.model.Pago;
import com.crud.model.Consumo;
import com.crud.model.Contacto;
import com.crud.model.DetalleServicio;
import com.crud.model.HorarioServicio;
import com.crud.model.Persona;
import com.crud.model.Servicio;
import com.crud.model.Telefono;
import com.crud.model.Usuario;
import com.crud.service.ConsumoService;
import com.crud.service.ConsumoServiceImpl;
import com.crud.service.UsuarioService;
import com.crud.service.ServicioService;
import com.crud.service.HorarioServicioService;
import com.crud.service.DetalleServicioService;


@Controller
@RequestMapping(value="/admin/consumo")
@SessionAttributes("NomUser")
public class ConsumoController {



	@Autowired
	private ConsumoService consumoService;

	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ServicioService servicioService;
	
	@Autowired
	private DetalleServicioService servicioDetalle;
	
	@Autowired
	private HorarioServicioService servicioHorario;
	
	
	
	@RequestMapping(value="/buscar", method=RequestMethod.GET)
	public ModelAndView addConsumoPage(){
		
		ModelAndView modelandview = new ModelAndView("buscar-consumo-admin");
		
		return modelandview;
	}
	
	@RequestMapping(value="/buscar", method=RequestMethod.POST)
	public ModelAndView addingConsumo(@RequestParam("buscarRun") String run){
		
		//ModelAndView modelandview = new ModelAndView("redirect:/admin/consumo/list-consumo");
		ModelAndView modelandview = new ModelAndView("buscar-consumo-admin");
		try{
		
			if(!run.equals("")){
				
				Persona persona = usuarioService.getRunPersona(run);
				Contacto contacto = usuarioService.getContacto(persona.getId_contacto());
				Usuario usuario = usuarioService.getUsuario(contacto.getId_usuario());
				
				if(usuario != null){
					
					List<Consumo> listaConsumo = consumoService.getConsumo();
					List<Servicio> listaServicio =  servicioService.getServicio();
					List<DetalleServicio> listaDetalle = servicioDetalle.getDetalleServicio();
					List<HorarioServicio> listaHorario = servicioHorario.getHorarioServicio();
					
					int precio = consumoService.getTotalPorIdUsuario(usuario.getId());
					
					modelandview.addObject("precio", precio);
					modelandview.addObject("usuario", usuario);
					modelandview.addObject("listaConsumo", listaConsumo);
					modelandview.addObject("listaServicio", listaServicio);
					modelandview.addObject("listaDetalle", listaDetalle);
					modelandview.addObject("listaHorario", listaHorario);
					
					modelandview.setViewName("list-consumo-admin");
					
				}else{
					
					modelandview.setViewName("buscar-consumo-admin");
					modelandview.addObject("message","Persona No existe");
				}
				
			}
		}catch(Exception ex){
			modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			
		}
		return modelandview;
		
	}
	
	@RequestMapping(value="/list-consumo", method=RequestMethod.GET)
	public ModelAndView listConsumoPage(@RequestParam("usuario") Usuario usuario){
		
		ModelAndView modelandview = new ModelAndView("list-consumo-admin");
		try{
			List<Servicio> listaServicio =  servicioService.getServicio();
			List<DetalleServicio> listaDetalle = servicioDetalle.getDetalleServicio();
			List<HorarioServicio> listaHorario = servicioHorario.getHorarioServicio();
			
			
			modelandview.addObject("usuario", usuario);
			modelandview.addObject("listaServicio", listaServicio);
			modelandview.addObject("listaDetalle", listaDetalle);
			modelandview.addObject("listaHorario", listaHorario);
		}catch(Exception ex){
			modelandview.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			}else{
				modelandview.addObject("message",ex.getMessage());
			}
		}
		return modelandview;
	}
	
	
	@RequestMapping(value="/agregar/{id}", method=RequestMethod.POST)
	public ModelAndView addingConsumoFormPage(@RequestParam("idUs") String id_usuario,
											   @PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("");
						
		try{
			Consumo con = new Consumo();
			int parsedIdUsuario = Integer.parseInt(id_usuario);
			int id_servicioInt = Integer.parseInt(id);
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			con.setEstado(1);
			con.setId_servicio(id_servicioInt);
			con.setId_usuario(parsedIdUsuario);
			con.setFecha(date);
			
			
			consumoService.addConsumo(con);
			
			List<Consumo> listaConsumo = consumoService.getConsumo();
			List<Servicio> listaServicio =  servicioService.getServicio();
			List<DetalleServicio> listaDetalle = servicioDetalle.getDetalleServicio();
			List<HorarioServicio> listaHorario = servicioHorario.getHorarioServicio();
			

			Usuario usuario = usuarioService.getUsuario(parsedIdUsuario);
			int precio = consumoService.getTotalPorIdUsuario(usuario.getId());
			
			modelandview.addObject("precio", precio);
			modelandview.addObject("usuario", usuario);
			modelandview.addObject("listaConsumo", listaConsumo);
			modelandview.addObject("listaServicio", listaServicio);
			modelandview.addObject("listaDetalle", listaDetalle);
			modelandview.addObject("listaHorario", listaHorario);
			
			modelandview.setViewName("list-consumo-admin");
		
		}catch(Exception ex){
			modelandview.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			}else{
				modelandview.addObject("message",ex.getMessage());
			}
		}
		return modelandview;
		
	}
	
	@RequestMapping(value="/add-consu", method=RequestMethod.POST)
	public ModelAndView addingConsumoFormPage(@RequestParam("id_usuario") String id_usuario){
		ModelAndView modelandview = new ModelAndView("add-consumo-form-admin");
		
		try{
			int idUsuarioParsed = Integer.parseInt(id_usuario);
			Usuario usuario = usuarioService.getUsuario(idUsuarioParsed);
			
			
			if(usuario!=null){
				List<Servicio> listaServicio =  servicioService.getServicio();
				List<DetalleServicio> listaDetalle = servicioDetalle.getDetalleServicio();
				List<HorarioServicio> listaHorario = servicioHorario.getHorarioServicio();
				int precio = consumoService.getTotalPorIdUsuario(usuario.getId());
				
				modelandview.addObject("precio", precio);
				modelandview.addObject("usuario", usuario);
				modelandview.addObject("listaServicio", listaServicio);
				modelandview.addObject("listaDetalle", listaDetalle);
				modelandview.addObject("listaHorario", listaHorario);
			}else{
				throw new Exception("Problemas al procesar la transaccion, porfavor intentelo mas tarde.");
			}
		}catch(Exception ex){
			modelandview.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			}else{
				modelandview.addObject("message",ex.getMessage());
			}
		}
		
		return modelandview;
						
	}
	
	@RequestMapping(value="/borrar/{id}", method=RequestMethod.POST)
	public ModelAndView borrarConsumoFormPage(@RequestParam("idUs") String id_usuario,
											   @PathVariable("id") String id){
		
		ModelAndView modelandview = new ModelAndView("list-consumo-admin");
						
		try{
			
			int parsedIdUsuario = Integer.parseInt(id_usuario);
			int idConsumo = Integer.parseInt(id);
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			Consumo con = consumoService.getConsumo(idConsumo);
			
			con.setEstado(0);
			
			
			consumoService.updateConsumo(con);
			
			List<Consumo> listaConsumo = consumoService.getConsumo();
			List<Servicio> listaServicio =  servicioService.getServicio();
			List<DetalleServicio> listaDetalle = servicioDetalle.getDetalleServicio();
			List<HorarioServicio> listaHorario = servicioHorario.getHorarioServicio();
			

			Usuario usuario = usuarioService.getUsuario(parsedIdUsuario);
			int precio = consumoService.getTotalPorIdUsuario(usuario.getId());
			
			modelandview.addObject("precio", precio);
			modelandview.addObject("usuario", usuario);
			modelandview.addObject("listaConsumo", listaConsumo);
			modelandview.addObject("listaServicio", listaServicio);
			modelandview.addObject("listaDetalle", listaDetalle);
			modelandview.addObject("listaHorario", listaHorario);
			
		
		}catch(Exception ex){
			modelandview.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			}else{
				modelandview.addObject("message",ex.getMessage());
			}
		}
		return modelandview;
		
	}
	
	@RequestMapping(value="/pago", method=RequestMethod.POST)
	public ModelAndView addingPagoFormPage(@RequestParam("id_usuario") String id_usuario){
		
		ModelAndView modelandview = new ModelAndView("");
						
		try{
			Pago pag = new Pago();
		
			int id_usuarioInt = Integer.parseInt(id_usuario);
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			Usuario usuario = usuarioService.getUsuario(id_usuarioInt);
			int total = consumoService.getTotalPorIdUsuario(usuario.getId());
			List<Consumo> listaConsumo = consumoService.getConsumoPorEstado(id_usuarioInt);
					
			
			pag.setFecha(date);
			pag.setTotal(total);
			pag.setId_usuario(id_usuarioInt);
			pag.setId_descuento(0);
			pag.setCargo_adicional_id(0);
			
			for (int i = 0; i < listaConsumo.size(); i++) {
				
				Consumo con = listaConsumo.get(i);
				con.setEstado(0);
				consumoService.updateConsumo(con);
				
			}
			
			consumoService.addPago(pag);
			
			modelandview.setViewName("list-consumo-admin");
		
		}catch(Exception ex){
			modelandview.setViewName("error-page-admin");
			if(ex.getMessage().equals("")){
				modelandview.addObject("message","Problemas al procesar la solicitud, porfavor intente nuevamente.");
			}else{
				modelandview.addObject("message",ex.getMessage());
			}
		}
		return modelandview;
		
	}
	
	
	
	
	
	
	
	
	
	
		
	}

