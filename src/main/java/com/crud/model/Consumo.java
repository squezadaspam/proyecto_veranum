package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="consumo")
public class Consumo {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONSUMO_ID_SEQUENCE")
	@SequenceGenerator(name="CONSUMO_ID_SEQUENCE", sequenceName="CONSUMO_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="estado")
	private Integer estado;
	
	@Column(name="id_servicio")
	private Integer id_servicio;
	
	@Column(name="id_usuario")
	private Integer id_usuario;
	
	@Column(name="fecha")
	private java.sql.Date fecha;
	
	public java.sql.Date getFecha() {
		return fecha;
	}
	public void setFecha(java.sql.Date fecha) {
		this.fecha = fecha;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Integer getId_servicio() {
		return id_servicio;
	}
	public void setId_servicio(Integer id_servicio) {
		this.id_servicio = id_servicio;
	}
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
}
