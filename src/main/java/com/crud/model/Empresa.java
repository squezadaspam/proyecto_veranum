package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="empresa")
public class Empresa{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPRESA_ID_SEQUENCE")
	@SequenceGenerator(name="EMPRESA_ID_SEQUENCE", sequenceName="EMPRESA_ID_SEQUENCE")
	private Integer id;
	
	private Integer id_contacto;

	private String rut;
	
	private String nombre;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId_contacto() {
		return id_contacto;
	}
	public void setId_contacto(Integer id_contacto) {
		this.id_contacto = id_contacto;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}

}
