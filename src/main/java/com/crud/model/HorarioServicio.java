package com.crud.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


import java.io.Serializable;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.time.*;
import java.time.format.DateTimeFormatter;
@Entity
@Table(name="Horario_servicio")
public class HorarioServicio implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HARARIO_SERVICIO_ID_SEQUENCE")
	@SequenceGenerator(name="HARARIO_SERVICIO_ID_SEQUENCE", sequenceName="HARARIO_SERVICIO_ID_SEQUENCE")
	private Integer id;
    
	@Column(name="hora_inicio")
	private java.sql.Timestamp hora_Inicio;

	@Column(name="hora_fin")
	private java.sql.Timestamp hora_Fin;

	public LocalDateTime getHora_InicioLocalDateTime() {
		LocalDateTime ld = this.hora_Inicio.toLocalDateTime();
		return ld;
	}
	
	public Timestamp getHora_Inicio() {
		return this.hora_Inicio;
	}

	public void setHora_Inicio(Timestamp hora_Inicio) {
		this.hora_Inicio = hora_Inicio;
	}
	
	public void setHora_Inicio(LocalDateTime hora_Inicio) {
		Timestamp ts = Timestamp.valueOf(hora_Inicio);
		this.hora_Inicio = ts;
	}

	public LocalDateTime getHora_FinLocalDateTime() {
		LocalDateTime ld = this.hora_Fin.toLocalDateTime();
		return ld;
	}
	
	public Timestamp getHora_Fin() {
		return this.hora_Fin;
	}

	public void setHora_Fin(LocalDateTime hora_fin) {
		Timestamp ts = Timestamp.valueOf(hora_fin);
		this.hora_Fin = ts;
	}
	
	public void setHora_Fin(Timestamp hora_fin) {
		this.hora_Fin = hora_fin;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getHora_InicioFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = this.getHora_InicioLocalDateTime();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
	
	public String getHora_FinFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = this.getHora_FinLocalDateTime();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}

	


	
}
	

	
