package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="direccion")
public class Direccion {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DIRECCION_ID_SEQUENCE")
	@SequenceGenerator(name="DIRECCION_ID_SEQUENCE", sequenceName="DIRECCION_ID_SEQUENCE")
	private Integer id;
	
	private Integer id_ciudad;

	private String calle;

	private String cod_postal;
	private Integer numero;
	private String num_depto;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId_ciudad() {
		return id_ciudad;
	}
	public void setId_ciudad(Integer id_ciudad) {
		this.id_ciudad = id_ciudad;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCod_postal() {
		return cod_postal;
	}
	public void setCod_postal(String cod_postal) {
		this.cod_postal = cod_postal;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getNum_depto() {
		return num_depto;
	}
	public void setNum_depto(String num_depto) {
		this.num_depto = num_depto;
	}
	
}