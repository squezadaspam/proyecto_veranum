package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="pais")
public class Pais {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAIS_ID_SEQUENCE")
	@SequenceGenerator(name="PAIS_ID_SEQUENCE", sequenceName="PAIS_ID_SEQUENCE")
	private Integer id;

	private String nombre;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {

		return this.nombre;
	}
	
}