package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="telefono")
public class Telefono{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TELEFONO_ID_SEQUENCE")
	@SequenceGenerator(name="TELEFONO_ID_SEQUENCE", sequenceName="TELEFONO_ID_SEQUENCE")
	private Integer id;
	private Integer id_contacto;
	
	private String nombre;

	private String numero;
	
	private Integer estado;
	
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Integer getId_contacto() {
		return id_contacto;
	}
	public void setId_contacto(Integer id_contacto) {
		this.id_contacto = id_contacto;
	}
}