package com.crud.model;

import java.sql.Date;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ESQUEMA")
public class Esquema {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ESQUEMA_ID_SEQUENCE")
	@SequenceGenerator(name="ESQUEMA_ID_SEQUENCE", sequenceName="ESQUEMA_ID_SEQUENCE")
	private int id;
	
	@Column(name="id_habitacion")
	private int idHabitacion;	
	
	@Column(name="fecha")
	private Date fecha;
	
	@Column(name="id_reserva")
	private int idReserva;	
	
	@Column(name="estado")
	private int estado;
	
	@Column(name="reservada")
	private int reservada;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdHabitacion() {
		return idHabitacion;
	}

	public void setIdHabitacion(int idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
    
	public LocalDate getFechaLocalDate() {
		LocalDate ld = this.fecha.toLocalDate();
		return ld;
	}
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public void setFecha(LocalDate fecha) {
		Date ts = Date.valueOf(fecha);
		this.fecha = ts;
	}
	
	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getReservada() {
		return reservada;
	}

	public void setReservada(int reservada) {
		this.reservada = reservada;
	}
	
	
	
}
