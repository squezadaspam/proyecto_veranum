package com.crud.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.time.*;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name="incidente")
public class Incidente{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INCIDENTE_ID_SEQUENCE")
	@SequenceGenerator(name="INCIDENTE_ID_SEQUENCE", sequenceName="INCIDENTE_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="fecha_registro")
	private Date fecha_registro;
	
	@Column(name="fecha_suceso")
	private Date fecha_suceso;
	
	@Column(name="id_hotel")
	private Integer id_hotel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Date getFecha_suceso() {
		return fecha_suceso;
	}

	public void setFecha_suceso(Date fecha_suceso) {
		this.fecha_suceso = fecha_suceso;
	}

	public Integer getId_hotel() {
		return id_hotel;
	}

	public void setId_hotel(Integer id_hotel) {
		this.id_hotel = id_hotel;
	}
	
	
}
