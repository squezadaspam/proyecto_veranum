package com.crud.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="TIPO_HABITACION")
public class TipoHabitacion implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TIPO_HABIACION_ID_SEQUENCE")
	@SequenceGenerator(name="TIPO_HABIACION_ID_SEQUENCE", sequenceName="TIPO_HABIACION_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="tipo")
	private String tipo;	
	
	@Column(name="descripcion")
	private String descripcion;	
	
	@Column(name="precio_base")
	private Integer precio_base;
	
	@Column(name="capacidad_maxima")
	private Integer capacidad_maxima;
	
	
	
	
	
	public Integer getCapacidad_maxima() {
		return capacidad_maxima;
	}
	public void setCapacidad_maxima(Integer capacidad_maxima) {
		this.capacidad_maxima = capacidad_maxima;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getPrecio_base() {
		return precio_base;
	}
	public void setPrecio_base(Integer precio_base) {
		this.precio_base = precio_base;
	}

	
	
	
}
