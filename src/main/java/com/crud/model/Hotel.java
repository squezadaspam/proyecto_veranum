package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="hotel")
public class Hotel {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOTEL_ID_SEQUENCE")
	@SequenceGenerator(name="HOTEL_ID_SEQUENCE", sequenceName="HOTEL_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="categoria")
	private Integer categoria;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	
}
