package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="DETALLE_SERVICIO")
public class DetalleServicio {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETALLE_SERVICIO_ID_SEQUENCE")
	@SequenceGenerator(name="DETALLE_SERVICIO_ID_SEQUENCE", sequenceName="DETALLE_SERVICIO_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="monto")
	private Integer monto;
	
	@Column(name="descripcion")
	private String descripcion;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMonto() {
		return monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
}
