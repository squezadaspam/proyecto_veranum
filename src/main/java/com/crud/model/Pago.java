package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.sql.Date;


@Entity
@Table(name="pago")
public class Pago {

@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAGO_ID_SEQUENCE")
@SequenceGenerator(name="PAGO_ID_SEQUENCE", sequenceName="PAGO_ID_SEQUENCE")
private Integer id;
	
	@Column(name="total")
	private Integer total;
	
	@Column(name="fecha")
	private java.sql.Date fecha;
	
	@Column(name="id_descuento")
	private Integer id_descuento;
	
	@Column(name="cargo_adicional_id")
	private Integer cargo_adicional_id;
	
	@Column(name="id_usuario")
	private Integer id_usuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public java.sql.Date getFecha() {
		return fecha;
	}

	public void setFecha(java.sql.Date fecha) {
		this.fecha = fecha;
	}

	public Integer getId_descuento() {
		return id_descuento;
	}

	public void setId_descuento(Integer id_descuento) {
		this.id_descuento = id_descuento;
	}

	public Integer getCargo_adicional_id() {
		return cargo_adicional_id;
	}

	public void setCargo_adicional_id(Integer cargo_adicional_id) {
		this.cargo_adicional_id = cargo_adicional_id;
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	
	
	
}
