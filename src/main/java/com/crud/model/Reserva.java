package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.crud.service.ReservaServiceImpl;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name="reserva")
public class Reserva implements Serializable, Comparable<Reserva> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RESERVA_ID_SEQUENCE")
	@SequenceGenerator(name="RESERVA_ID_SEQUENCE", sequenceName="RESERVA_ID_SEQUENCE")
	private int id;
	
	@Column(name="fecha_inicio")
	private Date fechaInicio;

	@Column(name="fecha_fin")
	private Date fechaFin;
	
	private int estado;
	
	@Column(name="id_usuario")
	private int idUsuario;
	
	@Column(name="id_ocupacion")
	private int idOcupacion;
	
	@Column(name="id_habitacion")
	private int idHabitacion;
	
	@Column(name="ocupada")
	private int ocupada;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="ninos")
	private int ninos;
	
	@Column(name="precio")
	private int precio;
	
	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getNinos() {
		return ninos;
	}

	public void setNinos(int ninos) {
		this.ninos = ninos;
	}

	public int getAdultos() {
		return adultos;
	}

	public void setAdultos(int adultos) {
		this.adultos = adultos;
	}

	@Column(name="adultos")
	private int adultos;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(){
		System.out.print("Generacion de codigo");
		this.codigo = this.generarHash();
	}
	
	public void setCodigo(String codigo){
		this.codigo = codigo;
	}

	public int getOcupada() {
		return ocupada;
	}

	public void setOcupada(int ocupada) {
		this.ocupada = ocupada;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getFechaInicioLocalDate() {
		LocalDate ld = this.fechaInicio.toLocalDate();
		return ld;
	}
	
	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaInicio(LocalDate fechaInicio) {
		Date ts = Date.valueOf(fechaInicio);
		this.fechaInicio = ts;
	}

	public LocalDate getFechaFinLocalDate() {
		LocalDate ld = this.fechaFin.toLocalDate();
		return ld;
	}
	
	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(LocalDate fecha_fin) {
		Date ts = Date.valueOf(fecha_fin);
		this.fechaFin = ts;
	}
	
	public void setFechaFin(Date fecha_fin) {
		this.fechaFin = fecha_fin;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdOcupacion() {
		return idOcupacion;
	}

	public void setIdOcupacion(int idOcupacion) {
		this.idOcupacion = idOcupacion;
	}

	public int getIdHabitacion() {
		return idHabitacion;
	}

	public void setIdHabitacion(int idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	
	public String getFechaInicioFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dateTime = this.getFechaInicioLocalDate();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
	
	public String getFechaFinFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dateTime = this.getFechaFinLocalDate();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
	
	public int getFechaInicioComoNumeroDeDiaDeAno(){
		return getFechaInicioLocalDate().getDayOfYear();
	}
	
	public int getFechaFinComoNumeroDeDiaDeAno(){
		return getFechaFinLocalDate().getDayOfYear();
	}
	
	
	private String generarHash() {
		String upToNCharacters;
		try{		
			String toEnc = this.getFechaInicioFormateada()+this.getFechaFinFormateada()+this.id+"VERANUM"+LocalDateTime.now().toString();; // Value to hash
			MessageDigest mdEnc = MessageDigest.getInstance("MD5"); 
			mdEnc.update(toEnc.getBytes(), 0, toEnc.length());
			String md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Hash value
	        System.out.println(md5);
	        upToNCharacters = md5.substring(0, Math.min(md5.length(), 5));
		}catch(Exception e){
			upToNCharacters = "00000";
		}
        return upToNCharacters;
	}
	
	public int calcularPrecio(int precioBase){
		int resultado = 0;
		int dias = (int) ChronoUnit.DAYS.between(this.getFechaInicioLocalDate(), this.getFechaFinLocalDate());
		if(dias <= 0){
			dias = 1;
		}
		resultado = dias * precioBase;
		return resultado;
	}

	@Override
	public int compareTo(Reserva reserva) {
		return this.getFechaInicioLocalDate().isBefore(reserva.getFechaInicioLocalDate())? -1 : 1;
	}
	
	
	
}
