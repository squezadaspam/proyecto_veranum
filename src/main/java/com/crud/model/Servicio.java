package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="SERVICIO")
public class Servicio {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICIO_ID_SEQUENCE")
	@SequenceGenerator(name="SERVICIO_ID_SEQUENCE", sequenceName="SERVICIO_ID_SEQUENCE")
	
	private Integer id;
	
	@Column(name="id_detalle_servicio")
	private Integer id_detalle_servicio;
	
	@Column(name="id_horario_servicio")
	private Integer id_horario_servicio;
	
	@Column(name="estado")
	private Integer estado;
	
	@Column(name="nombre")
	private String nombre;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId_detalle_servicio() {
		return id_detalle_servicio;
	}

	public void setId_detalle_servicio(Integer id_detalle_servicio) {
		this.id_detalle_servicio = id_detalle_servicio;
	}

	public Integer getId_horario_servicio() {
		return id_horario_servicio;
	}

	public void setId_horario_servicio(Integer id_horario_servicio) {
		this.id_horario_servicio = id_horario_servicio;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}



	

	
}
