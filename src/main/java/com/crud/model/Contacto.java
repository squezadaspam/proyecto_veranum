package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="contacto")
public class Contacto {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONTACTO_ID_SEQUENCE")
	@SequenceGenerator(name="CONTACTO_ID_SEQUENCE", sequenceName="CONTACTO_ID_SEQUENCE")
	private Integer id;
	
	private Integer id_usuario;
	private Integer id_direccion;

	private String nombre;

	private String email;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Integer getId_direccion() {
		return id_direccion;
	}
	public void setId_direccion(Integer id_direccion) {
		this.id_direccion = id_direccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}