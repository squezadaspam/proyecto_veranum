package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="empleado")
public class Empleado {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPLEADO_ID_SEQUENCE")
	@SequenceGenerator(name="EMPLEADO_ID_SEQUENCE", sequenceName="EMPLEADO_ID_SEQUENCE")
	private Integer id;
	
	@Column(name="id_cargo")
	private Integer id_cargo;
	
	@Column(name="estado")
	private Integer estado;

	@Column(name="id_persona")
	private Integer id_persona;

	@Column(name="id_hotel")
	private Integer id_hotel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId_cargo() {
		return id_cargo;
	}

	public void setId_cargo(Integer id_cargo) {
		this.id_cargo = id_cargo;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getId_persona() {
		return id_persona;
	}

	public void setId_persona(Integer id_persona) {
		this.id_persona = id_persona;
	}

	public Integer getId_hotel() {
		return id_hotel;
	}

	public void setId_hotel(Integer id_hotel) {
		this.id_hotel = id_hotel;
	}
	
	
	
}
