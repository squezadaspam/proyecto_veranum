package com.crud.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="CARGO_ADICIONAL")
public class Temporada {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="my_entity_seq_gen")
	@SequenceGenerator(name="my_entity_seq_gen", sequenceName="CARGO_ADICIONAL_ID_SEQUENCE")
	private int id;
	
	@NotNull
	@Column(name="DESCRIPCION")
	private String nombre;
	
	@NotNull
	@Column(name="fecha_inicio")
	private Date  fechaIni;
	
	@NotNull
	@Column(name="fecha_fin")
	private Date  fechaFin;
	
	@NotNull
	@Column(name="factor")
	private double cargo;
	
	@NotNull
	@Column(name="estado")
	private int estado;

	public Temporada(){}
	
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date  date) {
		fechaIni = date;
	}
	
	public void setFechaIni(LocalDate fecha_ini) {
		Date ts = Date.valueOf(fecha_ini);
		this.fechaIni = ts;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date  date) {
		fechaFin = date;
	}
	
	public void setFechaFin(LocalDate fecha_fin) {
		Date ts = Date.valueOf(fecha_fin);
		this.fechaFin = ts;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	
	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	public String getFechaInicioFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dateTime = this.getFechaInicioLocalDate();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
	
	private LocalDate getFechaInicioLocalDate() {
		LocalDate ld = this.fechaIni.toLocalDate();
		return ld;
		
	}

	public String getFechaFinFormateada(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dateTime = this.getFechaFinLocalDate();
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}

	private LocalDate getFechaFinLocalDate() {
		LocalDate ld = this.fechaFin.toLocalDate();
		return ld;
		
	}
	
	
	
	
}
