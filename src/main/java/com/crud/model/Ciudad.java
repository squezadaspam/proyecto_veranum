package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="ciudad")
public class Ciudad {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CIUDAD_ID_SEQUENCE")
	@SequenceGenerator(name="CIUDAD_ID_SEQUENCE", sequenceName="CIUDAD_ID_SEQUENCE")
	private Integer id;

	private String nombre;
	
	private Integer id_pais;
	
	public Integer getId_pais() {
		return id_pais;
	}
	public void setId_pais(Integer id_pais) {
		this.id_pais = id_pais;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}