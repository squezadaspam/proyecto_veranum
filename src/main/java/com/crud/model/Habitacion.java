package com.crud.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import com.crud.service.TipoHabitacionService;
import com.crud.service.TipoHabitacionServiceImpl;

@Entity
@Table(name="Habitacion")
public class Habitacion implements Serializable {

	

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HABITACION_ID_SEQUENCE")
	@SequenceGenerator(name="HABITACION_ID_SEQUENCE", sequenceName="HABITACION_ID_SEQUENCE")
	private int id;
	
	@Column(name="numero")
	private int numero;
	
	@Column(name="estado")
	private int estado;
	
	@Column(name="id_tipo_habitacion")
	private int idTipoHabitacion;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getIdTipoHabitacion() {
		return idTipoHabitacion;
	}
	public void setIdTipoHabitacion(int idTipoHabitacion) {
		this.idTipoHabitacion = idTipoHabitacion;
	}
	
	
}

