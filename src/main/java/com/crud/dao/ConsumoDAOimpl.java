package com.crud.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Consumo;
import com.crud.model.Esquema;
import com.crud.model.Pago;
import com.crud.model.Reserva;

@Repository
public class ConsumoDAOimpl implements ConsumoDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addConsumo(Consumo consumo) {
		getCurrentSession().save(consumo);
		
	}
	@Override
	public Consumo getConsumo(int id) {
		Consumo consumo = (Consumo) getCurrentSession().createQuery("from Consumo where id = :idConsumo")
				.setParameter("idConsumo", id)
				.uniqueResult();
		return consumo;
	}

	@Override
	public void deleteConsumo(int id) {
		Consumo consumo = getConsumo(id);
		if(consumo != null) {
			getCurrentSession().delete(consumo);
		}
		
	}

	@Override
	public List<Consumo> getConsumo() {
		return (List<Consumo>)getCurrentSession().createQuery("from Consumo where estado = 1").list();
	}

	@Override
	public Consumo getConsumoInt(int id) {
		Consumo consumo = (Consumo) getCurrentSession().createSQLQuery("select * from Consumo where id = :id").addEntity(Consumo.class)
				.setParameter("id", id)
				.uniqueResult();
		return consumo;
	}
	
	@Override
	public int getTotalPorIdUsuario(int id) {
		BigDecimal total = (BigDecimal) getCurrentSession().createSQLQuery("select sum(d.monto) from detalle_servicio d, servicio s, horario_servicio h, consumo c where s.id_detalle_servicio = d.id and s.id_horario_servicio = h.id and c.id_servicio = s.id and c.estado = 1 and c.id_usuario = :id")
				.setParameter("id", id)
				.uniqueResult();
		return total != null ? total.intValue() : 0;
	}
	
	@Override
	public void updateConsumo(Consumo consumo) {
		getCurrentSession().update(consumo);
		
	}

	@Override
	public void addPago(Pago pago) {
		getCurrentSession().save(pago);
		
	}

	@Override
	public List<Consumo> getConsumoPorEstado(int idConsumo) {
		List<Consumo> lista = (List<Consumo>) getCurrentSession().createQuery("from Consumo where id_usuario = :id_usuario and estado = 1")
				.setParameter("id_usuario", idConsumo)
				.list();
		
		return lista;
	}
	
	
	

}
