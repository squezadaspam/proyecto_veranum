package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Hotel;
import com.crud.model.Hotel;

@Repository
public class HotelDAOimpl implements HotelDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addHotel(Hotel hotel) {

		getCurrentSession().save(hotel);
		
	}

	@Override
	public boolean updateHotel(Hotel hotel) {
		
		try{
			
			Hotel hotelToUpdate = getHotel(hotel.getId());
					
			getCurrentSession().update(hotelToUpdate);
			
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Hotel getHotel(int id) {

		Hotel hotel = (Hotel) getCurrentSession().createQuery("from Hotel where id = :i")
				.setParameter("i", id)
				.uniqueResult();
		return hotel;
	}


	@SuppressWarnings("unchecked")
	public List<Hotel> getHoteles() {
		
		return getCurrentSession().createQuery("from Hotel").list();
	}

}
