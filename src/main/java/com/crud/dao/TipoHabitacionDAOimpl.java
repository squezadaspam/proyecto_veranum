package com.crud.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Habitacion;
import com.crud.model.Servicio;
import com.crud.model.TipoHabitacion;

@Repository
public class TipoHabitacionDAOimpl implements TipoHabitacionDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public TipoHabitacion getTipoHabitacion(int id) {
		return (TipoHabitacion) getCurrentSession().createQuery("from TipoHabitacion where id = :id").setParameter("id", id).uniqueResult();

	}
	
	@Override
	public List<TipoHabitacion> getTipoHabitaciones() {
		return (List<TipoHabitacion>)getCurrentSession().createQuery("from TipoHabitacion").list();
	}
	

}
