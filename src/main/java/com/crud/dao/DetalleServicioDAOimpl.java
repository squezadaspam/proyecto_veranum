package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.DetalleServicio;



@Repository
public class DetalleServicioDAOimpl implements DetalleServicioDAO{

	@Autowired
	private SessionFactory sessionFactory; 
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addDetalleServicio(DetalleServicio detalleServicio) {
		getCurrentSession().save(detalleServicio);
		
	}

	@Override
	public void updateDetalleServicio(DetalleServicio detalleServicio) {
	
		DetalleServicio detalleServicioToUpdate = getDetalleServicio(detalleServicio.getId());
		detalleServicioToUpdate.setDescripcion(detalleServicio.getDescripcion());
		detalleServicioToUpdate.setMonto(detalleServicio.getMonto());
		getCurrentSession().update(detalleServicioToUpdate);
	}

	@Override
	public DetalleServicio getDetalleServicio(int id) {
		
		DetalleServicio detalle = (DetalleServicio) getCurrentSession().createSQLQuery("select * from Detalle_servicio where id = :idDetalle_servicio").addEntity(DetalleServicio.class)
				.setParameter("idDetalle_servicio", id)
				.uniqueResult();
		
		return detalle;
	}


	@SuppressWarnings("unchecked")
	public List<DetalleServicio> getDetalleServicio() {
		
		return getCurrentSession().createQuery("from DetalleServicio").list();
	}

	
	public boolean verificarDetalleServicio(int id) {
		
		DetalleServicio detalleServicio = getDetalleServicio(id);
		
		if(detalleServicio != null){
			
			return true;
		}
		
		return false;
	}

	@Override
	public DetalleServicio getDetalleServicioInt(int id) {
		
		DetalleServicio detalleServicio = (DetalleServicio) getCurrentSession().createSQLQuery("select * from Detalle_servicio where id = :id").addEntity(DetalleServicio.class)
				.setParameter("id", id)
				.uniqueResult();
		
		return detalleServicio;
	}


	
	
	
}