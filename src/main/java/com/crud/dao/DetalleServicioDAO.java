package com.crud.dao;

import java.util.List;

import com.crud.model.DetalleServicio;


public interface DetalleServicioDAO {

	public void addDetalleServicio (DetalleServicio detalleServicio);
	public void updateDetalleServicio(DetalleServicio detalleServicio);
	public DetalleServicio getDetalleServicio(int id);
	public List<DetalleServicio> getDetalleServicio();
	boolean verificarDetalleServicio(int id);
	public DetalleServicio getDetalleServicioInt(int id);
	
}
