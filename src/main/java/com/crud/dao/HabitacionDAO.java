package com.crud.dao;

import java.util.ArrayList;
import java.util.List;

import com.crud.model.Habitacion;
import com.crud.model.TipoHabitacion;

public interface HabitacionDAO {
	public void addHabitacion(Habitacion Habitacion);
	public List<TipoHabitacion> getTipos();
	public void updateHabitacion(Habitacion Habitacion);
	public Habitacion getHabitacion(int id);
	public void deleteHabitacion(int id);
	public List<Habitacion> listarHabitaciones();
	public List<Habitacion> getHabitaciones();
	public int getCantidadHabitacionesHabilitadas();
	public List<Habitacion> getHabitacionesHabilitadas();
}
