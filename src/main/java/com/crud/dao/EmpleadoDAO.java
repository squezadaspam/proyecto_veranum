package com.crud.dao;

import java.util.List;

import com.crud.model.Cargo;
import com.crud.model.Empleado;
import com.crud.model.Persona;
import com.crud.model.Usuario;

public interface EmpleadoDAO {

	public void addEmpleado(Empleado empleado);
	public boolean updateEmpleado(Empleado empleado);
	public Empleado getEmpleado(int id);
	public boolean cambiarEstadoEmpleado(int id);
	public List<Empleado> getEmpleados();
	
}
