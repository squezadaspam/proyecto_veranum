package com.crud.dao;

import java.util.List;

import com.crud.model.Incidente;

public interface IncidenteDAO {
	
	public void addIncidente( Incidente incidente);
	public boolean updateIncidente(Incidente incidente);
	public Incidente getIncidenteInt(int id);
	public List<Incidente> getIncidentes();
}