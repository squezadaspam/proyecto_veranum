package com.crud.dao;

import java.util.List;


import com.crud.model.Servicio;


public interface ServicioDAO {

	public void addServicio(Servicio servicio);
	public boolean updateServicio(Servicio servicio); 
	public Servicio getServicio(int id);
	public void deleteServicio(int id);
	public List<Servicio> getServicios();
	public Servicio getServicioInt(int id);
	
}
