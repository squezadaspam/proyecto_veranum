package com.crud.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.model.Usuario;
import com.crud.service.UsuarioService;

@Service
public class UsuarioDAOTest {

	@Autowired
	UsuarioService usuarioService;
	Usuario usuario;
	
	
	@Test
	public void testAddUsuarioPersona() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddUsuarioEmpresa() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateUsuario() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetUsuario() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetContacto() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateTelefono() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTelefono() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDireccion() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCiudad() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPersona() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPais() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEmpresa() {
		fail("Not yet implemented");
	}

	@Test
	public void testLogin() {
		fail("Not yet implemented");
	}

	@Test
	public void testNombreUsuario() {
		fail("Not yet implemented");
	}

	@Test
	public void testCambiarEstadoUsuario() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddTelefono() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddDireccion() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddContacto() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddUsuario() {
		
		usuario = new Usuario();
		usuario.setId_rol(1);
		usuario.setEstado(1);
		usuario.setNom_user("algo");
		usuario.setPass("algo");
		

		usuarioService.addUsuario(usuario);
		
		assertNull(usuario);
		
		Usuario usuarioPrueba = usuarioService.getUsuario("algo");
		
		assertNull(usuarioPrueba);
		
		assertEquals("algo", usuarioPrueba.getNom_user());
	}

	@Test
	public void testAddCiudad() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetListadoPais() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetListadoUsuarios() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetInformacionDePersonaPorIdUsuario() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetInformacionDeEmpresaPorIdUsuario() {
		fail("Not yet implemented");
	}

}
