package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Cargo;
import com.crud.model.Persona;

@Repository
public class CargoDAOimpl implements CargoDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addCargo(Cargo cargo){
		
		getCurrentSession().save(cargo);
	}

	

	@Override
	public Cargo getCargo(String nombreCargo) {
		
		Cargo cargo = (Cargo) getCurrentSession().createQuery("from Cargo where nombre = :nombreCargo")
				.setParameter("nombreCargo", nombreCargo)
				.uniqueResult();
		
		return cargo;
	}

	

	@SuppressWarnings("unchecked")
	public List<Cargo> getCargos() {
		
		return getCurrentSession().createQuery("from Cargo").list();
	}
	
	@Override
	public boolean updateCargo(Cargo cargo) {
		
		Cargo cargoToUpdate = getCargoPorId(cargo.getId());
		cargoToUpdate.setNombre(cargo.getNombre());
		cargoToUpdate.setDescripcion(cargo.getDescripcion());
		cargoToUpdate.setEstado(cargo.getEstado());
		getCurrentSession().update(cargoToUpdate);
		return true;
	}
	
	@Override
	public boolean cambiarEstadoCargo(String nombreCargo) {
		
		try{
			Cargo cargoToUpdate = getCargo(nombreCargo);
			cargoToUpdate.setEstado(0);
			getCurrentSession().update(cargoToUpdate);
			
			return true;
			
		}catch(Exception e){
			
			return false;
		}
		
	}

	@Override
	public Cargo getCargoPorId(int id) {
		
		Cargo cargo = (Cargo) getCurrentSession().createQuery("from Cargo where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		
		return cargo;

	}

	@Override
	public Cargo getCargoInt(int id) {
		
		Cargo cargo = (Cargo) getCurrentSession().createQuery("from Cargo where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		
		return cargo;
	}

	@Override
	public List<Cargo> getCargosActivos() {
		return getCurrentSession().createQuery("from Cargo where estado = 1").list();
	}

	@Override
	public boolean getCargoPorNombre(String nombre) {
		
		Cargo cargo = (Cargo) getCurrentSession().createQuery("from Cargo where nombre = :nombre")
				.setParameter("nombre", nombre)
				.uniqueResult();
		
		if(cargo != null){
			
			return true;
		}	
		
		return false;
	}
	
		
	

}
