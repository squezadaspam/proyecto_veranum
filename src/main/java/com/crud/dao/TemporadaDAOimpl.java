package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Temporada;


@Repository
public class TemporadaDAOimpl implements TemporadaDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	public void addTemporada(Temporada temporada){
		getCurrentSession().save(temporada);
	}
	
	@Override
	public void updateTemporada(Temporada temporada) {
		
		Temporada temporadaToUpdate = getTemporada(temporada.getId());
		temporadaToUpdate.setNombre(temporada.getNombre());
		temporadaToUpdate.setFechaIni(temporada.getFechaIni());
		temporadaToUpdate.setFechaFin(temporada.getFechaFin());
		temporadaToUpdate.setCargo(temporada.getCargo());
		temporadaToUpdate.setEstado(temporada.getEstado());
		getCurrentSession().update(temporadaToUpdate);
		
	}
	
	@Override
	public Temporada getTemporada(int id) {
		Temporada temporada = (Temporada) getCurrentSession().get(Temporada.class, id);
		return temporada;
	}
	
	@Override
	public void deleteTemporada(int id) {
		
		Temporada temporada = getTemporada(id);
		if(temporada != null)
			getCurrentSession().delete(temporada);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Temporada> getTemporadas() {
		return getCurrentSession().createQuery("from Temporada where estado=1").list();
	}
	
	
		
	
	
}
