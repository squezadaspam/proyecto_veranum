package com.crud.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Habitacion;
import com.crud.model.TipoHabitacion;

@Repository
public class HabitacionDAOimpl implements HabitacionDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addHabitacion(Habitacion habitacion) {
		sessionFactory.getCurrentSession().save(habitacion);
		
	}

	@SuppressWarnings("unchecked")	
	public List<TipoHabitacion> getTipos() {
		return (List<TipoHabitacion>) getCurrentSession().createSQLQuery("select * from TIPO_HABITACION").addEntity(TipoHabitacion.class).list();
	}

	@Override
	public void deleteHabitacion(int idDelete) {
		Habitacion hab = new Habitacion();
		hab.setId(idDelete);
		getCurrentSession().delete(hab);
		
	}

	@SuppressWarnings("unchecked")
	public List<Habitacion> listarHabitaciones() {
		return (List<Habitacion>) getCurrentSession().createQuery("from Habitacion").list();
	}

	@Override
	public Habitacion getHabitacion(int id) {
		Habitacion empresa = (Habitacion) getCurrentSession().createQuery("from Habitacion where id = :idHabitacion")
				.setParameter("idHabitacion", id)
				.uniqueResult();
		return empresa;
	}

	@Override
	public void updateHabitacion(Habitacion habitacion) {
		Habitacion hab = getHabitacion(habitacion.getId());
		hab.setNumero(habitacion.getNumero());
		hab.setEstado(habitacion.getEstado());
		hab.setIdTipoHabitacion(habitacion.getIdTipoHabitacion());
		getCurrentSession().update(hab);
		
	}

	@Override
	public List<Habitacion> getHabitaciones() {
		List<Habitacion> books = (List<Habitacion>) getCurrentSession().createQuery("from Habitacion").list();
		return books;
	}

	@Override
	public List<Habitacion> getHabitacionesHabilitadas() {
		List<Habitacion> books = (List<Habitacion>) getCurrentSession().createQuery("from Habitacion where estado = 1").list();
		return books;
	}
	
	@Override
	public int getCantidadHabitacionesHabilitadas() {
		java.math.BigDecimal count = (java.math.BigDecimal)getCurrentSession().createSQLQuery("Select count(*) from Habitacion where estado = 1").uniqueResult();
		return count != null ? count.intValue() : 0;
	}

	
}
	
	
