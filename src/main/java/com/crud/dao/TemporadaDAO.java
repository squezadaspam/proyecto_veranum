package com.crud.dao;

import java.util.List;

import com.crud.model.Temporada;


public interface TemporadaDAO {

	public void addTemporada(Temporada temporada);
	public void updateTemporada(Temporada temporada);
	public Temporada getTemporada(int id);
	public void deleteTemporada(int id);
	public List<Temporada> getTemporadas();
}
