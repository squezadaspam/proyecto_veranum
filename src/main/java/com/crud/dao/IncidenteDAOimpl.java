package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Cargo;
import com.crud.model.Incidente;

@Repository
public class IncidenteDAOimpl implements IncidenteDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addIncidente(Incidente incidente) {
		getCurrentSession().save(incidente);
		
	}

	@Override
	public boolean updateIncidente(Incidente incidente) {
		
		Incidente incidenteToUpdate = getIncidenteInt(incidente.getId());
		incidenteToUpdate.setId_hotel(incidente.getId_hotel());
		incidenteToUpdate.setDescripcion(incidente.getDescripcion());
		incidenteToUpdate.setFecha_registro(incidente.getFecha_registro());
		incidenteToUpdate.setFecha_suceso(incidente.getFecha_suceso());
		getCurrentSession().update(incidenteToUpdate);
		return true;
	}

	@Override
	public Incidente getIncidenteInt(int id) {
		
		Incidente incidente = (Incidente) getCurrentSession().createQuery("from Incidente where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		return incidente;
	}

	@SuppressWarnings("unchecked")
	public List<Incidente> getIncidentes() {
		
		return getCurrentSession().createQuery("from Incidente").list();
	}
	
}