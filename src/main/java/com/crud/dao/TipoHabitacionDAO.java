package com.crud.dao;

import java.util.List;

import com.crud.model.Habitacion;
import com.crud.model.Servicio;
import com.crud.model.TipoHabitacion;

public interface TipoHabitacionDAO {

	public TipoHabitacion getTipoHabitacion(int id);
	public List<TipoHabitacion> getTipoHabitaciones();
}
