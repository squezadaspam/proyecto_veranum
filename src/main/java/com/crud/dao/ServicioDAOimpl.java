package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.DetalleServicio;
import com.crud.model.Servicio;


@Repository
public class ServicioDAOimpl implements ServicioDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addServicio(Servicio servicio) {
		getCurrentSession().save(servicio);
		
	}

	@Override
	public boolean updateServicio(Servicio servicio) {

		try{
			
			Servicio servicioToUpdate = getServicio(servicio.getId());
			servicioToUpdate.setNombre(servicio.getNombre());
			servicioToUpdate.setEstado(servicio.getEstado());
			getCurrentSession().update(servicioToUpdate);

			return true;
			
		}catch(Exception e){
			
			return false;
		}
			
	}
		
	

	@Override
	public Servicio getServicio(int id) {
		Servicio empresa = (Servicio) getCurrentSession().createQuery("from Servicio where id = :idServicio")
				.setParameter("idServicio", id)
				.uniqueResult();
		return empresa;
	}

	@Override
	public void deleteServicio(int id) {
		Servicio servicio = getServicio(id);
		if(servicio != null) {
			getCurrentSession().delete(servicio);
		}
	}

	@Override
	public List<Servicio> getServicios() {
		return getCurrentSession().createQuery("from Servicio").list();
	}

	@Override
	public Servicio getServicioInt(int id) {
		Servicio servicio = (Servicio) getCurrentSession().createSQLQuery("select * from Servicio where id = :id").addEntity(Servicio.class)
				.setParameter("id", id)
				.uniqueResult();
		return servicio;
	}
	
	
	
}
