package com.crud.dao;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Reserva;

@Repository
public class ReservaDAOImpl implements ReservaDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public Reserva addReserva(Reserva reserva)  {
		reserva.setCodigo();
		getCurrentSession().save(reserva);
		return reserva;
	}

	@Override
	public void updateReserva(Reserva reserva) {
		getCurrentSession().update(reserva);
		
	}
	
	@Override
	public void saveOrUpdateReserva(Reserva reserva) {
		if(reserva.getId()==0){
			reserva.setCodigo();
		}
		getCurrentSession().saveOrUpdate(reserva);
		
	}

	@Override
	public Reserva getReserva(int id) {
		Reserva empresa = (Reserva) getCurrentSession().createQuery("from Reserva where id = :idReserva")
				.setParameter("idReserva", id)
				.uniqueResult();
		return empresa;
	}
	
	@Override
	public Reserva getReservaPorIdUsuarioYCodigo(int idUsuario, String codigo) {
		Reserva empresa = (Reserva) getCurrentSession().createQuery("from Reserva where id_usuario = :idUsuario and codigo = :codigo")
				.setParameter("idUsuario", idUsuario)
				.setParameter("codigo", codigo)
				.uniqueResult();
		return empresa;
	}

	@Override
	public void deleteReserva(int id) {
		Reserva reserva = getReserva(id);
		if(reserva != null) {
			getCurrentSession().delete(reserva);
		}
	}

	@Override
	public List<Reserva> getReservas() {
		List<Reserva> reservas = (List<Reserva>) getCurrentSession().createQuery("from Reserva").list();
		return reservas;
	}
	
	@Override
	public List<Reserva> getReservasPorIdUsuario(int idUsuario) {
		List<Reserva> reservas = (List<Reserva>) getCurrentSession().createQuery("from Reserva where id_usuario = :idUsuario and estado = 1").setParameter("idUsuario", idUsuario).list();
		return reservas;
	}

	@Override
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion) {
		List<Reserva> books = (List<Reserva>) getCurrentSession().createQuery("from Reserva where id_habitacion = ?").setParameter(0, idHabitacion).list();
		return books;
	}
	
	@Override
	public int getDuracionEnDias(int idReserva) {
		int duracion = (int) getCurrentSession().createQuery("Select fecha_inicio - fecha_fin 'Days' from Reserva where id = :idReserva").setParameter("idReserva", idReserva).uniqueResult();
		return duracion;
	}
	
	@Override
	public void desactivarReservasPorIdHabitacion(int idHabitacion) {
		
		getCurrentSession().createSQLQuery("update reserva set estado = 0 where id_habitacion = :idHabitacion")
		.setParameter("idHabitacion", idHabitacion)
		.executeUpdate();
		
	}

}
