package com.crud.dao;

import java.util.List;

import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empleado;
import com.crud.model.Empresa;
import com.crud.model.Hotel;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.model.Usuario;


public interface UsuarioDAO {


	public void addUsuarioPersona( Persona persona);
	public void addUsuarioEmpresa( Empresa empresa);
	public boolean updateUsuario(Usuario usuario, Contacto contacto, Pais pais, Ciudad ciudad, Direccion direccion, Persona persona);
	public Usuario getUsuario(String usuario);
	public Contacto getContacto(String usuario);
	public boolean cambiarClave(Usuario user);
	public boolean updateTelefono(Telefono telefono, String nom_user);
	public List<Telefono> getTelefono(String usuario);
	public Direccion getDireccion(String usuario);
	public Ciudad getCiudad(String usuario);
	public Persona getPersona(String usuario);
	public Pais getPais(String usuario);
	public Empresa getEmpresa(String usuario);
	public Usuario login(String usuario,String pass);
	boolean NombreUsuario(String usuario);
	public boolean CambiarEstadoUsuario(String usuario, String pass);
	public void addTelefono(Telefono telefono);
	public void addDireccion(Direccion direccion);
	public void addContacto(Contacto contacto);
	public void addUsuario(Usuario usuario);
	public void addCiudad(Ciudad ciudad);
	public List<Pais> getListadoPais();
	public List<Usuario> getListadoUsuarios();
	public List getInformacionDePersonaPorIdUsuario();
	public List getInformacionDeEmpresaPorIdUsuario();
	public Usuario getUsuario(int idUsuario);
	public Persona getRunPersona(String run);
	public Contacto getRunContacto(String run);
	public Direccion getRunDireccion(String run);
	public Telefono getRunTelefono(String run);
	public void addEmpleado(Empleado empleado);
	public List<Persona> getListadoPersonas();
	public List<Empleado> getListadoEmpleados();
	public Empleado getEmpleadoUsuario(int id);
	public boolean updateEmpleadoUsuario(Empleado empleado);
	public Persona getPersona(int id);
	public List<Hotel> getListadoHoteles();
	public boolean getHotelPorNombre(String nombre);
	public void addHotelUsuario(Hotel hotel);
	public Hotel getHotelUsuario(int id);
	public boolean updateHotelUsuario(Hotel hotel);
	public Empleado getEmpleadoIdPersona(int id_persona);
	public boolean empleadoExiste(int id_persona);
	public boolean personaExiste(String run);
	public Contacto getContacto(int id);

}
