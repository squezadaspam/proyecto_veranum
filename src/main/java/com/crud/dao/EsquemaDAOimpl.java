package com.crud.dao;

import java.sql.CallableStatement;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Reserva;
import com.crud.model.Esquema;

@Repository
public class EsquemaDAOimpl implements EsquemaDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addEsquema(Esquema esquema) {
		getCurrentSession().save(esquema);
		
	}

	@Override
	public void updateEsquema(Esquema esquema) {
		getCurrentSession().update(esquema);
		
	}

	@Override
	public Esquema getEsquema(int id) {
		
		Esquema empresa = (Esquema) getCurrentSession().createQuery("from esquema where id = :idesquema")
				.setParameter("idesquema", id)
				.uniqueResult();
		return empresa;
	}

	@Override
	public void deleteEsquema(int id) {
		
		Esquema esquema = getEsquema(id);
		
		if(esquema != null) {
			getCurrentSession().delete(esquema);
		}
	}

	@Override
	public List<Esquema> getEsquemas() {
		
		List<Esquema> list = (List<Esquema>) getCurrentSession().createSQLQuery("SELECT * FROM ESQUEMA").addEntity(Esquema.class).list();
		return list;
	}

	@Override
	public int getCantidadDias() {
		
		java.math.BigDecimal  count = (java.math.BigDecimal)getCurrentSession().createSQLQuery("select (max(fecha) - min(fecha)) from esquema").uniqueResult();
		return count != null ? count.intValue() : 0;
	}
	
	@Override
	public Integer getEsquemaPorHabitacionIdYPorFechaCantidad(int idHabitacion, LocalDate fecha1, LocalDate fecha2) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		int cantidad = ((Number)getCurrentSession().createSQLQuery("select count(*) from esquema where id_habitacion = :idHabitacion and estado = 1 and reservada = 1 and fecha BETWEEN to_date(:fecha1,'dd/MM/yyyy') and to_date(:fecha2,'dd/MM/yyyy') ")
				//.addEntity(Esquema.class)
				.setParameter("idHabitacion", idHabitacion)
				.setParameter("fecha1", fecha1.format(formatter))
				.setParameter("fecha2", fecha2.format(formatter))
				.uniqueResult()).intValue();
		
		return cantidad;
	}
	
	@Override
	public Esquema getEsquemaPorHabitacionIdYPorFecha(int idHabitacion, LocalDate fecha) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		Esquema esquema = (Esquema)getCurrentSession().createSQLQuery("select * from esquema where id_habitacion = :idHabitacion and estado = 1 and fecha = to_date(:fecha,'dd/MM/yyyy') ")
				.addEntity(Esquema.class)
				.setParameter("idHabitacion", idHabitacion)
				.setParameter("fecha", fecha.format(formatter))
				.uniqueResult();
		
		return esquema;
	}
	
	@Override
	public LocalDate getFechaMin() {
		
		java.sql.Timestamp fechaSql = (java.sql.Timestamp)getCurrentSession().createSQLQuery("select min(fecha) from esquema").uniqueResult();
		
		return fechaSql.toLocalDateTime().toLocalDate();
	}
	
	@Override
	public LocalDate getFechaMax() {
		
		java.sql.Timestamp fechaSql = (java.sql.Timestamp)getCurrentSession().createSQLQuery("select Max(fecha) from esquema").uniqueResult();
		
		return fechaSql.toLocalDateTime().toLocalDate();
	}
	
	@Override
	public boolean isSpaceForAReserveByHabitacion(LocalDate fechaDesde, LocalDate fechaHasta, int idHabitacion) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		java.math.BigDecimal idHabitacionResult = (java.math.BigDecimal)getCurrentSession().createSQLQuery("Select id_habitacion from esquema where reservada = 0 and estado = 1 and fecha between to_date(:fechaDesde,'dd/MM/yyyy') and to_date(:fechaHasta,'dd/MM/yyyy') and id_habitacion = :idHabitacion group by id_habitacion having count(*) = to_date(:fechaHasta,'dd/MM/yyyy') - to_date(:fechaDesde,'dd/MM/yyyy') + 1")
				.setParameter("fechaHasta", fechaHasta.format(formatter))
				.setParameter("fechaDesde", fechaDesde.format(formatter))
				.setParameter("idHabitacion", idHabitacion)
				.uniqueResult();
		
		return idHabitacionResult != null ? true : false;
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdDeReserva(int idReserva){
		
		List<Esquema> lista = (List<Esquema>) getCurrentSession().createQuery("from esquema where id_reserva = :idReserva and estado = 1")
				.setParameter("idReserva", idReserva)
				.list();
		
		return lista;
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdHabitacion(int idHabitacion, String fechaActual){
		
		List<Esquema> esquema = (List<Esquema>)getCurrentSession().createSQLQuery("select * from esquema where id_habitacion = :idHabitacion and estado = 1 and fecha >= to_date(:fechaActual,'yyyy-MM-dd')")
				.addEntity(Esquema.class)
				.setParameter("idHabitacion", idHabitacion)
				.setParameter("fechaActual", fechaActual)
				.list();
		
		return esquema;
	}
	
	@Override
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion){
		
		List<Reserva> reservas = (List<Reserva>)getCurrentSession().createSQLQuery("select distinct r.* from reserva r, esquema e where r.id = e.id_reserva and e.id_habitacion = :idHabitacion and r.estado = 1 and r.ocupada = 0 and e.estado = 1")
				.addEntity(Reserva.class)
				.setParameter("idHabitacion", idHabitacion)
				.list();
		
		return reservas;
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdHabitacionEntreFecha(int idHabitacion, LocalDate fechaInicio, LocalDate fechaFin){
		
		List<Esquema> esquema = (List<Esquema>)getCurrentSession().createSQLQuery("select * from esquema where id_habitacion = :idHabitacion and estado = 1 and fecha Between to_date(:fechaInicio,'yyyy-MM-dd') and to_date(:fechaFin,'yyyy-MM-dd')")
				.addEntity(Esquema.class)
				.setParameter("idHabitacion", idHabitacion)
				.setParameter("fechaInicio", fechaInicio.toString())
				.setParameter("fechaFin", fechaFin.toString())
				.list();
		
		return esquema;
	}
	
	@Override
	public void desactivarEsquemasPorIdHabitacion(int idHabitacion) {
		
		getCurrentSession().createSQLQuery("update esquema set id_reserva = 0, reservada = 0 where id_habitacion = :idHabitacion")
		.setParameter("idHabitacion", idHabitacion)
		.executeUpdate();
	}
	
	@Override
	public void guardaReservaEnEsquema(int idReserva, int idHabitacion, LocalDate fechaDesde,LocalDate fechaHasta){
		
		getCurrentSession().createSQLQuery("update esquema set id_reserva = :idReserva, estado = 1, reservada = 1 where id_habitacion = :idHabitacion and fecha between to_date(:fechaDesde,'yyyy-MM-dd') and to_date(:fechaHasta,'yyyy-MM-dd')")
		.setParameter("idReserva", idReserva)
		.setParameter("idHabitacion", idHabitacion)
		.setParameter("fechaDesde", fechaDesde.toString())
		.setParameter("fechaHasta", fechaHasta.toString())
		.executeUpdate();
	}
	
	@Override
	public void ordenaTodo(){
			
		getCurrentSession().createSQLQuery("call ordenar()").executeUpdate();
	}
	
	@Override
	public void ordenaTodoPorTipoHabitacion(int idTipoHabitacion){
			
		getCurrentSession().createSQLQuery("call ordenarportipo(:idTipoHabitacion)").setParameter("idTipoHabitacion", idTipoHabitacion).executeUpdate();
	}
	
	@Override
	public Integer calcularPrecioReserva(int idReserva) {
		int precio = ((Number)getCurrentSession().createSQLQuery("select (r.fecha_fin - r.fecha_inicio) * t.precio_base precio_reserva from reserva r, habitacion h, tipo_habitacion t where r.id_habitacion = h.id and h.id_tipo_habitacion = t.id and r.id = :idReserva")
				//.addEntity(Esquema.class)
				.setParameter("idReserva", idReserva)
				.uniqueResult()).intValue();
		return precio;
	}
	
	@Override
	public List<Reserva> getReservasEnCurso(){
		
		List<Reserva> reservas = (List<Reserva>)getCurrentSession().createSQLQuery("Select r.* from esquema e, reserva r where e.id_reserva = r.id and fecha like(sysdate) and reservada = 1")
				.addEntity(Reserva.class)
				.list();
		
		return reservas;
	}
	

}
