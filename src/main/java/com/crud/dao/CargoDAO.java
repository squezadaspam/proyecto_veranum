package com.crud.dao;

import java.util.List;

import com.crud.model.Cargo;

public interface CargoDAO {

	public void addCargo(Cargo cargo);
	public boolean updateCargo(Cargo cargo);
	public Cargo getCargo(String nombreCargo);
	public Cargo getCargoInt(int id);
	public boolean cambiarEstadoCargo(String nombreCargo);
	public List<Cargo> getCargos();
	public Cargo getCargoPorId(int id);
	public List<Cargo> getCargosActivos();
	public boolean getCargoPorNombre(String nombre);
}
