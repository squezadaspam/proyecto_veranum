package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.DetalleServicio;
import com.crud.model.HorarioServicio;


@Repository
public class HorarioServicioDAOimpl implements HorarioServicioDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addHorarioServicio(HorarioServicio horarioServicio) {
		getCurrentSession().save(horarioServicio);
	}

	@Override
	public void updateHorarioServicio(HorarioServicio horarioServicio) {
		getCurrentSession().update(horarioServicio);
		
	}

	@Override
	public HorarioServicio getHorarioServicio(int id) {
		HorarioServicio horario = (HorarioServicio) getCurrentSession().createSQLQuery("select * from Horario_servicio where id = :idHorario_servicio").addEntity(HorarioServicio.class)
				.setParameter("idHorario_servicio", id)
				.uniqueResult();
		return horario;
	}

	@Override
	public void deleteHorarioServicio(int id) {
		HorarioServicio horarioServicio = getHorarioServicio(id);
		if(horarioServicio != null) {
			getCurrentSession().delete(horarioServicio);
		}
	}

	@SuppressWarnings("unchecked")
	public List<HorarioServicio> getHorarioServicio() {
		return getCurrentSession().createQuery("from HorarioServicio").list();
	}

	@Override
	public HorarioServicio getHorarioServicioInt(int id) {
		HorarioServicio horarioServicio = (HorarioServicio) getCurrentSession().createSQLQuery("select * from Horario_servicio where id = :id").addEntity(HorarioServicio.class)
				.setParameter("id", id)
				.uniqueResult();
		return horarioServicio;
	}


	
	
	
}
