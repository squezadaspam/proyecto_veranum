package com.crud.dao;

import java.util.List;

import com.crud.model.Consumo;
import com.crud.model.Esquema;
import com.crud.model.Pago;

public interface ConsumoDAO {

	public void addConsumo(Consumo consumo);
	public void addPago(Pago pago);
	public Consumo getConsumo(int id);
	public void deleteConsumo(int id);
	public List<Consumo> getConsumo();
	public Consumo getConsumoInt(int id);
	public int getTotalPorIdUsuario(int id);
	public void updateConsumo(Consumo consumo);
	public List<Consumo> getConsumoPorEstado(int idConsumo);
}
