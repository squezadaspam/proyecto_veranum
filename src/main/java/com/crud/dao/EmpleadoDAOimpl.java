package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.model.Cargo;
import com.crud.model.Empleado;
import com.crud.model.Persona;
import com.crud.model.Usuario;

@Repository
public class EmpleadoDAOimpl implements EmpleadoDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addEmpleado(Empleado empleado) {
		
		getCurrentSession().save(empleado);
		
	}

	@Override
	public boolean updateEmpleado(Empleado empleado) {
		

		try{
			
			Empleado empleadoToUpdate = getEmpleado(empleado.getId());
			empleadoToUpdate.setEstado(empleado.getEstado());
			empleadoToUpdate.setId_cargo(empleado.getId_cargo());
			empleadoToUpdate.setId_hotel(empleado.getId_hotel());	
			empleadoToUpdate.setId_persona(empleado.getId_persona());
			getCurrentSession().update(empleadoToUpdate);
			
			return true;
			
		}catch(Exception e){
			
			return false;
		}
	}

	@Override
	public Empleado getEmpleado(int id) {

		Empleado emp = (Empleado) getCurrentSession().createQuery("from Empleado where id = :i")
				.setParameter("i", id)
				.uniqueResult();
		return emp;
	}

	@Override
	public boolean cambiarEstadoEmpleado(int id) {

		try{
			Empleado empleadoToUpdate = getEmpleado(id);
			empleadoToUpdate.setEstado(0);
			getCurrentSession().update(empleadoToUpdate);
			
			return true;
			
		}catch(Exception e){
			
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Empleado> getEmpleados() {
		
		return getCurrentSession().createQuery("from Empleado").list();
	}

	
	
	
}
