package com.crud.dao;

import java.util.List;

import com.crud.model.Reserva;

public interface ReservaDAO {
	public Reserva addReserva(Reserva reserva) ;
	public void updateReserva(Reserva reserva);
	public Reserva getReserva(int id);
	public void deleteReserva(int id);
	public List<Reserva> getReservas();
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion);
	public int getDuracionEnDias(int idReserva);
	public List<Reserva> getReservasPorIdUsuario(int idUsuario);
	public Reserva getReservaPorIdUsuarioYCodigo(int idUsuario, String codigo);
	public void desactivarReservasPorIdHabitacion(int idHabitacion);
	public void saveOrUpdateReserva(Reserva reserva);
}
