package com.crud.dao;


import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empleado;
import com.crud.model.Empresa;
import com.crud.model.Hotel;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.model.Usuario;


@Repository
public class UsuarioDAOimpl implements UsuarioDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/******************* N U E V O ****************/
	public void addUsuarioPersona(Persona persona){
	    
			getCurrentSession().save(persona);
		
	}
	
	public void addUsuarioEmpresa(Empresa empresa){
	   
			getCurrentSession().save(empresa);
	}
	
	public void addTelefono(Telefono telefono){
		getCurrentSession().save(telefono);
	}
	
	public void addCiudad(Ciudad ciudad){
		getCurrentSession().save(ciudad);
	}
	
	public void addUsuario(Usuario usuario){
		getCurrentSession().save(usuario);
	}
	
	public void addContacto(Contacto contacto){
		getCurrentSession().save(contacto);
	}
	
	public void addDireccion(Direccion direccion){
		getCurrentSession().save(direccion);
	}
	
	public boolean updateTelefono(Telefono telefono, String nom_user){
		//update de telefono
		try{
			List<Telefono> telefonoToUpdate = getTelefono(nom_user);
			
			for (int i = 0; i < telefonoToUpdate.size(); i++) {
				
		        if(telefonoToUpdate.get(i).getNombre().equals(telefono.getNombre())){
		        	
		        	telefonoToUpdate.get(i).setNumero(telefono.getNumero());
		        	telefonoToUpdate.get(i).setEstado(telefono.getEstado());
		        	getCurrentSession().update(telefonoToUpdate.get(i));
		        	
		        	return true;
				}
			}
			
			return false;
			
		}catch(Exception e){
			
			return false;
		}
	}
	
	
	public boolean updateUsuario(Usuario usuario, Contacto contacto, Pais pais, Ciudad ciudad, Direccion direccion, Persona persona){
		try{
			//update de Direccion y Ciudad
			Ciudad ciudadToUpdate = getCiudad(usuario.getNom_user());
			ciudadToUpdate.setNombre(ciudad.getNombre());
			ciudadToUpdate.setId_pais(ciudad.getId_pais());
			getCurrentSession().update(ciudadToUpdate);
			
			Direccion direccionToUpdate = getDireccion(usuario.getNom_user());
			direccionToUpdate.setCalle(direccion.getCalle());
			direccionToUpdate.setCod_postal(direccion.getCod_postal());
			direccionToUpdate.setNum_depto(direccion.getNum_depto());
			direccionToUpdate.setNumero(direccion.getNumero());
			direccionToUpdate.setId_ciudad(ciudadToUpdate.getId());
			getCurrentSession().update(direccionToUpdate);
			
			Contacto contactoToUpdate = getContacto(usuario.getNom_user());
			contactoToUpdate.setEmail(contacto.getEmail());
			contactoToUpdate.setNombre(contacto.getNombre());
			
			if(persona != null){
				
				Persona personaToUpdate = getPersona(usuario.getNom_user());
				personaToUpdate.setNombre(persona.getNombre());
				personaToUpdate.setApellido(persona.getApellido());
				getCurrentSession().update(personaToUpdate);
				contactoToUpdate.setNombre(persona.getNombre());
			}
			
			getCurrentSession().update(contactoToUpdate);
			
			return true;
		}catch(Exception e){
			
			return false;
		}
		
	}
	/*********** L I S T A D O S ******************/
	
	
	@SuppressWarnings("unchecked")
	public List<Pais> getListadoPais() {
		return getCurrentSession().createQuery("from Pais").list();
	}
	
	/*********** G E T   D E   E N T I D A D E S **********/
	
	
	public Usuario getUsuario(String usuario){
		Usuario user =  (Usuario) getCurrentSession().createQuery("from Usuario where nom_user = :name")
				.setParameter("name", usuario)
				.uniqueResult();
		return user;
		
	}
	public Contacto getContacto(String usuario){
		Usuario user = getUsuario(usuario);
		Contacto contacto = (Contacto) getCurrentSession().createQuery("from Contacto where id_usuario = :idUser")
				.setParameter("idUser", user.getId())
				.uniqueResult();
		return contacto;
	}
	
	@SuppressWarnings("unchecked")
	public List<Telefono> getTelefono(String usuario){
		Contacto contacto = getContacto(usuario);
		return getCurrentSession().createQuery("from Telefono where id_contacto = :idContacto and estado = 1")
				.setParameter("idContacto", contacto.getId())
				.list();
	}
	
	public Persona getPersona(String usuario){
		Contacto contacto = getContacto(usuario);
		Persona persona = (Persona) getCurrentSession().createQuery("from Persona where id_contacto = :idPersona")
				.setParameter("idPersona", contacto.getId())
				.uniqueResult();
		return persona;		
	}
	
	public Direccion getDireccion(String usuario){
		Contacto contacto = getContacto(usuario);
		Direccion direccion = (Direccion) getCurrentSession().createQuery("from Direccion where id = :idContacto")
				.setParameter("idContacto", contacto.getId_direccion())
				.uniqueResult();
		return direccion;		
	}
	
	public Ciudad getCiudad(String usuario){
		Direccion direccion = getDireccion(usuario);
		Ciudad ciudad = (Ciudad) getCurrentSession().createQuery("from Ciudad where id = :idCiudad")
				.setParameter("idCiudad", direccion.getId_ciudad())
				.uniqueResult();
		return ciudad;		
	}
	
	public Pais getPais(String usuario){
		Ciudad ciudad = getCiudad(usuario);
		Pais pais = (Pais) getCurrentSession().createQuery("from Pais where id = :idPais")
				.setParameter("idPais", ciudad.getId_pais())
				.uniqueResult();
		return pais;		
	}
	
	public Empresa getEmpresa(String usuario){
		Contacto contacto= getContacto(usuario);
		Empresa empresa = (Empresa) getCurrentSession().createQuery("from Empresa where id_contacto = :idContacto")
				.setParameter("idContacto", contacto.getId())
				.uniqueResult();
		return empresa;
	}
	
	/******************* L O G I N ****************/
	
	
	public Usuario login(String usuario, String pass){
		
		Usuario user =  getUsuario(usuario);

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(user != null){
			
			if(user.getId_rol() != 1){
				//verifica contrase�a con hash (para usuarios NO admin) y el estado del usuario
				if(passwordEncoder.matches(pass, user.getPass()) && user.getEstado() == 1){
					
					return user;
				}
				
			}else{
				//verifica contrase�a sin hash (para usuarios ADMIN) y el estado del usuario
				if(user.getPass().equals(pass) && user.getEstado() == 1){
					return user;
				}
			}
		}
		
		return null;
	}
	
	/************ verificar existencia de nombre de usuario *************/
	
	public boolean NombreUsuario(String usuario){
			
		Usuario user =  getUsuario(usuario);
		
			if(user != null){
				
				return true;
			}
			
			return false;
		}
	
	/************ Da de baja a un usuario *************/
	public boolean CambiarEstadoUsuario(String usuario,String pass){
		
		Usuario user =  getUsuario(usuario);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(user.getNom_user().equals(usuario) && passwordEncoder.matches(pass, user.getPass())){
			Usuario usuarioToUpdate = getUsuario(usuario);
			usuarioToUpdate.setEstado(0);
			getCurrentSession().update(usuarioToUpdate);
			
			return true;
			
		}else return false;
		
	}
	
	@Override
	public List<Usuario> getListadoUsuarios() {
		return (List<Usuario>)getCurrentSession().createQuery("from Usuario").list();
	}
	
	// El orden de los .addScalar() es importante, ya que corresponde al orden de las posiciones en el Object[]
	// se deben agregar .addScalar() debajo del �ltimo.
	@Override
	public List getInformacionDePersonaPorIdUsuario() {
		
		return 	(List) getCurrentSession().createSQLQuery("select u.id as id, p.nombre as nombre,p.apellido as apellido,c.email as email,d.calle as calle, d.cod_postal as cod_postal, NVL(d.num_depto, 0) as num_depto, d.numero as numero, p.id_contacto as id_contacto, t.nombre as nombre_telefono, t.numero as numero_telefono from usuario u, contacto c, persona p, direccion d, telefono t	where c.id_usuario = u.id and p.id_contacto = c.id and c.id_direccion = d.id and t.id_contacto = p.id_contacto")
				.addScalar("id", new StringType()) // posicion 0
				.addScalar("nombre", new StringType()) // posicion 1
				.addScalar("apellido", new StringType()) // posicion 2
				.addScalar("email", new StringType()) // posicion 3
				.addScalar("calle", new StringType()) // posicion 4
				.addScalar("cod_postal", new StringType()) // posicion 5
				.addScalar("num_depto", new StringType()) // posicion 6
				.addScalar("numero", new StringType()) // posicion 7
				.addScalar("id_contacto", new StringType()) // posicion 8
				.addScalar("nombre_telefono", new StringType()) // posicion 9
				.addScalar("numero_telefono", new StringType()) // posicion 10
				.list();
	}
	
	@Override
	public List getInformacionDeEmpresaPorIdUsuario() {
		return 	(List) getCurrentSession().createSQLQuery("select u.id as id, c.nombre as nombre, c.email as email,d.calle as calle, d.cod_postal as cod_postal, NVL(d.num_depto, 0) as num_depto, d.numero as numero, e.id_contacto as id_contacto, t.nombre as nombre_telefono, t.numero as numero_telefono, e.nombre as nombreEmpresa from usuario u, contacto c, empresa e, direccion d, telefono t	where c.id_usuario = u.id and e.id_contacto = c.id and c.id_direccion = d.id and t.id_contacto = e.id_contacto")
				.addScalar("id", new StringType()) // posicion 0
				.addScalar("nombre", new StringType()) // posicion 1
				.addScalar("email", new StringType()) // posicion 2
				.addScalar("calle", new StringType()) // posicion 3
				.addScalar("cod_postal", new StringType()) // posicion 4
				.addScalar("num_depto", new StringType()) // posicion 5
				.addScalar("numero", new StringType()) // posicion 6
				.addScalar("id_contacto", new StringType()) // posicion 7
				.addScalar("nombre_telefono", new StringType()) // posicion 8
				.addScalar("numero_telefono", new StringType()) // posicion 9
				.addScalar("nombreEmpresa", new StringType()) // posicion 10
				.list();
	}
	
	@Override
	public boolean cambiarClave(Usuario user){
		
		try{
			Usuario usuarioToUpdate = getUsuario(user.getNom_user());
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			usuarioToUpdate.setPass(passwordEncoder.encode(user.getPass()));
			getCurrentSession().update(usuarioToUpdate);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	@Override
	public Usuario getUsuario(int idUsuario){
		Usuario user =  (Usuario) getCurrentSession().createQuery("from Usuario where id = :idUsuario")
				.setParameter("idUsuario", idUsuario)
				.uniqueResult();
		return user;
	}
	
	@Override
	public Persona getRunPersona(String run) {
		
		Persona per = (Persona) getCurrentSession().createQuery("from Persona where run = :run")
				.setParameter("run", run)
				.uniqueResult();
		return per;
	}
	
	
	@Override
	public Contacto getRunContacto(String run) {
		
		Contacto con = (Contacto) getCurrentSession().createSQLQuery("select c.id, c.nombre, c.email, c.id_direccion, c.id_usuario from persona p, contacto c where p.id_contacto = c.id and p.run = :run")
				.addEntity(Contacto.class)
				.setParameter("run", run)
				.uniqueResult();
		return con;
	}

	@Override
	public Direccion getRunDireccion(String run) {
		
		Direccion dir = (Direccion) getCurrentSession().createSQLQuery("select d.id, d.calle, d.cod_postal, d.numero, d.num_depto, d.id_ciudad from persona p, contacto c, direccion d where p.id_contacto = c.id and c.id_direccion = d.id and p.run = :run")
				.addEntity(Direccion.class)
				.setParameter("run", run)
				.uniqueResult();
		return dir;
	}

	@Override
	public Telefono getRunTelefono(String run) {

		Telefono tel = (Telefono) getCurrentSession().createSQLQuery("select t.id, t.nombre, t.numero, t.estado, t.id_contacto from persona p, contacto c, telefono t where p.id_contacto = c.id and c.id = t.id_contacto and p.run = :run")
				.addEntity(Telefono.class)
				.setParameter("run", run)
				.uniqueResult();
		return tel;
	}

	@Override
	public void addEmpleado(Empleado empleado) {

		getCurrentSession().save(empleado);
		
	}
	

	@SuppressWarnings("unchecked")
	public List<Persona> getListadoPersonas() {
		
		return getCurrentSession().createQuery("from Persona").list();
	}

	@SuppressWarnings("unchecked")
	public List<Empleado> getListadoEmpleados() {
		 
		return getCurrentSession().createQuery("from Empleado").list();
	}

	@Override
	public Empleado getEmpleadoUsuario(int id) {
		 
		Empleado emp =  (Empleado) getCurrentSession().createQuery("from Empleado where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		return emp;
	}

	@Override
	public boolean updateEmpleadoUsuario(Empleado empleado) {
		
			try{
				Empleado empleadoToUpdate = getEmpleadoUsuario(empleado.getId());
				empleadoToUpdate.setId_cargo(empleado.getId_cargo());
				empleadoToUpdate.setId_hotel(empleado.getId_hotel());
				empleadoToUpdate.setId_persona(empleado.getId_persona());
				empleadoToUpdate.setEstado(empleado.getEstado());
				getCurrentSession().update(empleadoToUpdate);
				
				return true;
				
			}catch(Exception e){
				
				return false;
			}
		}

	@Override
	public Persona getPersona(int id) {
		
		Persona persona =  (Persona) getCurrentSession().createQuery("from Persona where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		return persona;
	}

	@SuppressWarnings("unchecked")
	public List<Hotel> getListadoHoteles() {

		return getCurrentSession().createQuery("from Hotel").list();
	}

	@Override
	public boolean getHotelPorNombre(String nombre) {

		Hotel hotel = (Hotel) getCurrentSession().createQuery("from Hotel where nombre = :nombre")
				.setParameter("nombre", nombre)
				.uniqueResult();
		if(hotel != null){
			return true;
		}	
		return false;
	}

	@Override
	public void addHotelUsuario(Hotel hotel) {
		
		getCurrentSession().save(hotel);
		
	}

	@Override
	public Hotel getHotelUsuario(int id) {

		Hotel hotel =  (Hotel) getCurrentSession().createQuery("from Hotel where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		return hotel;
	}

	@Override
	public boolean updateHotelUsuario(Hotel hotel) {

		try{
			Hotel hotelToUpdate = getHotelUsuario(hotel.getId());
			hotelToUpdate.setNombre(hotel.getNombre());
			hotelToUpdate.setCategoria(hotel.getCategoria());
			getCurrentSession().update(hotelToUpdate);
			
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Empleado getEmpleadoIdPersona(int id_persona) {

		Empleado emp =  (Empleado) getCurrentSession().createQuery("from Empleado where id_persona = :id_persona")
				.setParameter("id_persona", id_persona)
				.uniqueResult();
		return emp;
	}

	@Override
	public boolean empleadoExiste(int id_persona) {
		

			Empleado emp =  (Empleado) getCurrentSession().createQuery("from Empleado where id_persona = :id_persona")
					.setParameter("id_persona", id_persona)
					.uniqueResult();
			if(emp != null)
			return true;
			else
			return false;
	
	}

	@Override
	public boolean personaExiste(String run) {

		Persona per =  (Persona) getCurrentSession().createQuery("from Persona where run = :run")
				.setParameter("run", run)
				.uniqueResult();
		if(per != null)
		return true;
		else
		return false;
	}
	
	@Override
	public Contacto getContacto(int id){
		Contacto contacto = (Contacto) getCurrentSession().createQuery("from Contacto where id = :id")
				.setParameter("id", id)
				.uniqueResult();
		return contacto;
	}

}