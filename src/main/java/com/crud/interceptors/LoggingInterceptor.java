package com.crud.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.crud.model.Usuario;

@SessionAttributes("NomUser")
public class LoggingInterceptor implements HandlerInterceptor  {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		boolean estado = true;
		if( // p�ginas que no afectar� este preHandle
			!request.getRequestURI().equals("/veranum/index") &&
			!request.getRequestURI().equals("/veranum/") &&
		    !request.getRequestURI().equals("/veranum/ingreso") &&
		    !request.getRequestURI().equals("/veranum/salir") &&
		    !request.getRequestURI().equals("/veranum/persona/registro") &&
		    !request.getRequestURI().equals("/veranum/empresa/registro") &&
		    !request.getRequestURI().equals("/veranum/valida-tipo-habitacion")
		){
			
			
			try{
				Usuario user = (Usuario) request.getSession().getAttribute("NomUser");
				if(user.getNom_user().equals("Invitado")){
					response.sendRedirect("/veranum/ingreso");
					estado = false;
				}else{
					String url = request.getRequestURI();
					String[] verificaAdmin = url.split("admin");
					if(verificaAdmin.length > 1){
							if(user.getId_rol()!= 1 && user.getId_rol()!= 4 ){
							System.out.println("dentor del if id rol user"+user.getId_rol());
							response.sendRedirect("/veranum/index");
							estado = false;
						}
					}
					estado = true;
				}
				String url = request.getRequestURI();
				String[] urlCargo = url.split("cargo");
				String[] urlHotel = url.split("hotel");
				String[] urlEmpleado = url.split("empleado");
				if(user.getId_rol() == 4){
					if(urlCargo.length > 1 || urlHotel.length > 1 || urlEmpleado.length > 1){
						response.sendRedirect("/veranum/admin");
						estado = false;
					}
				}
				
				
			}catch(Exception ex){
				response.sendRedirect("/veranum/index");
				estado = false;
			}
			
		   
		}
		return estado;
	}
	
	@Override
	public void postHandle(	HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
			Object handler, Exception ex) throws Exception {
	}	
	
} 
