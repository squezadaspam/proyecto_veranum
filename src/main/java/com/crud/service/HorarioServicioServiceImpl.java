package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.HorarioServicioDAO;
import com.crud.model.HorarioServicio;

@Service
@Transactional
public class HorarioServicioServiceImpl implements HorarioServicioService{

	@Autowired
	private HorarioServicioDAO horarioServicioDAO;
	
	public void addHorarioServicio(HorarioServicio horarioServicio) {
		horarioServicioDAO.addHorarioServicio(horarioServicio);
	}

	public void updateHorarioServicio(HorarioServicio horarioServicio) {
		horarioServicioDAO.updateHorarioServicio(horarioServicio);
	}

	public HorarioServicio getHorarioServicio(int id) {
		return horarioServicioDAO.getHorarioServicio(id);
	}

	public void deleteHorarioServicio(int id) {
		horarioServicioDAO.deleteHorarioServicio(id);
	}

	public List<HorarioServicio> getHorarioServicio() {
		return horarioServicioDAO.getHorarioServicio();
	}

	@Override
	public HorarioServicio getHorarioServicioInt(int id) {
		return horarioServicioDAO.getHorarioServicioInt(id);
	}



}
