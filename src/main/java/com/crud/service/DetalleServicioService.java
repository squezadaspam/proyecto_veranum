package com.crud.service;

import java.util.List;

import com.crud.model.DetalleServicio;


public interface DetalleServicioService {


	public void addDetalleServicio (DetalleServicio detalleServicio);
	public void updateDetalleServicio(DetalleServicio detalleServicio);
	public DetalleServicio getDetalleServicio(int id);
	public List<DetalleServicio> getDetalleServicio();
	boolean verificarDetalleServicio(int id);
	public DetalleServicio getDetalleServicioInt(int id);
	
	
	
}