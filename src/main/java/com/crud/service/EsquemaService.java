package com.crud.service;

import java.time.LocalDate;
import java.util.List;

import com.crud.model.Esquema;
import com.crud.model.Reserva;

public interface EsquemaService {
	
	public void addEsquema(Esquema Esquema);
	public void updateEsquema(Esquema Esquema);
	public Esquema getEsquema(int id);
	public void deleteEsquema(int id);
	public List<Esquema> getEsquemas();
	public int getCantidadDias();
	public Esquema getEsquemaPorHabitacionIdYPorFecha(int idHabitacion, LocalDate fecha);
	public Integer getEsquemaPorHabitacionIdYPorFechaCantidad(int idHabitacion, LocalDate fecha1, LocalDate fecha2);
	public LocalDate getFechaMin();
	public LocalDate getFechaMax();
	public boolean isSpaceForAReserveByHabitacion(LocalDate fechaDesde, LocalDate fechaHasta, int idHabitacion);
	public List<Esquema> getEsquemasPorIdDeReserva(int idReserva);
	public List<Esquema> getEsquemasPorIdHabitacion(int idHabitacion);
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion);
	public List<Esquema> getEsquemasPorIdHabitacionEntreFecha(int idHabitacion, LocalDate fechaInicio, LocalDate fechaFin);
	public void desactivarEsquemasPorIdHabitacion(int idHabitacion);
	public void guardaReservaEnEsquema(int idReserva, int idHabitacion, LocalDate fechaDesde,LocalDate fechaHasta);
	public void ordenaTodo();
	public Integer calcularPrecioReserva(int idReserva);
	public void ordenaTodoPorTipoHabitacion(int idTipoHabitacion);
	public List<Reserva> getReservasEnCurso();
}