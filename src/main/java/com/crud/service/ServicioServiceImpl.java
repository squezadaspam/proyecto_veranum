package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.ServicioDAO;
import com.crud.model.Servicio;

@Service
@Transactional
public class ServicioServiceImpl implements ServicioService{

	@Autowired
	private ServicioDAO servicioDAO;
	
	public void addServicio(Servicio servicio) {
		servicioDAO.addServicio(servicio);
	}

	public boolean updateServicio(Servicio servicio) {
		servicioDAO.updateServicio(servicio);
		return servicioDAO.updateServicio(servicio);
		
	}

	public Servicio getServicio(int id) {
		return servicioDAO.getServicio(id);
	}

	public void deleteServicio(int id) {
		servicioDAO.deleteServicio(id);
	}

	public List<Servicio> getServicio() {
		return servicioDAO.getServicios();
	}

	@Override
	public Servicio getServicioInt(int id) {
		
		return servicioDAO.getServicioInt(id);
	}

	


}
