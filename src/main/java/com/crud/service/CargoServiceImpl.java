package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.CargoDAO;
import com.crud.model.Cargo;

@Service
@Transactional
public class CargoServiceImpl implements CargoService{

	@Autowired
	private CargoDAO cargoDAO;
	
	@Override
	public void addCargo(Cargo cargo) {
		cargoDAO.addCargo(cargo);
		
	}

	@Override
	public boolean updateCargo(Cargo cargo) {
		
		return cargoDAO.updateCargo(cargo);
	}
	
	@Override
	public Cargo getCargo(String nombreCargo) {
		
		return cargoDAO.getCargo(nombreCargo);
	}

	@Override
	public boolean cambiarEstadoCargo(String nombreCargo) {
		return cargoDAO.cambiarEstadoCargo(nombreCargo);
	}

	@Override
	public List<Cargo> getCargos() {
		
		return cargoDAO.getCargos();
	}

	@Override
	public Cargo getCargoPorId(int id) {
		
		return cargoDAO.getCargoPorId(id);
	}

	@Override
	public Cargo getCargoInt(int id) {
		
		return cargoDAO.getCargoInt(id);
	}

	@Override
	public List<Cargo> getCargosActivos() {
		
		return cargoDAO.getCargosActivos();
	}

	@Override
	public boolean getCargoPorNombre(String nombre) {
		
		return cargoDAO.getCargoPorNombre(nombre);
	}

		
}
