package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.HabitacionDAO;
import com.crud.dao.TipoHabitacionDAO;
import com.crud.model.Habitacion;
import com.crud.model.Servicio;
import com.crud.model.TipoHabitacion;

@Service
@Transactional
public class TipoHabitacionServiceImpl implements TipoHabitacionService{

	@Autowired
	private TipoHabitacionDAO tipoHabitacionDAO;

	@Override
	public TipoHabitacion getTipoHabitacion(int id) {
		return tipoHabitacionDAO.getTipoHabitacion(id);
	}

	@Override
	public List<TipoHabitacion> getTipoHabitaciones() {
		return tipoHabitacionDAO.getTipoHabitaciones();
	}

	
}
