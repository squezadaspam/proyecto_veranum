package com.crud.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.LocalDataSourceConnectionProvider;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.model.Esquema;
import com.crud.model.Reserva;
import com.crud.dao.EsquemaDAO;
import com.crud.dao.EsquemaDAOimpl;
import com.crud.dao.HabitacionDAO;
import com.crud.dao.ReservaDAO;

@Service
@Transactional
public class EsquemaServiceImpl implements EsquemaService {
	
	@Autowired
	private EsquemaDAO esquemaDAO;
	
	@Override
	public void addEsquema(Esquema Esquema) {
		esquemaDAO.addEsquema(Esquema);
		
	}

	@Override
	public void updateEsquema(Esquema Esquema) {
		esquemaDAO.updateEsquema(Esquema);
		
	}

	@Override
	public Esquema getEsquema(int id) {
		return esquemaDAO.getEsquema(id);
	}

	@Override
	public void deleteEsquema(int id) {
		esquemaDAO.deleteEsquema(id);
		
	}

	@Override
	public List<Esquema> getEsquemas() {
		return esquemaDAO.getEsquemas();
	}
	
	@Override
	public int getCantidadDias() {
		return esquemaDAO.getCantidadDias();
	}
	
	@Override
	public Integer getEsquemaPorHabitacionIdYPorFechaCantidad(int idHabitacion, LocalDate fecha1, LocalDate fecha2){
		return esquemaDAO.getEsquemaPorHabitacionIdYPorFechaCantidad(idHabitacion, fecha1, fecha2);
	}
	
	@Override
	public Esquema getEsquemaPorHabitacionIdYPorFecha(int idHabitacion, LocalDate fecha){
		return esquemaDAO.getEsquemaPorHabitacionIdYPorFecha(idHabitacion, fecha);
	}
	
	@Override
	public LocalDate getFechaMin(){
		return esquemaDAO.getFechaMin();
	}
	
	@Override
	public LocalDate getFechaMax(){
		return esquemaDAO.getFechaMax();
	}
	
	@Override
	public boolean isSpaceForAReserveByHabitacion(LocalDate fechaDesde, LocalDate fechaHasta, int idHabitacion){
		return esquemaDAO.isSpaceForAReserveByHabitacion(fechaDesde, fechaHasta, idHabitacion);
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdDeReserva(int idReserva){
		return esquemaDAO.getEsquemasPorIdDeReserva(idReserva);
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdHabitacion(int idHabitacion){
		return esquemaDAO.getEsquemasPorIdHabitacion(idHabitacion, LocalDate.now().toString());
	}
	
	@Override
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion){
		return esquemaDAO.getReservasByIdHabitacion(idHabitacion);
	}
	
	@Override
	public List<Esquema> getEsquemasPorIdHabitacionEntreFecha(int idHabitacion,LocalDate fechaInicio,LocalDate fechaFin){
		return esquemaDAO.getEsquemasPorIdHabitacionEntreFecha(idHabitacion, fechaInicio,fechaFin);
	}

	@Override
	public void desactivarEsquemasPorIdHabitacion(int idHabitacion) {
		esquemaDAO.desactivarEsquemasPorIdHabitacion(idHabitacion);
		
	}

	@Override
	public void guardaReservaEnEsquema(int idReserva, int idHabitacion, LocalDate fechaDesde, LocalDate fechaHasta) {
		esquemaDAO.guardaReservaEnEsquema(idReserva, idHabitacion, fechaDesde, fechaHasta);
	}

	@Override
	public void ordenaTodo() {
		esquemaDAO.ordenaTodo();
	}

	@Override
	public Integer calcularPrecioReserva(int idReserva) {
		return esquemaDAO.calcularPrecioReserva(idReserva);
	}

	@Override
	public void ordenaTodoPorTipoHabitacion(int idTipoHabitacion) {
		esquemaDAO.ordenaTodoPorTipoHabitacion(idTipoHabitacion);
	}

	@Override
	public List<Reserva> getReservasEnCurso() {
		return esquemaDAO.getReservasEnCurso();
	}
	

	

}
