package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.IncidenteDAO;
import com.crud.model.Incidente;

@Service
@Transactional
public class IncidenteServiceImpl implements IncidenteService {

	@Autowired
	private IncidenteDAO incidenteDAO;

	@Override
	public void addIncidente(Incidente incidente) {
		incidenteDAO.addIncidente(incidente);
		
	}

	@Override
	public boolean updateIncidente(Incidente incidente) {
		
		return incidenteDAO.updateIncidente(incidente);
	}

	@Override
	public Incidente getIncidenteInt(int id) {
		
		return incidenteDAO.getIncidenteInt(id);
	}

	@Override
	public List<Incidente> getIncidentes() {
		
		return incidenteDAO.getIncidentes();
	}
	
}
		