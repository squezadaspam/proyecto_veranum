package com.crud.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.HabitacionDAO;
import com.crud.model.Habitacion;
import com.crud.dao.HabitacionDAO;
import com.crud.model.TipoHabitacion;

@Service
@Transactional
public class HabitacionServiceImpl implements HabitacionService{

	@Autowired
	private HabitacionDAO habitacionDAO;
		
	@Override
	public void addHabitacion(Habitacion Habitacionn) {
		habitacionDAO.addHabitacion(Habitacionn);
		
	}

	public List<TipoHabitacion> getTipos(){
		return habitacionDAO.getTipos();
	}
	

	@Override
	public void updateHabitacion(Habitacion Habitacionn) {
		habitacionDAO.updateHabitacion(Habitacionn);
		
	}

	@Override
	public Habitacion getHabitacion(int id) {
		return habitacionDAO.getHabitacion(id);
	}

	@Override
	public void deleteHabitacion(int id) {
		habitacionDAO.deleteHabitacion(id);
		
	}

	public List<Habitacion> listarHabitaciones()
	{
		return habitacionDAO.listarHabitaciones();
	}

	@Override
	public List<Habitacion> getHabitaciones() {
		return habitacionDAO.getHabitaciones();
	}

	@Override
	public int getCantidadHabitacionesHabilitadas() {
		return habitacionDAO.getCantidadHabitacionesHabilitadas();
	}

	@Override
	public List<Habitacion> getHabitacionesHabilitadas() {
		return habitacionDAO.getHabitacionesHabilitadas();
	}
	
	

}