package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.DetalleServicioDAO;
import com.crud.model.DetalleServicio;

@Service
@Transactional
public class DetalleServicioServiceImpl implements DetalleServicioService{

	@Autowired
	private DetalleServicioDAO detalleServicioDAO;
	
	@Override
	public void addDetalleServicio(DetalleServicio detalleServicio) {
		detalleServicioDAO.addDetalleServicio(detalleServicio);
		
	}

	@Override
	public void updateDetalleServicio(DetalleServicio detalleServicio) {
		detalleServicioDAO.updateDetalleServicio(detalleServicio);
	}

	@Override
	public DetalleServicio getDetalleServicio(int id) {
		return detalleServicioDAO.getDetalleServicio(id);
	}

	@Override
	public List<DetalleServicio> getDetalleServicio() {
		
		return detalleServicioDAO.getDetalleServicio();
	}

	@Override
	public boolean verificarDetalleServicio(int id) {
		
		return detalleServicioDAO.verificarDetalleServicio(id);
	}

	@Override
	public DetalleServicio getDetalleServicioInt(int id) {
		return detalleServicioDAO.getDetalleServicioInt(id);
	}



}





