package com.crud.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.dao.HotelDAO;
import com.crud.model.Hotel;

@Service
@Transactional
public class HotelServiceImpl implements HotelService {

	@Autowired
	private HotelDAO hotelDAO;
	
	@Override
	public void addHotel(Hotel hotel) {

		hotelDAO.addHotel(hotel);
		
	}

	@Override
	public boolean updateHotel(Hotel hotel) {
		
		return hotelDAO.updateHotel(hotel);
	}

	@Override
	public Hotel getHotel(int id) {
		
		return hotelDAO.getHotel(id);
	}

	@Override
	public List<Hotel> getHoteles() {
		
		return hotelDAO.getHoteles();
	}

}
