package com.crud.service;

import java.util.List;

import com.crud.model.DetalleServicio;
import com.crud.model.Servicio;

public interface ServicioService {

	
	
	public void addServicio(Servicio servicio);
	public boolean updateServicio(Servicio servicio);
	public Servicio getServicio(int id);
	public void deleteServicio(int id);
	public List<Servicio> getServicio();
	public Servicio getServicioInt(int id);
}
