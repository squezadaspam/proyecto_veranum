package com.crud.service;

import java.util.List;

import com.crud.model.Hotel;

public interface HotelService {

	public void addHotel(Hotel hotel);
	public boolean updateHotel(Hotel hotel);
	public Hotel getHotel(int id);
	public List<Hotel> getHoteles();
}
