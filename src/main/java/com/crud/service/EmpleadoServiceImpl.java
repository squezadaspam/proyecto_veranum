package com.crud.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.dao.EmpleadoDAO;
import com.crud.model.Empleado;

@Service
@Transactional
public class EmpleadoServiceImpl implements EmpleadoService {

	@Autowired
	private EmpleadoDAO empleadoDAO;
	
	@Override
	public void addEmpleado(Empleado empleado) {
		
		empleadoDAO.addEmpleado(empleado);
		
	}

	@Override
	public boolean updateEmpleado(Empleado empleado) {
		
		return empleadoDAO.updateEmpleado(empleado);
	}

	@Override
	public Empleado getEmpleado(int id) {
		
		return empleadoDAO.getEmpleado(id);
	}

	@Override
	public boolean cambiarEstadoEmpleado(int id) {
		
		return empleadoDAO.cambiarEstadoEmpleado(id);
	}

	@Override
	public List<Empleado> getEmpleados() {
		
		return empleadoDAO.getEmpleados();
	}

	

	

	
	

}
