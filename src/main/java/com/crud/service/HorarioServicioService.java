package com.crud.service;

import java.util.List;

import com.crud.model.DetalleServicio;
import com.crud.model.HorarioServicio;


public interface HorarioServicioService {

	public void addHorarioServicio(HorarioServicio horarioServicio);
	public void updateHorarioServicio(HorarioServicio horarioServicio);
	public HorarioServicio getHorarioServicio(int id);
	public void deleteHorarioServicio(int id);
	public List<HorarioServicio> getHorarioServicio();
	public HorarioServicio getHorarioServicioInt(int id);
}