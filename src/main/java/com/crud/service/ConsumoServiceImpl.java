package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.ConsumoDAO;
import com.crud.model.Consumo;
import com.crud.model.Pago;
@Service
@Transactional
public class ConsumoServiceImpl implements ConsumoService {

	
	@Autowired
	private ConsumoDAO consumoDAO;
	
	@Override
	public void addConsumo(Consumo consumo) {
		consumoDAO.addConsumo(consumo);
		
	}

	@Override
	public Consumo getConsumo(int id) {
		return consumoDAO.getConsumo(id);
	}

	@Override
	public void deleteConsumo(int id) {
		consumoDAO.deleteConsumo(id);
		
	}

	@Override
	public List<Consumo> getConsumo() {
		return consumoDAO.getConsumo();
	}

	@Override
	public Consumo getConsumoInt(int id) {
		return consumoDAO.getConsumoInt(id);
	}

	@Override
	public int getTotalPorIdUsuario(int id) {
		return consumoDAO.getTotalPorIdUsuario(id);
	}

	@Override
	public void updateConsumo(Consumo consumo) {
		consumoDAO.updateConsumo(consumo);
	}

	@Override
	public void addPago(Pago pago) {
		consumoDAO.addPago(pago);
		
	}

	@Override
	public List<Consumo> getConsumoPorEstado(int idConsumo) {
		return consumoDAO.getConsumoPorEstado(idConsumo);
	}

}
