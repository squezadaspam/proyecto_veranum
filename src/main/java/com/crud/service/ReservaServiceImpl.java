package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.model.Reserva;
import com.crud.dao.ReservaDAO;

@Service
@Transactional
public class ReservaServiceImpl implements ReservaService {

	@Autowired
	private ReservaDAO reservaDAO;
		
	@Override
	public Reserva addReserva(Reserva reserva) {
		return reservaDAO.addReserva(reserva);
		
	}

	@Override
	public void updateReserva(Reserva reserva) {
		reservaDAO.updateReserva(reserva);
		
	}

	@Override
	public Reserva getReserva(int id) {
		return reservaDAO.getReserva(id);
	}

	@Override
	public void deleteReserva(int id) {
		reservaDAO.deleteReserva(id);
		
	}

	@Override
	public List<Reserva> getReservas() {
		return reservaDAO.getReservas();
	}

	@Override
	public List<Reserva> getReservasByIdHabitacion(int idHabitacion) {
		return reservaDAO.getReservasByIdHabitacion(idHabitacion);
	}

	@Override
	public int getDuracionEnDias(int idReserva) {
		return reservaDAO.getDuracionEnDias(idReserva);
	}

	@Override
	public List<Reserva> getReservasPorIdUsuario(int idUsuario){
		return reservaDAO.getReservasPorIdUsuario(idUsuario);
	}

	@Override
	public Reserva getReservaPorIdUsuarioYCodigo(int idUsuario, String codigo) {
		return reservaDAO.getReservaPorIdUsuarioYCodigo(idUsuario, codigo);
	}

	@Override
	public void desactivarReservasPorIdHabitacion(int idHabitacion) {
		reservaDAO.desactivarReservasPorIdHabitacion(idHabitacion);
	}

	@Override
	public void saveOrUpdateReserva(Reserva reserva) {
		reservaDAO.saveOrUpdateReserva(reserva);
	}
}
