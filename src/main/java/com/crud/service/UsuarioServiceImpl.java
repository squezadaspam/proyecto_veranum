package com.crud.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.UsuarioDAO;
import com.crud.model.Ciudad;
import com.crud.model.Contacto;
import com.crud.model.Direccion;
import com.crud.model.Empleado;
import com.crud.model.Empresa;
import com.crud.model.Hotel;
import com.crud.model.Pais;
import com.crud.model.Persona;
import com.crud.model.Telefono;
import com.crud.model.Usuario;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDAO UsuarioDAO;


	@Override
	public void addUsuarioPersona(Persona persona) {
		UsuarioDAO.addUsuarioPersona(persona);
		
	}
	@Override
	public void addUsuarioEmpresa(Empresa empresa) {
		UsuarioDAO.addUsuarioEmpresa(empresa);
		
	}
	
	@Override
	public void addTelefono(Telefono telefono){
		UsuarioDAO.addTelefono(telefono);
	}
	
	@Override
	public void addUsuario(Usuario usuario){
		UsuarioDAO.addUsuario(usuario);
	}
	
	@Override
	public void addContacto(Contacto contacto){
		UsuarioDAO.addContacto(contacto);
	}
	
	@Override
	public void addDireccion(Direccion direccion){
		UsuarioDAO.addDireccion(direccion);
	}
	
	@Override
	public void addCiudad(Ciudad ciudad){
		UsuarioDAO.addCiudad(ciudad);
	}
	
	@Override
	public boolean updateUsuario(Usuario usuario, Contacto contacto, Pais pais, Ciudad ciudad,
			Direccion direccion, Persona persona) {
		return UsuarioDAO.updateUsuario(usuario, contacto, pais, ciudad, direccion, persona);
		
	}

	@Override
	public Usuario getUsuario(String usuario) {
		return UsuarioDAO.getUsuario(usuario);
	}

	@Override
	public Contacto getContacto(String usuario) {
		return UsuarioDAO.getContacto(usuario);
	}

	@Override
	public List<Telefono> getTelefono(String usuario) {
		return UsuarioDAO.getTelefono(usuario);
	}

	@Override
	public Direccion getDireccion(String usuario) {
		return UsuarioDAO.getDireccion(usuario);
	}

	@Override
	public Persona getPersona(String usuario) {
		return UsuarioDAO.getPersona(usuario);
	}

	@Override
	public Pais getPais(String usuario) {
		return UsuarioDAO.getPais(usuario);
	}
	
	@Override
	public Empresa getEmpresa(String usuario){
		return UsuarioDAO.getEmpresa(usuario);
	}
	
	@Override
	public Ciudad getCiudad(String usuario){
		return UsuarioDAO.getCiudad(usuario);
	}

	@Override
	public Usuario login(String usuario, String pass) {
		return UsuarioDAO.login(usuario, pass);
	}

	@Override
	public boolean NombreUsuario(String usuario) {
		return UsuarioDAO.NombreUsuario(usuario);
	}

	@Override
	public boolean CambiarEstadoUsuario(String usuario, String pass) {
		return UsuarioDAO.CambiarEstadoUsuario(usuario, pass);
	}
	
	@Override
	public List<Pais> getListadoPais() {
		return UsuarioDAO.getListadoPais();
	}

	@Override
	public boolean updateTelefono(Telefono telefono, String nom_user) {
		return UsuarioDAO.updateTelefono(telefono, nom_user);
	}
	
	@Override
	public List<Usuario> getListadoUsuarios() {
		return UsuarioDAO.getListadoUsuarios();
	}
	
	@Override
	public List getInformacionDePersonaPorIdUsuario() {
		return UsuarioDAO.getInformacionDePersonaPorIdUsuario();		
	}
	
	@Override
	public List getInformacionDeEmpresaPorIdUsuario() {
		return UsuarioDAO.getInformacionDeEmpresaPorIdUsuario();		
	}
	
	@Override
	public boolean cambiarClave(Usuario user) {
		return UsuarioDAO.cambiarClave(user);
	}

	@Override
	public Usuario getUsuario(int idUsuario) {
		return UsuarioDAO.getUsuario(idUsuario);
	}
	
	@Override
	public Persona getRunPersona(String run) {
		
		return UsuarioDAO.getRunPersona(run);
	}
	@Override
	public Contacto getRunContacto(String run) {
		
		return UsuarioDAO.getRunContacto(run);
	}
	@Override
	public Direccion getRunDireccion(String run) {
		
		return UsuarioDAO.getRunDireccion(run);
	}
	@Override
	public Telefono getRunTelefono(String run) {
		
		return UsuarioDAO.getRunTelefono(run);
	}
	@Override
	public void addEmpleado(Empleado empleado) {

		UsuarioDAO.addEmpleado(empleado);
	}
	@Override
	public List<Persona> getListadoPersonas() {
		
		return UsuarioDAO.getListadoPersonas();
	}
	@Override
	public List<Empleado> getListadoEmpleados() {
		
		return  UsuarioDAO.getListadoEmpleados();
	}
	@Override
	public Empleado getEmpleadoUsuario(int id) {
		
		return UsuarioDAO.getEmpleadoUsuario(id);
	}
	@Override
	public boolean updateEmpleadoUsuario(Empleado empleado) {

		return UsuarioDAO.updateEmpleadoUsuario(empleado);
	}
	@Override
	public Persona getPersona(int id) {
		
		return UsuarioDAO.getPersona(id);
	}
	@Override
	public List<Hotel> getListadoHoteles() {
		
		return UsuarioDAO.getListadoHoteles();
	}
	@Override
	public boolean getHotelPorNombre(String nombre) {
		
		return UsuarioDAO.getHotelPorNombre(nombre);
	}
	@Override
	public void addHotelUsuario(Hotel hotel) {

		UsuarioDAO.addHotelUsuario(hotel);
		
	}
	@Override
	public Hotel getHotelUsuario(int id) {

		return UsuarioDAO.getHotelUsuario(id);
	}
	@Override
	public boolean updateHotelUsuario(Hotel hotel) {
		
		return UsuarioDAO.updateHotelUsuario(hotel);
	}

	@Override
	public Empleado getEmpleadoIdPersona(int id_persona) {
		
		return UsuarioDAO.getEmpleadoIdPersona(id_persona);
	}
	@Override
	public boolean empleadoExiste(int id_persona) {
		
		return UsuarioDAO.empleadoExiste(id_persona);
	}
	@Override
	public boolean personaExiste(String run) {
		
		return UsuarioDAO.personaExiste(run);
	}
	@Override
	public Contacto getContacto(int id) {
		return UsuarioDAO.getContacto(id);
	}

}
