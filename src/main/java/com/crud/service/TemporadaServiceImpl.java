package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.TemporadaDAO;
import com.crud.model.Temporada;

@Service
@Transactional
public class TemporadaServiceImpl implements TemporadaService{

	@Autowired
	private TemporadaDAO temporadaDAO;
	
	
	@Override
	public void addTemporada(Temporada temporada) {
		temporadaDAO.addTemporada(temporada);
		
	}

	@Override
	public void updateTemporada(Temporada temporada) {
		temporadaDAO.updateTemporada(temporada);
		
	}

	@Override
	public Temporada getTemporada(int id) {
		return temporadaDAO.getTemporada(id);
	}

	@Override
	public void deleteTemporada(int id) {
		temporadaDAO.deleteTemporada(id);
		
	}

	@Override
	public List<Temporada> getTemporada() {
		return temporadaDAO.getTemporadas();
	}

}
