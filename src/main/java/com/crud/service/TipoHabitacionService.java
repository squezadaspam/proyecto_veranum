package com.crud.service;

import java.util.List;

import com.crud.model.Servicio;
import com.crud.model.TipoHabitacion;

public interface TipoHabitacionService {

	public TipoHabitacion getTipoHabitacion(int id);
	public List<TipoHabitacion> getTipoHabitaciones();
	
	
	
}
