package com.crud.reservaCore;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.mapping.Fetchable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.crud.model.Esquema;
import com.crud.model.Habitacion;
import com.crud.model.Reserva;
import com.crud.service.EsquemaService;
import com.crud.service.HabitacionService;
import com.crud.service.ReservaService;

/**
 * esta clase provee los metodos necesarios 
 * para el manejo de los espacios de las reservas.
 * 
 * @author sergio
 */
@Service
public class Matrix {
    
    
    @Autowired
	private HabitacionService habitacionService;
    
    @Autowired
	private EsquemaService esquemaService;
    
    @Autowired
	private ReservaService reservaService;
    
    /**
     * constructor
     */
    
    /**
     * Establece el rango de fechas y habitaciones habilitados para efectuar una reserva.
     * 
     * @param fechaDesde
     * @param FechaHasta 
     * @return boolean
     */
    public void setInicioEsquema(LocalDate fechaDesde, LocalDate FechaHasta){
    	List<Habitacion> habitaciones = habitacionService.getHabitaciones();
        for (Habitacion habitacion : habitaciones) {
			for (LocalDate date = fechaDesde; (date.isEqual(FechaHasta) || date.isBefore(FechaHasta)); date = date.plusDays(1)) {
				Esquema esquema = new Esquema();
				esquema.setEstado(1);
				esquema.setFecha(date);
				esquema.setIdHabitacion(habitacion.getId());
				esquema.setReservada(0);
				esquemaService.addEsquema(esquema);
			}
		}
    }
    
    /**
     * Devuelve true si la habitaci�n est� llena de reservas
     * @param idHabitacion
     * @return
     */
    public boolean habitacionLlena(int idHabitacion){
    	System.out.println("habitacionLlena");
        boolean state = false;
        
        LocalDate fechaMinima = esquemaService.getFechaMin();
        LocalDate fechaMaxima = esquemaService.getFechaMax();
        int numDias = esquemaService.getCantidadDias() +1;
        int cantidad = esquemaService.getEsquemaPorHabitacionIdYPorFechaCantidad(idHabitacion, fechaMinima, fechaMaxima);		
        /*int count = 0;
        
        for (LocalDate date = fechaMinima; (date.isEqual(fechaMaxima) || date.isBefore(fechaMaxima)); date = date.plusDays(1)) {
            if(esquemaService.getEsquemaPorHabitacionIdYPorFecha(idHabitacion, fechaMinima, fechaMaxima).getReservada()==1){
                count++;
            }
        }*/
        
        if(cantidad == numDias){
            state = true;
        }
        
        return state;
    }
    
    /**
     * M�todo que devuelve true si hay espacio para las posiciones especificadas.
     * 
     * @param posicionHabitacion - habitaci�n
     * @param diaInicio - Dia inicio
     * @param diaSalida - Dia t�rmino
     * @return 
     */
    public boolean hayEspacioParaLaReserva(int idHabitacion,LocalDate fechaDesde, LocalDate fechaHasta){
    	System.out.println("hayEspacioParaLaReserva");
    	LocalDate fechaMaxima = esquemaService.getFechaMax();
    	boolean estado = false;
    	Habitacion habitacion = habitacionService.getHabitacion(idHabitacion);
    	int duracionReserva = (int) ChronoUnit.DAYS.between(fechaDesde, fechaHasta);
    	
    	// si la fechaDesde es menor a la fechaHasta y si la duracion de la fecha no exede la fecha m�xima (fecha m�xima definida en el esquema) y si hay espacio para la reserva en la habitaci�n definida
    	// Si se cumplen las condiciones, se establece la variable estado = true
    	if((fechaDesde.isBefore(fechaHasta) || (fechaDesde.isEqual(fechaHasta))) && (habitacion.getEstado()==1) && ((fechaDesde.plusDays(duracionReserva).isBefore(fechaMaxima)) || (fechaDesde.plusDays(duracionReserva).isEqual(fechaMaxima))) && (esquemaService.isSpaceForAReserveByHabitacion(fechaDesde, fechaHasta, idHabitacion))){
    		estado = true;
    	}
    	return estado;
    }
    
    /**
     * 
     * Metodo que agrega una reserva al esquema. Guarda la reserva y el equema en la base de datos.
     * 
     * @param x1 - dia de inicio
     * @param x2 - dia de termino
     * @param id - id de reserva
     * @return boolean
     */
    public boolean guardarReservaEnEsquema(Reserva reserva, int idTipoHabitacion){
    	System.out.println("guardarReservaEnEsquema");
    	boolean reservado = false;
        LocalDate reservaFechaDesde = reserva.getFechaInicioLocalDate();
        LocalDate reservaFechaFin = reserva.getFechaFinLocalDate();
        int cont=1;
        List<Habitacion> habitaciones = habitacionService.getHabitaciones();
        
        for (Habitacion habitacion : habitaciones) {
        	if(hayEspacioParaLaReserva(habitacion.getId(), reservaFechaDesde, reservaFechaFin) && !reservado && habitacion.getIdTipoHabitacion() == idTipoHabitacion){
        		List<Esquema> esquemas = esquemaService.getEsquemasPorIdHabitacionEntreFecha(habitacion.getId(),reservaFechaDesde,reservaFechaFin);
        		reserva.setEstado(1);
        		reserva.setOcupada(0);
	    		reserva.setIdHabitacion(habitacion.getId());
	    		
	    		if(reserva.getId()>0){
	    			reservaService.updateReserva(reserva);
	    		} else {
		    		
		    		reserva = reservaService.addReserva(reserva);
	    		}
	    		
        		esquemaService.guardaReservaEnEsquema(reserva.getId(), habitacion.getId(), reservaFechaDesde, reservaFechaFin);
		    	
        		/*
	    		for(Esquema esquema : esquemas){
	    			if((esquema.getFechaLocalDate().isAfter(reservaFechaDesde) || esquema.getFechaLocalDate().isEqual(reservaFechaDesde)) && (esquema.getFechaLocalDate().isBefore(reservaFechaFin) || esquema.getFechaLocalDate().isEqual(reservaFechaFin))){
	    				esquema.setEstado(1);
			    		esquema.setIdHabitacion(habitacion.getId());
			    		esquema.setIdReserva(reserva.getId());
			    		esquema.setReservada(1);
			    		esquemaService.updateEsquema(esquema);
	    			}
	    		}
	    		*/
	    		reservado = true;
	    	
	    	}
        }
        return reservado;
	    	
    } 
    
    /**
     * M�todo que desvincula una reserva del esquema
     * @param id - id de reserva
     */
    public void desactivarReservaPorIdReserva(int idReserva){
    	System.out.println("desactivarReservaPorIdReserva");
        List<Esquema> esquemas = esquemaService.getEsquemasPorIdDeReserva(idReserva);
        
        for (Esquema esquema : esquemas) {
        	
			esquema.setReservada(0);
			esquema.setIdReserva(0);
			esquemaService.updateEsquema(esquema);
		}
        
        Reserva reserva = reservaService.getReserva(idReserva);
    	reserva.setEstado(0);
    	reservaService.updateReserva(reserva);
    	//reservaService.deleteReserva(reserva.getId());
    	
         
    }
    
    /**
     * M�todo que desvincula todas las reservas de una habitaci�n
     * Id
     * @param habitacion - id de habitacion
     */
    public void desactivarReservaPorIdHabitacion(int idHabitacion){
    	System.out.println("desactivarReservaPorIdHabitacion");
    	reservaService.desactivarReservasPorIdHabitacion(idHabitacion);
    	esquemaService.desactivarEsquemasPorIdHabitacion(idHabitacion);
    	
    }
    
    /**
     * M�todo que ordena las reservas de 2 habitaciones.
     * 
     * @param y1 - habitacion 1
     * @param y2 - habitacion 2
     * @return 
     */
    
    /*
    public boolean order(int idHabitaci�n1, int idHabitaci�n2){
    	System.out.println("order");
        boolean state = false;
            
        List<Reserva> reservasHabitacion1 = esquemaService.getReservasByIdHabitacion(idHabitaci�n1);
        List<Reserva> reservasHabitacion2 = esquemaService.getReservasByIdHabitacion(idHabitaci�n2);

		List<Reserva> optimalRoom = new ArrayList<>();
		List<Reserva> restReserves = new ArrayList<>();
		List<Reserva> listaDeReservas = new ArrayList<>();

        desactivarReservaPorIdHabitacion(idHabitaci�n1);
        desactivarReservaPorIdHabitacion(idHabitaci�n2);

        listaDeReservas.addAll(reservasHabitacion1);
        listaDeReservas.addAll(reservasHabitacion2);
        
        Collections.sort(listaDeReservas);

        if(!listaDeReservas.isEmpty() && (idHabitaci�n1!=idHabitaci�n2)){
            // se toma el primer elemento 
            optimalRoom.add(listaDeReservas.get(0));

            for (int i = 0; i < listaDeReservas.size(); i++) {

                if(i==0){
                    continue;
                }

                if(optimalRoom.get(optimalRoom.size() - 1).getFechaFinLocalDate().isBefore(listaDeReservas.get(i).getFechaInicioLocalDate())){
                    optimalRoom.add(listaDeReservas.get(i));
                } else {
                    restReserves.add(listaDeReservas.get(i));
                }


            }

            guardarReservasEnEsquema(optimalRoom);
            guardarReservasEnEsquema(restReserves);
            state = true;
        }
        
        return state;
    }
    */
    
    public void guardarReservasEnEsquema(List<Reserva> reservas, int idTipoHabitacion){
    	System.out.println("guardarReservasEnEsquema");
    	if(reservas.size()!=0){
    		for (Reserva reserva : reservas) {
				guardarReservaEnEsquema(reserva, idTipoHabitacion);
			}
    	}
    }
    
    public void guardarEsquemas(List<Esquema> esquemas){
    	System.out.println("guardarEsquemas");
    	if(esquemas.size()!=0){
    		for (Esquema esquema : esquemas) {
				esquemaService.updateEsquema(esquema);
			}
    	}
    }
    
    /**
     * 
     * M�todo que itera las habitaciones ordenando y buscando un espacio para
     * la reserva que se le especifica por par�metros.
     * Si inserta la reserva, devuelve true.
     * 
     * @param reserva - instancia de reserva
     * @return 
     */
    
    public boolean orderAndReserve(Reserva reserva,int idHabitacion){
        boolean doAgain = false;
        
    	if(guardarReservaEnEsquema(reserva, idHabitacion)){
            doAgain = true;
        }else{
        	// Se ordenan los espacios entre habitacion.
            esquemaService.ordenaTodoPorTipoHabitacion(idHabitacion);
            if(guardarReservaEnEsquema(reserva, idHabitacion)){
            	doAgain = true;
            }
        }
    	return doAgain;
    }
    public boolean estaDentroDeLaFechaDesdeYHasta(LocalDate fechaDesde, LocalDate fechaHasta, LocalDate fechaConsulta){
    	boolean estado = false;
    	for(LocalDate date = fechaDesde; (date.isEqual(fechaHasta) || date.isBefore(fechaHasta)); date = date.plusDays(1)){
    		if(fechaConsulta.isEqual(date)){
    			estado = true;
    		}
    	}
    	return estado;
    }
    
    
    
    
}
